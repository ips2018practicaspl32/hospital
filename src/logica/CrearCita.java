package logica;

//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import util.Conf;
import util.Jdbc;

public class CrearCita {

	private Cita cita;

	private Paciente p;
	private String dniPaciente;
	private String idPaciente;
	private String name;
	private String apellido;

	Connection c;
	PreparedStatement pst;
	ResultSet rs;

	public CrearCita(Cita cita, String nombreNuevoPaciente, String apellidoNuevoPaciente) {
		this.cita = cita;
		this.dniPaciente = cita.getDniPaciente();
		this.p = OperacionesPaciente.getPacienteDni(dniPaciente);
		this.name = nombreNuevoPaciente;
		this.apellido = apellidoNuevoPaciente;
	}

	public void crearCita() throws SQLException {
		try {
			setIdPaciente();
			if (dniPaciente == null || p == null) {
				insertarPaciente();
			}
			insertarCita();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}

	private void insertarCita() throws SQLException {

		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_INSERTAR_CITA"));

			pst.setString(1, cita.getIdCita());
			pst.setString(2, idPaciente);
			pst.setString(3, cita.getIdAdmin());
			pst.setString(4, cita.getIdMedico());
			pst.setString(5, cita.getIdSala());
			pst.setString(6, cita.getTipo());
			pst.setDate(7, cita.getHoraComienzo());
			pst.setDate(8, cita.getHoraFinal());
			pst.setString(9, cita.getInfoContacto());
			pst.setString(10, cita.getEstadoCita());
			pst.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		} finally {
			Jdbc.close(c);
			Jdbc.close(pst);
		}
	}

	private String getMaxIdPaciente() throws SQLException {
		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_GET_MAX_ID_PACIENTE"));
			rs = pst.executeQuery();

			if (rs.next()) {
				return rs.getString(1);
			}
			return null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}

	private void setIdPaciente() throws SQLException {
		if (dniPaciente != null)
			idPaciente = OperacionesPaciente.getPacienteDni(dniPaciente).getIdPaciente();
		else if (getMaxIdPaciente() != null)
			idPaciente = getMaxIdPaciente() + 1;
		else
			idPaciente = "1";
	}

	private void insertarPaciente() throws SQLException {

		try {
			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_INSERTAR_PACIENTE"));

			pst.setString(1, idPaciente);
			pst.setString(2, dniPaciente);
			pst.setString(3, name);
			pst.setString(4, apellido);
			pst.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
}
