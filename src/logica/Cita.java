package logica;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import util.Conf;
import util.Jdbc;

public class Cita {

	private String idCita;
	private String idPaciente;
	private String idAdmin;
	private String idMedico;
	private String idSala;
	private String tipo;
	private Date horaComienzo;
	private Date horaFinal;
	private String infoContacto;

	private String dniPaciente;
	private String estadoCita;

	public Cita() {
		try {
			Connection c = Jdbc.getConnection();
			PreparedStatement pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_SUM_ID_CITA"));
			ResultSet rs = pst.executeQuery();
			boolean cita = rs.next();
			if (cita)
				idCita = Integer.toString((rs.getInt(1) + 1));
			else
				idCita = "1";
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Cita(String idCita, String dniPaciente, String idAdmin, String idMedico, String idSala, String tipo,
			Date horaComienzo, Date horaFinal, String infoContacto, String estadoCita) {
		this.idCita = idCita;
		this.idMedico = idMedico;
		this.idAdmin = idAdmin;
		this.idPaciente = dniPaciente;
		this.idSala = idSala;
		this.tipo = tipo;
		this.horaComienzo = horaComienzo;
		this.horaFinal = horaFinal;
		this.infoContacto = infoContacto;
		this.estadoCita = estadoCita;
	}

	public String getEstadoCita() {
		return estadoCita;
	}

	public void setEstadoCita(String estadoCita) {
		this.estadoCita = estadoCita;
	}

	public String getDniPaciente() {
		return dniPaciente;
	}

	public void setDniPaciente(String dniPaciente) {
		this.dniPaciente = dniPaciente;
	}

	public void setIdCita(String idCita) {
		this.idCita = idCita;
	}

	public String getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(String idPaciente) {
		this.idPaciente = idPaciente;
	}

	public String getIdAdmin() {
		return idAdmin;
	}

	public void setIdAdmin(String idAdmin) {
		this.idAdmin = idAdmin;
	}

	public String getIdMedico() {
		return idMedico;
	}

	public void setIdMedico(String idMedico) {
		this.idMedico = idMedico;
	}

	public String getIdSala() {
		return idSala;
	}

	public void setIdSala(String idSala) {
		this.idSala = idSala;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Date getHoraComienzo() {
		return horaComienzo;
	}

	public void setHoraComienzo(java.sql.Date horaComienzo) {
		this.horaComienzo = horaComienzo;
	}

	public Date getHoraFinal() {
		return horaFinal;
	}

	public void setHoraFinal(java.sql.Date horaFinal) {
		this.horaFinal = horaFinal;
	}

	public String getInfoContacto() {
		return infoContacto;
	}

	public void setInfoContacto(String infoContacto) {
		this.infoContacto = infoContacto;
	}

	public String getIdCita() {
		return idCita;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(idCita);
		sb.append(" - ");
		sb.append(idPaciente);
		sb.append(" - ");
		sb.append(idAdmin);
		sb.append(" - ");
		sb.append(idMedico);
		sb.append(" - ");
		sb.append(idSala);
		sb.append(" - ");
		sb.append(tipo);
		sb.append(" - ");
		sb.append(horaComienzo);
		sb.append(" " + horaFinal);
		return sb.toString();
	}

	public static String getIdCita(String idPaciente, String fecha, String horaComienzo) {
		PreparedStatement pst = null;
		Connection c = null;
		ResultSet rs = null;
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_GET_ID_CITA"));

			pst.setString(1, idPaciente);
			String[] aFecha = fecha.split("-");
			String fechaSQL = aFecha[2] + "-" + aFecha[1] + "-" + aFecha[0] + horaComienzo + ":00.000000000";
			pst.setString(2, fechaSQL);

			rs = pst.executeQuery();

			if (rs.next())
				return rs.getString(1);
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}

	public static void updateCita(String idCita, String idAdmin) {
		PreparedStatement pst = null;
		Connection c = null;
		ResultSet rs = null;
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_CAMBIAR_ADMIN_CITA"));
			pst.setString(1, idAdmin);
			pst.setString(2, idCita);
			pst.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
}
