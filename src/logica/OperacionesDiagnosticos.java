package logica;

import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import util.Conf;
import util.Jdbc;

public class OperacionesDiagnosticos {

	private static Connection c;
	private static PreparedStatement pst;
	private static ResultSet rs;

	public static List<Diagnostico> buscarPorCodigo(String nombre) {
		List<Diagnostico> diagnosticos = new ArrayList<Diagnostico>();
		Diagnostico diagnostico;
		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_BUSCAR_DIAGNOSTICO_POR_CODIGO"));
			pst.setString(1, "%" + nombre + "%");
			rs = pst.executeQuery();

			while (rs.next()) {
				diagnostico = new Diagnostico(rs.getString(1), rs.getString(2), rs.getString(3));
				diagnosticos.add(diagnostico);
			}
			return diagnosticos;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public static List<Diagnostico> buscarPorDescripcion(String nombre) {
		List<Diagnostico> diagnosticos = new ArrayList<Diagnostico>();
		Diagnostico diagnostico;
		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_BUSCAR_DIAGNOSTICO_POR_DESCRIPCION"));
			pst.setString(1, "%" + nombre + "%");
			rs = pst.executeQuery();

			while (rs.next()) {
				diagnostico = new Diagnostico(rs.getString(1), rs.getString(2), rs.getString(3));
				diagnosticos.add(diagnostico);
			}
			return diagnosticos;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public static List<Diagnostico> buscarPorSubseccion(String nombre) {
		List<Diagnostico> diagnosticos = new ArrayList<Diagnostico>();
		Diagnostico diagnostico;
		
		try {
			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_BUSCAR_DIAGNOSTICO_POR_SUBSECCION"));
			pst.setString(1, "%" + nombre + "%");
			rs = pst.executeQuery();

			while (rs.next()) {
				diagnostico = new Diagnostico(rs.getString(1), rs.getString(2), rs.getString(3));
				diagnosticos.add(diagnostico);
			}
			return diagnosticos;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public static void insertarDiagnostico(String codigo, String id, String fecha){
		
		try {
			
			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_INSERTAR_DIAGNOSTICO"));
			pst.setString(1, codigo);
			pst.setString(2, id);
			
			Date f = new SimpleDateFormat("dd-MM-yyyy").parse(fecha);
			
			pst.setDate(3, new java.sql.Date(f.getTime()));
			pst.execute();
			
		} catch (SQLException | ParseException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(c);
			Jdbc.close(pst);
		}
	}
}
