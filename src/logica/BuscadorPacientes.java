package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import util.Conf;
import util.Jdbc;

public class BuscadorPacientes {
	
	private static Connection c;
	private static PreparedStatement pst;
	private static ResultSet rs;
	
	public static List<Paciente> buscarPorNombre(String nombre){
		List<Paciente> pacientes = new ArrayList<Paciente>();
		Paciente paciente;
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_BUSCAR_PACIENTE_POR_NOMBRE"));
			pst.setString(1,"%"+ nombre +"%");
			rs = pst.executeQuery();
			
			while(rs.next()) {
				paciente = new Paciente(null,rs.getString(1),rs.getString(2),rs.getString(3),null,null,null);
				pacientes.add(paciente);
			}
			return pacientes;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public static List<Paciente> buscarPorApellidos(String apellidos){
		List<Paciente> pacientes = new ArrayList<Paciente>();
		Paciente paciente;
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_BUSCAR_PACIENTE_POR_APELLIDOS"));
			pst.setString(1,"%"+ apellidos +"%");
			rs = pst.executeQuery();
			
			while(rs.next()) {
				paciente = new Paciente(null,rs.getString(1),rs.getString(2),rs.getString(3),null,null,null);
				pacientes.add(paciente);
			}
			return pacientes;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public static List<Paciente> buscarPorDni(String dni){
		List<Paciente> pacientes = new ArrayList<Paciente>();
		Paciente paciente;
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_BUSCAR_PACIENTE_POR_DNI"));
			pst.setString(1,"%"+ dni +"%");
			rs = pst.executeQuery();
			
			while(rs.next()) {
				paciente = new Paciente(null,rs.getString(1),rs.getString(2),rs.getString(3),null,null,null);
				pacientes.add(paciente);
			}
			return pacientes;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}	
}
