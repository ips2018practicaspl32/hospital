package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import util.Conf;
import util.Jdbc;
import util.PasswordEncrypt;

public class AņadirMedicos {
	private List<Medico> medicos;

	Connection c;
	PreparedStatement pst;
	ResultSet rs;

	public AņadirMedicos() {
		c = null;
		pst = null;
		rs = null;
		medicos = new ArrayList<Medico>();
		cargarMedicos();
	}

	private void cargarMedicos() {
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(Conf.getInstance().getProperty(
					"SQL_LISTAR_MEDICOS"));

			rs = pst.executeQuery();
			while (rs.next()) {
				String id = rs.getString(1);
				String nombre = rs.getString(2);
				String apellido = rs.getString(3);
				Medico medico = new Medico(id, nombre, apellido);
				medicos.add(medico);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}

	public boolean checkMedicoNoRepetido(String nombre, String apellido,
			String dni) {
		for (Medico m : medicos) {
			if (m.getNombre().equals(nombre)
					&& m.getApellidos().equals(apellido)
					&& m.getIdMedico().equals(dni))
				return false;
		}
		return true;

	}

	public String aņadirMedico(String nombre, String apellidos, String email,
			String dni) {
		PasswordEncrypt encriptador = new PasswordEncrypt();
		String contraseņaTemp=generarContraseņa();
		String contraseņa = encriptador.encriptarPassword(contraseņaTemp);
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(Conf.getInstance().getProperty(
					"SQL_AŅADIR_MEDICO"));
			
			pst.setString(1, dni);
			pst.setString(2, nombre);
			pst.setString(3, apellidos);
			pst.setString(4, email);
			pst.setString(5, contraseņa);
			pst.execute();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		
		return contraseņaTemp;

	}
	
	public String generarContraseņa(){
		 int leftLimit = 97; // letra 'a'
		    int rightLimit = 122; // letra 'z'
		    int targetStringLength = 10;
		    Random random = new Random();
		    StringBuilder buffer = new StringBuilder(targetStringLength);
		    for (int i = 0; i < targetStringLength; i++) {
		        int randomLimitedInt = leftLimit + (int) 
		          (random.nextFloat() * (rightLimit - leftLimit + 1));
		        buffer.append((char) randomLimitedInt);
		    }
		   return buffer.toString();
	}
}
