package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import util.Conf;
import util.Jdbc;

public class OperacionesProcedimientos {

	private static Connection c;
	private static PreparedStatement pst;

	
	public static void insertarProcedimiento(String codigo,String id,String descripcion, String fecha){
		
		try {
			
			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_INSERTAR_PROCEDIMIENTO"));
			pst.setString(1, codigo);
			pst.setString(2,id);
			pst.setString(3,descripcion);
			
			Date f = new SimpleDateFormat("dd-MM-yyyy").parse(fecha);
			
			pst.setDate(4, new java.sql.Date(f.getTime()));
			pst.execute();
			
		} catch (SQLException | ParseException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(c);
			Jdbc.close(pst);
		}
	}
}
