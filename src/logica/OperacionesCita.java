package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import util.Conf;
import util.Jdbc;

public class OperacionesCita {

	private static Connection c;
	private static PreparedStatement pst;

	public static void actualizarEstadoCita(String idCita) {

		try {
			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_ACTUALIZAR_ESTADO_CITA"));
			pst.setString(1, idCita);
			pst.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		} finally {
			Jdbc.close(c);
			Jdbc.close(pst);
		}
	}
}
