package logica;

import java.sql.*;

import util.Conf;
import util.Jdbc;

public class ListarPacientes {

	Connection c;
	PreparedStatement pst;
	ResultSet rs;

	public ListarPacientes() {
		c = null;
		pst = null;
		rs = null;
	}

	public Paciente getPacientePorID(String ID_PACIENTE) {
		Paciente p = null;
		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_CITAS_PACIENTE"));
			pst.setString(1, ID_PACIENTE);
			rs = pst.executeQuery();
			if (rs.next())
				p = new Paciente(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),rs.getString(6),rs.getString(7));
		
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return p;
	}
}