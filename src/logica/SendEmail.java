package logica;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmail {

	private final Properties properties = new Properties();
	private Session session;
	private Paciente paciente;

	public SendEmail(Paciente paciente) {
		this.paciente = paciente;
		sendEmail();
	}

	public SendEmail(String dni, String nombre, String apellidos, String email,
			String contraseña) {
		sendEmailMedico(dni, nombre, apellidos, email, contraseña);
	}

	private void sendEmailMedico(String dni, String nombre, String apellidos,
			String email, String contraseñaMedico) {
		String correoEnvia = "confirmacioncitaurgente@gmail.com"; // Desde el
																	// que se
																	// envia el
																	// mensaje
																	// (correo
		// real)
		String contraseña = "ipsPL3-2"; // Contraseña del correo que envia el
										// mensaje
		String destinatario = "jorgeiturrioz@gmail.com"; // Email del receptor
															// (por ahora el
															// mio, modificar
															// mas adelante para
															// que lo reciba por
															// parametro)
		String asunto = "Añadido al plantel de medicos: " + nombre + " "
				+ apellidos;
		String mensaje = "Estos son los datos que han sido añadidos a nuestra"
				+ " base de datos: \n\n\n Nombre: "
				+ nombre
				+ "\nApellidos: "
				+ apellidos
				+ "\nDni: "
				+ dni
				+"\nEmail: "
				+email
				+ "\nContraseña: "
				+ contraseñaMedico
				+ "\n\n\n ESTE CORREO HA SIDO GENERADO AUTOMATICAMENTE, NO TRATE DE RESPONDERLO";

		properties.setProperty("mail.smtp.host", "smtp.gmail.com");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.smtp.port", "587");

		/* Para evitar errores por firewall y antivirus */

		properties.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");
		properties.setProperty("mail.smtp.auth", "true");

		session = Session.getDefaultInstance(properties);
		MimeMessage mail = new MimeMessage(session);
		try {
			mail.setFrom(new InternetAddress(correoEnvia));
			mail.addRecipient(Message.RecipientType.TO, new InternetAddress(
					destinatario));
			mail.setSubject(asunto);
			mail.setText(mensaje);

			Transport t = session.getTransport("smtp");
			t.connect(correoEnvia, contraseña);
			t.sendMessage(mail, mail.getAllRecipients());
			t.close();

		} catch (Exception e) {
			System.out
					.println("Se ha producido un error al intentar enviar el correo");
			throw new RuntimeException();
		}
	}

	private void sendEmail() {

		/* No tocar el properties */

		String correoEnvia = "confirmacioncitaurgente@gmail.com"; // Desde el
																	// que se
																	// envia el
																	// mensaje
																	// (correo
		// real)
		String contraseña = "ipsPL3-2"; // Contraseña del correo que envia el
										// mensaje
		String destinatario = "jorgeiturrioz@gmail.com"; // Email del receptor
															// (por ahora el
															// mio, modificar
															// mas adelante para
															// que lo reciba por
															// parametro)
		String asunto = "Cita urgente";
		String mensaje = "Se adjunta la siguiente información sobre el paciente"
				+ " debido a que ha solicitado una cita urgente:\n\n\n"
				+ paciente.toString();

		properties.setProperty("mail.smtp.host", "smtp.gmail.com");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.smtp.port", "587");

		/* Para evitar errores por firewall y antivirus */

		properties.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");
		properties.setProperty("mail.smtp.auth", "true");

		session = Session.getDefaultInstance(properties);
		MimeMessage mail = new MimeMessage(session);
		try {
			mail.setFrom(new InternetAddress(correoEnvia));
			mail.addRecipient(Message.RecipientType.TO, new InternetAddress(
					destinatario));
			mail.setSubject(asunto);
			mail.setText(mensaje);

			Transport t = session.getTransport("smtp");
			t.connect(correoEnvia, contraseña);
			t.sendMessage(mail, mail.getAllRecipients());
			t.close();

		} catch (Exception e) {
			System.out
					.println("Se ha producido un error al intentar enviar el correo");
			throw new RuntimeException();
		}
	}
}
