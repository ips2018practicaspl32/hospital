package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import util.Conf;
import util.Jdbc;

public class EstadisticosEnfermedades {

	private static Connection c;
	private static PreparedStatement pst;
	private static ResultSet rs;
	
	
	public static List<Enfermedad> buscarEnfermedadesPeriodo(Date ini, Date fin){
		List<Enfermedad> enfermedades = new ArrayList<Enfermedad>();
		Enfermedad enfermedad;
		String codigo;
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_BUSCAR_ENFERMEDADES_PERIODO"));
			pst.setDate(1, ini);
			pst.setDate(2, fin);
			rs = pst.executeQuery();
			
			while(rs.next()) {
				enfermedad = new Enfermedad(rs.getString(1),rs.getDate(2));
				codigo = rs.getString(1);
				PreparedStatement pst2 = c.prepareStatement(Conf.getInstance().getProperty("SQL_BUSCAR_DIAGNOSTICO_POR_CODIGO"));
				pst2.setString(1, codigo);
				ResultSet rs2=pst2.executeQuery();
				if(rs2.next())
					enfermedad.setDescripcion(rs2.getString(1));
				
				enfermedades.add(enfermedad);
				Jdbc.close(rs2, pst2);
			}
			return enfermedades;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
}
