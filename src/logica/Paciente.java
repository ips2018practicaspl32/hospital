package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import util.Conf;
import util.Jdbc;

public class Paciente {

	private String idPaciente;
	private String nombrePaciente;
	private String apellidoPaciente;
	private String dniPaciente;
	private String telefono;
	private String historial;
	private String direccion;

	public Paciente(String idPaciente, String dniPaciente, String nombrePaciente, String apellido, String direccion,
			String telefono) {

		this.idPaciente = idPaciente;
		this.nombrePaciente = nombrePaciente;
		this.apellidoPaciente = apellido;
		this.telefono = telefono;
		this.dniPaciente = dniPaciente;
		this.direccion = direccion;

	}

	public Paciente(String idPaciente, String dniPaciente, String nombrePaciente, String apellido, String historial,
			String direccion, String telefono) {
		this(idPaciente, dniPaciente, nombrePaciente, apellido, direccion, telefono);
		this.historial = historial;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getIdPaciente() {
		return idPaciente;
	}

	public String getNombrePaciente() {
		return nombrePaciente;
	}

	public void setNombrePaciente(String nombrePaciente) {
		this.nombrePaciente = nombrePaciente;
	}

	public String getApellidoPaciente() {
		return apellidoPaciente;
	}

	public void setApellidoPaciente(String apellidoPaciente) {
		this.apellidoPaciente = apellidoPaciente;
	}

	public String getDniPaciente() {
		return dniPaciente;
	}

	public void setDniPaciente(String dniPaciente) {
		this.dniPaciente = dniPaciente;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getHistorial() {
		return historial;
	}

	public void setHistorial(String historial) {
		this.historial = historial;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("* * * * * * * * * * * * * * * * * * * * * \n");
		sb.append("Id del paciente: " + idPaciente + "\n");
		sb.append("Nombre del paciente: " + getNombrePaciente() + "\n");
		sb.append("Apellido/s del paciente: " + getApellidoPaciente() + "\n");
		sb.append("DNI del paciente: " + getDniPaciente() + "\n");

		if (getHistorial() != null)
			sb.append("Historial del paciente:" + getHistorial() + "\n");
		if (getTelefono() != null)
			sb.append("N�mero del paciente: " + getTelefono() + "\n");

		sb.append("* * * * * * * * * * * * * * * * * * * * *");
		return sb.toString();
	}
}
