package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import util.Conf;
import util.Jdbc;

public class OperacionesMedico {

	public static List<Medico> listarMedicos() {

		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		List<Medico> lista = new ArrayList<Medico>();

		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_LISTAR_MEDICOS"));
			rs = pst.executeQuery();

			while (rs.next()) {
				Medico m = new Medico(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5));
				lista.add(m);
			}

			return lista;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}

	public static void actualizarNombre(String ID_MEDICO, String nuevoValor) {

		Connection c = null;
		PreparedStatement pst = null;

		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_ACTUALIZAR_NOMBRE_MEDICO"));
			pst.setString(1, nuevoValor);
			pst.setString(2, ID_MEDICO);
			pst.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(c);
			Jdbc.close(pst);
		}
	}
	
	public static void actualizarApellidos(String ID_MEDICO, String nuevoValor){
		
		Connection c = null;
		PreparedStatement pst = null;

		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_ACTUALIZAR_APELLIDOS_MEDICO"));
			pst.setString(1, nuevoValor);
			pst.setString(2, ID_MEDICO);
			pst.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(c);
			Jdbc.close(pst);
		}
	}
	
	public static void actualizarCorreo(String ID_MEDICO, String nuevoValor){
		
		Connection c = null;
		PreparedStatement pst = null;

		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_ACTUALIZAR_CORREO_MEDICO"));
			pst.setString(1, nuevoValor);
			pst.setString(2, ID_MEDICO);
			pst.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(c);
			Jdbc.close(pst);
		}
	}
	
	public static void actualizarContraseña(String ID_MEDICO, String nuevoValor){
		
		Connection c = null;
		PreparedStatement pst = null;

		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_ACTUALIZAR_CONTRASEÑA_MEDICO"));
			pst.setString(1, nuevoValor);
			pst.setString(2, ID_MEDICO);
			pst.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(c);
			Jdbc.close(pst);
		}
	}
}
