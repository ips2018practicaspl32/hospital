package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import util.Conf;
import util.Jdbc;

public class Consulta {
	


	private static String id;
	
	private static int generarIdConsulta() {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_MAX_ID_CONSULTA"));
			
			rs = pst.executeQuery();
			
			if(rs.next())
				return rs.getInt(1) + 1 ;
			return 1;
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public static void insertarConsulta(String idCita, String causas, String prescripcion) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_ADD_CONSULTA"));
			
			String idConsulta = id = generarIdConsulta() +"";
			pst.setString(1, idConsulta);
			pst.setString(2, idCita);
			pst.setString(3, causas);
			pst.setString(4, prescripcion);
			pst.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public static String getId() {
		return id;
	}
}
