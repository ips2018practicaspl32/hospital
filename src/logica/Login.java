package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import util.Conf;
import util.Jdbc;

public class Login {

	Connection c;
	PreparedStatement pst;
	ResultSet rs;

	public Login() {
		c = null;
		pst = null;
		rs = null;
	}

	public int comprobarExisteAdmin(String id_admin) {
		int checkUser = 0;
		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_COMPROBAR_ADMIN_EXISTE"));
			pst.setString(1, id_admin);
			rs = pst.executeQuery();
			if (rs.next())
				checkUser = rs.getInt(1);
		
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return checkUser;
	}
	
	public int comprobarExisteMedico(String id_medico) {
		int checkUser = 0;
		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_COMPROBAR_MEDICO_EXISTE"));
			pst.setString(1, id_medico);
			rs = pst.executeQuery();
			if (rs.next())
				checkUser = rs.getInt(1);
		
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return checkUser;
	}
	
	public String comprobarContrasenaAdmin(String id_admin) {
		String pass = null;
		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_COMPROBAR_CONTRASENA_ADMIN"));
			pst.setString(1, id_admin);
			rs = pst.executeQuery();
			if (rs.next())
				pass = rs.getString(1);
		
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return pass;
	}
	
	public String comprobarContrasenaMedico(String id_medico) {
		String pass = null;
		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_COMPROBAR_CONTRASENA_MEDICO"));
			pst.setString(1, id_medico);
			rs = pst.executeQuery();
			if (rs.next())
				pass = rs.getString(1);
		
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return pass;
	}
}

