package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import util.Conf;
import util.Jdbc;

public class BuscadorMedicos {
	
	private static Connection c;
	private static PreparedStatement pst;
	private static ResultSet rs;
	
	public static List<Medico> buscarPorNombre(String nombre){
		List<Medico> medicos = new ArrayList<Medico>();
		Medico medico;
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_BUSCAR_MEDICO_POR_NOMBRE"));
			pst.setString(1,"%"+ nombre +"%");
			rs = pst.executeQuery();
			
			while(rs.next()) {
				medico = new Medico(rs.getString(1),rs.getString(2),rs.getString(3));
				medicos.add(medico);
			}
			return medicos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public static List<Medico> buscarPorApellidos(String apellidos){
		List<Medico> medicos = new ArrayList<Medico>();
		Medico medico;
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_BUSCAR_MEDICO_POR_APELLIDOS"));
			pst.setString(1,"%"+ apellidos +"%");
			rs = pst.executeQuery();
			
			while(rs.next()) {
				medico = new Medico(rs.getString(1),rs.getString(2),rs.getString(3));
				medicos.add(medico);
			}
			return medicos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public static List<Medico> buscarPorDni(String dni){
		List<Medico> medicos = new ArrayList<Medico>();
		Medico medico;
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_BUSCAR_MEDICO_POR_DNI"));
			pst.setString(1,"%"+ dni +"%");
			rs = pst.executeQuery();
			
			while(rs.next()) {
				medico = new Medico(rs.getString(1),rs.getString(2),rs.getString(3));
				medicos.add(medico);
			}
			return medicos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
}
