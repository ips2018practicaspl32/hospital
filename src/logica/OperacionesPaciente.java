package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import util.Conf;
import util.Jdbc;

public class OperacionesPaciente {

	public static List<Paciente> listarPacientes() {

		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		List<Paciente> lista = new ArrayList<Paciente>();

		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_LISTAR_PACIENTES"));
			rs = pst.executeQuery();

			while (rs.next()) {
				Paciente p = new Paciente(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(6), rs.getString(7));
				lista.add(p);
			}

			return lista;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}

	public static void actualizarNombre(String ID_PACIENTE, String nuevoValor) {

		Connection c = null;
		PreparedStatement pst = null;

		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_ACTUALIZAR_NOMBRE_PACIENTE"));
			pst.setString(1, nuevoValor);
			pst.setString(2, ID_PACIENTE);
			pst.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(c);
			Jdbc.close(pst);
		}
	}

	public static void actualizarApellido(String ID_PACIENTE, String nuevoValor) {

		Connection c = null;
		PreparedStatement pst = null;

		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_ACTUALIZAR_APELLIDO_PACIENTE"));
			pst.setString(1, nuevoValor);
			pst.setString(2, ID_PACIENTE);
			pst.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(c);
			Jdbc.close(pst);
		}
	}

	public static void actualizarDireccion(String ID_PACIENTE, String nuevoValor) {

		Connection c = null;
		PreparedStatement pst = null;

		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_ACTUALIZAR_DIRECCION_PACIENTE"));
			pst.setString(1, nuevoValor);
			pst.setString(2, ID_PACIENTE);
			pst.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(c);
			Jdbc.close(pst);
		}
	}

	public static void actualizarTelefono(String ID_PACIENTE, String nuevoValor) {

		Connection c = null;
		PreparedStatement pst = null;

		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_ACTUALIZAR_TELEFONO_PACIENTE"));
			pst.setString(1, nuevoValor);
			pst.setInt(2, Integer.parseInt(ID_PACIENTE));
			pst.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(c);
			Jdbc.close(pst);
		}
	}
	
	public static Paciente getPacienteDni(String dni) {
		Connection c = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_PACIENTE_POR_DNI"));

			pst.setString(1, dni);
			rs = pst.executeQuery();

			Paciente p;
			if (rs.next()) {
				p = new Paciente(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getString(7));
				return p;
			}

			return null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
}
