package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import util.Conf;
import util.Jdbc;

public class A�adirPacientes {

	
	private static Connection c=null;
	private static PreparedStatement pst = null;
	private static ResultSet rs = null;
	
	public static  boolean comprobarPaciente(String nombre,String apellidos,String Dni) {
		for (Paciente p : BuscadorPacientes.buscarPorNombre(nombre)) {
			if (p.getApellidoPaciente().equals(apellidos) && p.getDniPaciente().equals(Dni))
				return false;
		}
		return true;
		
	}

	public static void a�adir(String nombre, String apellidos, String dni,
			String direccion, String telefono) {
		int numeroTelefono=0;
		if (!telefono.isEmpty())
			numeroTelefono=Integer.valueOf(telefono);
		try {
			String id = getMaxIdPaciente();
			id=String.valueOf((Integer.valueOf(id) + 1));
			c = Jdbc.getConnection();

			pst = c.prepareStatement(Conf.getInstance().getProperty(
					"SQL_A�ADIR_PACIENTE"));
			
			pst.setString(1, id);
			pst.setString(2, dni);
			pst.setString(3, nombre);
			pst.setString(4, apellidos);
			pst.setString(5, "");
			pst.setString(6, direccion);
			pst.setInt(7,numeroTelefono);
			pst.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	private static String getMaxIdPaciente() throws SQLException {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_GET_MAX_ID_PACIENTE"));
			
			rs = pst.executeQuery();
			
			if(rs.next()) {
				return rs.getString(1);
			}
			return null;

}
}
