package logica;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

import logica.Cita;
import util.Conf;
import util.Jdbc;

public class ListarCitas {

	Connection c;
	PreparedStatement pst;
	ResultSet rs;

	public ListarCitas() {
		c = null;
		pst = null;
		rs = null;
	}

	public List<Cita> cargarCitasPorFechaYMedico(String ID_MEDICO, String dia, String mes, String anio) {
		List<Cita> citas = new ArrayList<Cita>();
		try {

			c = Jdbc.getConnection();

			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_CITAS_MEDICO"));
			pst.setString(1, ID_MEDICO);
			if (Integer.parseInt(dia) < 10) {
				dia = "0" + dia;
			}
			if (Integer.parseInt(mes) < 10) {
				mes = "0" + mes;
			}
			String fechaConsulta = anio + "-" + mes + "-" + dia;
			pst.setString(2, fechaConsulta);

			rs = pst.executeQuery();

			while (rs.next()) {
				String idCita = rs.getString(1);
				String dniPaciente = rs.getString(2);
				String idAdmin = rs.getString(3);
				String idMedico = rs.getString(4);
				String idSala = rs.getString(5);
				String tipo = rs.getString(6);
				if (idAdmin.equals("0"))
					continue;
				Timestamp horaComienzo = rs.getTimestamp(7);
				Timestamp horaFin = rs.getTimestamp(8);
				String infoContacto = rs.getString(9);
				String estadoCita = rs.getString(10);
				Cita cita = new Cita(idCita, dniPaciente, idAdmin, idMedico, idSala, tipo,
						new java.sql.Date(horaComienzo.getTime()), new java.sql.Date(horaFin.getTime()), infoContacto,
						estadoCita);
				citas.add(cita);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return citas;
	}

	public boolean getHorarioCoinciden(Date fecha, String idMedico) {

		try {

			Date parsedDate = fecha;
			Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_COUNT_HORARIO"));
			pst.setTimestamp(1, timestamp);
			pst.setString(2, idMedico);
			rs = pst.executeQuery();
			rs.next();
			int count = rs.getInt(1);
			if (count > 0)
				return true;
			else
				return false;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean coincideJornadaHorario(Date horaInicio, Date horaFinal, String idMedico) {
		try {
			String dia;
			Date jornadaInicio;
			Date jornadaFinal;
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(horaInicio);

			SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
			horaInicio = dateFormat.parse(dateFormat.format(horaInicio.getTime()));

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_GET_JORNADAS"));
			pst.setString(1, idMedico);
			rs = pst.executeQuery();
			while (rs.next()) {
				dia = rs.getString(1);
				jornadaInicio = rs.getTime(2);
				jornadaFinal = rs.getTime(3);
				switch (calendar.get(Calendar.DAY_OF_WEEK)) {
				case Calendar.MONDAY:
					if (dia.equals("Lunes") && jornadaInicio.getTime() <= horaInicio.getTime()
							&& jornadaFinal.getTime() > horaInicio.getTime()) {
						return true;
					}
				case Calendar.TUESDAY:
					if (dia.equals("Martes") && jornadaInicio.getTime() <= horaInicio.getTime()
							&& jornadaFinal.getTime() > horaInicio.getTime()) {
						return true;
					}
				case Calendar.WEDNESDAY:
					if (dia.equals("Miercoles") && jornadaInicio.getTime() <= horaInicio.getTime()
							&& jornadaFinal.getTime() > horaInicio.getTime()) {
						return true;
					}
				case Calendar.THURSDAY:
					if (dia.equals("Jueves") && jornadaInicio.getTime() <= horaInicio.getTime()
							&& jornadaFinal.getTime() > horaInicio.getTime()) {
						return true;
					}
				case Calendar.FRIDAY:
					if (dia.equals("Viernes") && jornadaInicio.getTime() <= horaInicio.getTime()
							&& jornadaFinal.getTime() > horaInicio.getTime()) {
						return true;
					}
				case Calendar.SATURDAY:
					if (dia.equals("Sabado") && jornadaInicio.getTime() <= horaInicio.getTime()
							&& jornadaFinal.getTime() > horaInicio.getTime()) {
						return true;
					}
				case Calendar.SUNDAY:
					if (dia.equals("Domingo") && jornadaInicio.getTime() <= horaInicio.getTime()
							&& jornadaFinal.getTime() > horaInicio.getTime()) {
						return true;
					}
				}
			}
		} catch (SQLException | ParseException e) {
			e.printStackTrace();
		}
		return false;
	}

	private ArrayList<String> cargarSalas(String fecha) {
		ArrayList<String> citas = new ArrayList<String>();
		try {
			c = Jdbc.getConnection();

			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_LISTAR_SALAS"));
			;
			rs = pst.executeQuery();
			while (rs.next()) {
				String cita = rs.getString(1);
				citas.add(cita);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return citas;
	}

	public String[] getSalasDisponibles(String fecha) {

		List<String> salas = cargarSalas(fecha);
		List<String> salasLibres = new ArrayList<String>();
		try {
			for (int i = 0; i < salas.size(); i++) {

				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
				Date parsedDate = dateFormat.parse(fecha);
				Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());

				c = Jdbc.getConnection();
				pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_LISTAR_SALAS_DISPONIBLES"));
				pst.setTimestamp(1, timestamp);
				pst.setString(2, salas.get(i));
				rs = pst.executeQuery();
				rs.next();
				int count = rs.getInt(1);
				if (count == 0)
					salasLibres.add(salas.get(i));
			}

		} catch (SQLException | ParseException e) {
			throw new RuntimeException(e);
		}
		String[] arraySalas = salasLibres.toArray(new String[salasLibres.size()]);
		return arraySalas;
	}

	public Cita getCitaPorID(String idCita) {
		Cita cita = null;
		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_CITA_POR_ID"));
			pst.setString(1, idCita);
			rs = pst.executeQuery();
			if (rs.next())
				cita = new Cita(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getDate(7), rs.getDate(8), rs.getString(9), rs.getString(10));

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return cita;
	}

	public List<Cita> getCitasPorIDmedico(String idMedico) {

		List<Cita> citas = new ArrayList<Cita>();
		Cita cita = null;

		try {

			c = Jdbc.getConnection();
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_CITAS_POR_ID_MEDICO"));
			pst.setString(1, idMedico);
			rs = pst.executeQuery();

			while (rs.next()) {

				cita = new Cita(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getDate(7), rs.getDate(8), rs.getString(9), rs.getString(10));
				citas.add(cita);
			}

			return citas;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
	}

	public List<Cita> cargarCitasPorFechaYMedicoNoAceptadas(String ID_MEDICO, String dia, String mes, String anio) {
		List<Cita> citas = new ArrayList<Cita>();
		try {

			c = Jdbc.getConnection();

			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_CITAS_MEDICO"));
			pst.setString(1, ID_MEDICO);
			if (Integer.parseInt(dia) < 10) {
				dia = "0" + dia;
			}
			if (Integer.parseInt(mes) < 10) {
				mes = "0" + mes;
			}
			String fechaConsulta = anio + "-" + mes + "-" + dia;
			pst.setString(2, fechaConsulta);

			rs = pst.executeQuery();

			while (rs.next()) {
				String idCita = rs.getString(1);
				String dniPaciente = rs.getString(2);
				String idAdmin = rs.getString(3);
				String idMedico = rs.getString(4);
				String idSala = rs.getString(5);
				String tipo = rs.getString(6);
				if (!idAdmin.equals("0"))
					continue;
				Timestamp horaComienzo = rs.getTimestamp(7);
				Timestamp horaFin = rs.getTimestamp(8);
				String infoContacto = rs.getString(9);
				String estadoCita = rs.getString(10);
				Cita cita = new Cita(idCita, dniPaciente, idAdmin, idMedico, idSala, tipo,
						new java.sql.Date(horaComienzo.getTime()), new java.sql.Date(horaFin.getTime()), infoContacto,
						estadoCita);
				citas.add(cita);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst, c);
		}
		return citas;
	}
}
