package logica;

import util.PasswordEncrypt;

public class Medico {

	private String idMedico;
	private String nombre;
	private String apellidos;
	private String email;
	private String contraseña;

	public Medico(String id, String nombre, String apellidos) {
		this.idMedico = id;
		this.nombre = nombre;
		this.apellidos = apellidos;
		
	}
	
	public Medico(String id, String nombre, String apellidos, String email) {
		this.idMedico = id;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.email=email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Medico(String id, String nombre, String apellidos, String email, String contraseña) {
		
		this(id, nombre, apellidos);
		PasswordEncrypt encriptador = new PasswordEncrypt();
		this.email = email;
		if (contraseña != null)
			this.contraseña = encriptador.encriptarPassword(contraseña);
	}

	public String getIdMedico() {
		return idMedico;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public String getEmail() {
		return email;
	}

	public String getContraseña() {
		return contraseña;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(idMedico);
		sb.append(" - ");
		sb.append(nombre);
		sb.append(" " + apellidos);

		if (email != null && contraseña != null) {
			sb.append(" " + email);
			sb.append(" " + contraseña);
		}

		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Medico other = (Medico) obj;
		if (idMedico == null) {
			if (other.idMedico != null)
				return false;
		} else if (!idMedico.equals(other.idMedico))
			return false;
		return true;
	}

}
