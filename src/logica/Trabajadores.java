package logica;

import java.sql.*;
import java.sql.Date;
import java.util.*;

import util.Conf;
import util.Jdbc;

public class Trabajadores {

	private List<Medico> medicos;
	
	Connection c;
	PreparedStatement pst;
	ResultSet rs;
	
	public Trabajadores () {
		c = null;
		pst = null;
		rs  = null;
		medicos = new ArrayList<Medico>();
		cargarTrabajadores();
	}
	
	private void cargarTrabajadores() {
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_LISTAR_MEDICOS"));
			
			rs = pst.executeQuery();
			while(rs.next()) {
				String id = rs.getString(1);
				String nombre = rs.getString(2);
				String apellido = rs.getString(3);
				Medico medico = new Medico(id,nombre,apellido);
				medicos.add(medico);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public Medico[] getMedicos() {
		Medico[] arrayMedicos = medicos.toArray(new Medico[medicos.size()]);
		return arrayMedicos;	
	}
	
	public void asignarJornadaLaboral(String id, String dia, String horaInicio, String horaFin,Date diaDesde,Date diaHasta, String idAdmin) throws SQLException{
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_ASIGNAR_JORNADA_LABORAL"));
			pst.setString(1,id);
			pst.setString(2, dia);
			pst.setString(3, horaInicio);
			pst.setString(4, horaFin);
			pst.setDate(5, diaDesde);
			pst.setDate(6, diaHasta);
			pst.setString(7, idAdmin);
			pst.execute();
			
		} catch (SQLException e) {
			c.rollback();
			e.printStackTrace();
			throw new SQLException();
		}
		finally {
			Jdbc.close(c);
			Jdbc.close(rs,pst);
		}
	}
	public boolean comprobarTrabajadorLibreDeHorario(String id,Date diaDesde,Date diaHasta) {
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_JORNADA_MEDICO"));
			pst.setString(1,id);
			rs = pst.executeQuery();

			int cont = 0;
			Date desde,hasta;
			
			while(rs.next()) {
				cont++;
				desde = rs.getDate(2);
				hasta = rs.getDate(3);
				if(desde.equals(diaDesde) && hasta.equals(diaHasta))
					return false;
			}
			
			if(cont == 0)
				return false;
				
			return true;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public Medico getMedicoById(String id) {
		Medico medico = null;
		try {
		c = Jdbc.getConnection();
		
		pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_MEDICO_POR_ID"));
		
		pst.setString(1, id);
		
		rs = pst.executeQuery();
		
		
		while(rs.next()) {
			String idMedico = rs.getString(1);
			String nombre = rs.getString(2);
			String apellido = rs.getString(3);
			medico = new Medico(idMedico,nombre,apellido);
		}
	} catch (SQLException e) {
		throw new RuntimeException(e);
	}
	finally {
		Jdbc.close(rs, pst, c);
	}
		return medico;
	}
	

	
	
}
