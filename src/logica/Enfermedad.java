package logica;

import java.util.Date;

public class Enfermedad{
	private String codigo;
	private Date fecha;
	private String descripcion;
	
	public Enfermedad(String codigo, Date fecha) {
		this.codigo = codigo;
		this.fecha = fecha;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public Date getFecha() {
		return fecha;
	}
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}