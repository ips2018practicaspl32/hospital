package logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import util.Conf;
import util.Jdbc;

public class Historial {
	
	Connection c;
	PreparedStatement pst;
	ResultSet rs;
	
	public Historial() {
		c = null;
		pst = null;
		rs = null;
	}
	
	public String getCausas(String idPaciente) {
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_LISTAR_CAUSAS_PACIENTE"));
			pst.setString(1, idPaciente);
			
			rs = pst.executeQuery();
			
			String causas = "";
			while(rs.next()) {
				causas += rs.getString(1) +"\n";
			}
			
			return causas;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public String getPrescripciones(String idPaciente) {
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_LISTAR_PRESCRIPCIONES_PACIENTE"));
			pst.setString(1, idPaciente);
			
			rs = pst.executeQuery();
			
			String PRESCRIPCIONES = "";
			while(rs.next()) {
				PRESCRIPCIONES += rs.getString(1) +"\n";
			}
			
			return PRESCRIPCIONES;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public String getDiagnosticos(String idPaciente) {
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_LISTAR_DIAGNOSTICOS_PACIENTE"));
			pst.setString(1, idPaciente);
			
			rs = pst.executeQuery();
			
			String diagnosticos = "";
			while(rs.next()) {
				String codigo = rs.getString(1) +" ";
				String descripcion = rs.getString(2) + " ";
				String subseccion = rs.getString(3) + "\n";
				String fecha = rs.getDate(4).toString()+" ";
				diagnosticos += fecha+": "+"C�digo: " +codigo+" Descripci�n: "+descripcion+" Subseccion: " + subseccion;
			}
			
			return diagnosticos;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	public String getProcedimientos(String idPaciente) {
		try {
			c = Jdbc.getConnection();
			
			pst = c.prepareStatement(Conf.getInstance().getProperty("SQL_LISTAR_PROCEDIMIENTOS_PACIENTE"));
			pst.setString(1, idPaciente);
			
			rs = pst.executeQuery();
			
			String procedimientos = "";
			while(rs.next()) {
				String codigo = rs.getString(1) +" ";
				String descripcion = rs.getString(2) + "\n ";
				String fecha = rs.getDate(3).toString()+" ";
				procedimientos += fecha+": "+"C�digo: " +codigo+" Descripci�n: "+descripcion;
			}
			
			return procedimientos;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}finally {
			Jdbc.close(rs, pst, c);
		}
	}
	
	
}
