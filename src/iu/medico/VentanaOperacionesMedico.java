package iu.medico;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import iu.VentanaLogin;
import iu.administrativo.VentanaOperacionesAdministrativo;
import logica.Medico;

public class VentanaOperacionesMedico extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel panelOpMedico;
	private JLabel lbOperacionesMedico;
	private JPanel pnBotonesOpMedico;
	private JButton btListarPacientes;
	private JButton btAtenderCita;
	private Medico medicoActual;
	private JButton btDesconectar;
	private JButton btFijarCitas;
	//private JButton btnRealizarConsulta;

	/**
	 * Create the frame.
	 */
	public VentanaOperacionesMedico(Medico medico) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaOperacionesAdministrativo.class.getResource("/iu/img/logo.jpeg")));
		setTitle("Operaciones");
		setBounds(100, 100, 752, 384);
		panelOpMedico = new JPanel();
		panelOpMedico.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelOpMedico.setLayout(new BorderLayout(0, 0));
		setContentPane(panelOpMedico);
		panelOpMedico.add(getLbOperacionesMedico(), BorderLayout.NORTH);
		panelOpMedico.add(getPnBotonesOpMedico(), BorderLayout.CENTER);
		this.medicoActual=medico;
	}

	private JLabel getLbOperacionesMedico() {
		if (lbOperacionesMedico == null) {
			lbOperacionesMedico = new JLabel("Elija la operaci\u00F3n que desea realizar:");
			lbOperacionesMedico.setFont(new Font("Tahoma", Font.PLAIN, 30));
		}
		return lbOperacionesMedico;
	}
	private JPanel getPnBotonesOpMedico() {
		if (pnBotonesOpMedico == null) {
			pnBotonesOpMedico = new JPanel();
			pnBotonesOpMedico.setLayout(new GridLayout(0, 1, 10, 10));
			pnBotonesOpMedico.add(getBtListarPacientes());
			pnBotonesOpMedico.add(getBtAtenderCita());
			pnBotonesOpMedico.add(getBtFijarCitas());
			pnBotonesOpMedico.add(getBtDesconectar());
		}
		return pnBotonesOpMedico;
	}
	private JButton getBtListarPacientes() {
		if (btListarPacientes == null) {
			btListarPacientes = new JButton("Listar Pacientes por M\u00E9dico");
			btListarPacientes.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					mostrarVentana(new VentanaListarPacientes(medicoActual));
				}
			});
			
			btListarPacientes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btListarPacientes;
	}
	
	private void mostrarVentana(JDialog ventana) {
		ventana.setLocationRelativeTo(this);
		ventana.setModal(true);
		ventana.setVisible(true);
	}
	private JButton getBtAtenderCita() {
		if (btAtenderCita == null) {
			btAtenderCita = new JButton("Atender Cita");
			btAtenderCita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentana(new VentanaSeleccionarPacienteCita(medicoActual));
				}
			});
			btAtenderCita.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btAtenderCita;
	}
	private JButton getBtDesconectar() {
		if (btDesconectar == null) {
			btDesconectar = new JButton("Desconectarse");
			btDesconectar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarLogin();
				}
			});
			btDesconectar.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btDesconectar;
	}
	
	private void mostrarLogin() {
		this.setVisible(false);
		VentanaLogin ventana = new VentanaLogin();
		ventana.setLocationRelativeTo(this);
		ventana.setVisible(true);
	}
	private JButton getBtFijarCitas() {
		if (btFijarCitas == null) {
			btFijarCitas = new JButton("Fijar citas");
			btFijarCitas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentana(new VentanaCrearCitasMedico(medicoActual));
				}
			});
			btFijarCitas.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btFijarCitas;
	}
}
