package iu.medico;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import iu.BuscadorMedicoInterface;
import iu.VentanaBuscadorMedicos;
import logica.Medico;
@Deprecated
public class VentanaLoginMedico extends JDialog implements BuscadorMedicoInterface{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblEntrarComo;
	private JPanel panelCentral;
	private JPanel panel_Central;
	private JLabel lbl_medico;
	private JButton btnSiguiente;
	private JPanel panel_Inferior;
	private JButton btnAtrs;
	private JPanel panel_Derecha_Abajo;
	private JPanel panel_Central_Contenido;
//	private Trabajadores trabajadores;
	private JButton btSeleccionarMedico;
	private JTextField txMedicoSeleccionado;
	
	private Medico medico;

	/**
	 * Create the frame.
	 */
	public VentanaLoginMedico() {
		medico = null;
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//		trabajadores = new Trabajadores();
		setBounds(100, 100, 750, 457);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getPanelCentral(), BorderLayout.CENTER);
		contentPane.add(getPanel_Inferior(), BorderLayout.SOUTH);
		
	}
	private JLabel getLblEntrarComo() {
		if (lblEntrarComo == null) {
			lblEntrarComo = new JLabel("Entrar como");
			lblEntrarComo.setFont(new Font("Tahoma", Font.PLAIN, 24));
		}
		return lblEntrarComo;
	}
	private JPanel getPanelCentral() {
		if (panelCentral == null) {
			panelCentral = new JPanel();
			panelCentral.setLayout(new BorderLayout(0, 0));
			panelCentral.add(getPanel_Central());
			panelCentral.add(getLblEntrarComo(), BorderLayout.NORTH);
		}
		return panelCentral;
	}
	private JPanel getPanel_Central() {
		if (panel_Central == null) {
			panel_Central = new JPanel();
			panel_Central.setLayout(new BorderLayout(0, 0));
			panel_Central.add(getPanel_Central_Contenido(), BorderLayout.CENTER);
		}
		return panel_Central;
	}
	private JLabel getLbl_medico() {
		if (lbl_medico == null) {
			lbl_medico = new JLabel("Seleccione el m\u00E9dico:");
			lbl_medico.setBounds(59, 107, 241, 31);
			lbl_medico.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return lbl_medico;
	}
	private JButton getBtnSiguiente() {
		if (btnSiguiente == null) {
			btnSiguiente = new JButton("Siguiente");
			btnSiguiente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(medico == null)
					{
						JOptionPane.showMessageDialog(null, "Debes seleccionar un m�dico antes de continuar");
					}else {
	//					Medico medico = cb_medico.getModel().getElementAt(cb_medico.getSelectedIndex());
						mostrarVentanaOperaciones(medico);
					}
				}
			});
			btnSiguiente.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return btnSiguiente;
	}
	
	private void mostrarVentanaOperaciones(Medico medico) {
		VentanaOperacionesMedico voa = new VentanaOperacionesMedico(medico);
		voa.setLocationRelativeTo(this);
		voa.setModal(true);
		voa.setVisible(true);
	}
	
	private JPanel getPanel_Inferior() {
		if (panel_Inferior == null) {
			panel_Inferior = new JPanel();
			panel_Inferior.setLayout(new BorderLayout(0, 0));
			panel_Inferior.add(getPanel_Derecha_Abajo(), BorderLayout.EAST);
		}
		return panel_Inferior;
	}
	private JButton getBtnAtrs() {
		if (btnAtrs == null) {
			btnAtrs = new JButton("Atr\u00E1s");
			btnAtrs.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					mostrarVentanaAnterior();
				}
			});
			btnAtrs.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return btnAtrs;
	}
	
	private void mostrarVentanaAnterior() {
		this.setVisible(false);
	}
	
	private JPanel getPanel_Derecha_Abajo() {
		if (panel_Derecha_Abajo == null) {
			panel_Derecha_Abajo = new JPanel();
			panel_Derecha_Abajo.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			panel_Derecha_Abajo.add(getBtnSiguiente());
			panel_Derecha_Abajo.add(getBtnAtrs());
		}
		return panel_Derecha_Abajo;
	}
	private JPanel getPanel_Central_Contenido() {
		if (panel_Central_Contenido == null) {
			panel_Central_Contenido = new JPanel();
			panel_Central_Contenido.setLayout(null);
			panel_Central_Contenido.add(getLbl_medico());
			panel_Central_Contenido.add(getBtSeleccionarMedico());
			panel_Central_Contenido.add(getTxMedicoSeleccionado());
		}
		return panel_Central_Contenido;
	}
	private JButton getBtSeleccionarMedico() {
		if (btSeleccionarMedico == null) {
			btSeleccionarMedico = new JButton("Seleccionar M\u00E9dico...");
			VentanaBuscadorMedicos ventana = new VentanaBuscadorMedicos(getTxMedicoSeleccionado(),this);
			btSeleccionarMedico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentana(ventana);
				}
			});
			btSeleccionarMedico.setFont(new Font("Tahoma", Font.PLAIN, 20));
			btSeleccionarMedico.setBounds(59, 151, 246, 31);
		}
		return btSeleccionarMedico;
	}
	private void mostrarVentana(JDialog ventana) {
		ventana.setLocationRelativeTo(this);
		ventana.setModal(true);
		ventana.setVisible(true);
	}
	private JTextField getTxMedicoSeleccionado() {
		if (txMedicoSeleccionado == null) {
			txMedicoSeleccionado = new JTextField();
			txMedicoSeleccionado.setEditable(false);
			txMedicoSeleccionado.setBounds(342, 131, 303, 31);
			txMedicoSeleccionado.setColumns(10);
		}
		return txMedicoSeleccionado;
	}
	@Override
	public void setMedico(Medico medico) {
		this.medico = medico;
	}
}