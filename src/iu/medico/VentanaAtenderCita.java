package iu.medico;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logica.OperacionesDiagnosticos;
import logica.Consulta;
import logica.Diagnostico;
import logica.OperacionesCita;
import logica.OperacionesProcedimientos;
import logica.Procedimiento;

import java.awt.BorderLayout;

import javax.swing.border.TitledBorder;
import javax.swing.table.TableModel;

import iu.ModeloNoEditable;
import iu.administrativo.VentanaOperacionesAdministrativo;

import javax.swing.UIManager;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JRadioButton;

public class VentanaAtenderCita extends JDialog {

	private static final long serialVersionUID = 1L;
	private DefaultListModel<String> model = new DefaultListModel<String>();
	private JPanel panelGeneral;
	private JPanel pnCausasCita;
	private JPanel pnProcedimientos;
	private JPanel pnDiagnosticos;
	private JPanel pnPrescripcion;
	private JPanel pnDatosPaciente;
	private JLabel lbNombrePaciente;
	private JLabel lbApellidosPaciente;
	private JLabel lblDniPaciente;
	private JTextField txNombrePaciente;
	private JTextField txApellidoPaciente;
	private JTextField txDniPaciente;
	private JLabel lbFechaCita;
	private JLabel lblHoraInicioCita;
	private JLabel lbHoraFinCita;
	private JTextField txFechaCita;
	private JTextField txHoraInicioCita;
	private JTextField txHoraFinCita;
	private JTextArea taCausasCita;
	private JScrollPane scrollCausasCita;
	private JButton btnFinalizarCita;
	private ModeloNoEditable modeloTabla;

	private String idCita;
	private JPanel pnBotones;
	private JPanel panelBoxLayout;
	private JButton btnCancelar;
	private JScrollPane scrollPrescripcion;
	private JTextArea taPrescripcion;
	private JLabel lbProcedimientos;
	private JList<String> listProcedimientos;
	private JComboBox<String> cbSeccion;
	private JComboBox<String> cbSistemaOrganico;
	private JButton btnA�adir;
	private JButton btnEliminar;
	private JLabel lbDiagnosticos;
	private JLabel lbBusqueda;
	private JComboBox<String> cbBusqueda;
	private JTextField txtBusqueda;
	private JPanel pnTablaBusqueda;
	private JScrollPane scBusqueda;
	private JTable tableBusqueda;
	private JButton btBuscar;
	private JButton btnAdd;

	private Set<Diagnostico> diagnosticos = new HashSet<Diagnostico>();
	private JButton btnDelete;
	private JPanel pnBotonesDiagnosticos;
	private JRadioButton rdCodigo;
	private JRadioButton rdTexto;
	private ButtonGroup grupo = new ButtonGroup();
	private JLabel lbBuscarProcedimientos;
	private JTextField txProcedimientosBuscar;
	private JButton btBuscarProcedimientos;

	public VentanaAtenderCita(String nombrePaciente, String apellidoPaciente, String dniPaciente, String fechaCita,
			String horaInicioCita, String horaFinCita, String idCita) {
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(VentanaOperacionesAdministrativo.class.getResource("/iu/img/logo.jpeg")));
		setTitle("Atender cita");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1338, 760);
		panelGeneral = new JPanel();
		panelGeneral.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panelGeneral);
		panelGeneral.setLayout(new BorderLayout(0, 0));
		panelGeneral.add(getPnBotones(), BorderLayout.SOUTH);
		panelGeneral.add(getPanelBoxLayout());
		grupo.add(rdCodigo);
		grupo.add(rdTexto);

		meterValoresDefecto(nombrePaciente, apellidoPaciente, dniPaciente, fechaCita, horaInicioCita, horaFinCita);
		this.idCita = idCita;
	}

	private void meterValoresDefecto(String nombrePaciente, String apellidoPaciente, String dniPaciente,
			String fechaCita, String horaInicioCita, String horaFinCita) {
		getTxNombrePaciente().setText(nombrePaciente);
		getTxApellidoPaciente().setText(apellidoPaciente);
		getTxDniPaciente().setText(dniPaciente);
		getTxFechaCita().setText(fechaCita);
		getTxHoraInicioCita().setText(horaInicioCita);
		getTxHoraFinCita().setText(horaFinCita);
	}

	private JPanel getPnCausasCita() {
		if (pnCausasCita == null) {
			pnCausasCita = new JPanel();
			pnCausasCita.setLayout(null);
			pnCausasCita.add(getScrollCausasCita());
		}
		return pnCausasCita;
	}

	private JPanel getPnProcedimientos() {
		if (pnProcedimientos == null) {
			pnProcedimientos = new JPanel();
			pnProcedimientos.setLayout(null);
			pnProcedimientos.add(getLbDiagnosticos());
			pnProcedimientos.add(getLbBusqueda());
			pnProcedimientos.add(getCbBusqueda());
			pnProcedimientos.add(getTxtBusqueda());
			pnProcedimientos.add(getPnTablaBusqueda());
			pnProcedimientos.add(getPnBotonesDiagnosticos());
		}
		return pnProcedimientos;
	}

	private JPanel getPnDiagnosticos() {
		if (pnDiagnosticos == null) {
			pnDiagnosticos = new JPanel();
			pnDiagnosticos.setLayout(null);
			pnDiagnosticos.add(getLbProcedimientos());
			pnDiagnosticos.add(getListProcedimientos());
			pnDiagnosticos.add(getCbSeccion());
			pnDiagnosticos.add(getCbSistemaOrganico());
			pnDiagnosticos.add(getBtnA�adir());
			pnDiagnosticos.add(getBtnEliminar());
			pnDiagnosticos.add(getRdCodigo());
			pnDiagnosticos.add(getRdTexto());
			pnDiagnosticos.add(getLbBuscarProcedimientos());
			pnDiagnosticos.add(getTxProcedimientosBuscar());
			pnDiagnosticos.add(getBtBuscarProcedimientos());
		}
		return pnDiagnosticos;
	}

	private JPanel getPnPrescripcion() {
		if (pnPrescripcion == null) {
			pnPrescripcion = new JPanel();
			pnPrescripcion.setLayout(null);
			pnPrescripcion.add(getScrollPrescripcion());
		}
		return pnPrescripcion;
	}

	private JPanel getPnDatosPaciente() {
		if (pnDatosPaciente == null) {
			pnDatosPaciente = new JPanel();
			pnDatosPaciente.setLayout(new GridLayout(3, 4, 0, 0));
			pnDatosPaciente.add(getLbNombrePaciente());
			pnDatosPaciente.add(getTxNombrePaciente());
			pnDatosPaciente.add(getLbFechaCita());
			pnDatosPaciente.add(getTxFechaCita());
			pnDatosPaciente.add(getLbApellidosPaciente());
			pnDatosPaciente.add(getTxApellidoPaciente());
			pnDatosPaciente.add(getLblHoraInicioCita());
			pnDatosPaciente.add(getTxHoraInicioCita());
			pnDatosPaciente.add(getLblDniPaciente());
			pnDatosPaciente.add(getTxDniPaciente());
			pnDatosPaciente.add(getLbHoraFinCita());
			pnDatosPaciente.add(getTxHoraFinCita());
		}
		return pnDatosPaciente;
	}

	private JLabel getLbNombrePaciente() {
		if (lbNombrePaciente == null) {
			lbNombrePaciente = new JLabel("Nombre Paciente:");
			lbNombrePaciente.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbNombrePaciente.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbNombrePaciente;
	}

	private JLabel getLbApellidosPaciente() {
		if (lbApellidosPaciente == null) {
			lbApellidosPaciente = new JLabel("Apellidos Paciente:");
			lbApellidosPaciente.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbApellidosPaciente.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbApellidosPaciente;
	}

	private JLabel getLblDniPaciente() {
		if (lblDniPaciente == null) {
			lblDniPaciente = new JLabel("DNI Paciente:");
			lblDniPaciente.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblDniPaciente.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblDniPaciente;
	}

	private JTextField getTxNombrePaciente() {
		if (txNombrePaciente == null) {
			txNombrePaciente = new JTextField();
			txNombrePaciente.setEditable(false);
			txNombrePaciente.setHorizontalAlignment(SwingConstants.CENTER);
			txNombrePaciente.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txNombrePaciente.setColumns(10);
		}
		return txNombrePaciente;
	}

	private JTextField getTxApellidoPaciente() {
		if (txApellidoPaciente == null) {
			txApellidoPaciente = new JTextField();
			txApellidoPaciente.setEditable(false);
			txApellidoPaciente.setHorizontalAlignment(SwingConstants.CENTER);
			txApellidoPaciente.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txApellidoPaciente.setColumns(10);
		}
		return txApellidoPaciente;
	}

	private JTextField getTxDniPaciente() {
		if (txDniPaciente == null) {
			txDniPaciente = new JTextField();
			txDniPaciente.setEditable(false);
			txDniPaciente.setHorizontalAlignment(SwingConstants.CENTER);
			txDniPaciente.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txDniPaciente.setColumns(10);
		}
		return txDniPaciente;
	}

	private JLabel getLbFechaCita() {
		if (lbFechaCita == null) {
			lbFechaCita = new JLabel("Fecha:");
			lbFechaCita.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbFechaCita.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbFechaCita;
	}

	private JLabel getLblHoraInicioCita() {
		if (lblHoraInicioCita == null) {
			lblHoraInicioCita = new JLabel("Hora Inicio Cita:");
			lblHoraInicioCita.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblHoraInicioCita.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblHoraInicioCita;
	}

	private JLabel getLbHoraFinCita() {
		if (lbHoraFinCita == null) {
			lbHoraFinCita = new JLabel("Hora Fin Cita:");
			lbHoraFinCita.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbHoraFinCita.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbHoraFinCita;
	}

	private JTextField getTxFechaCita() {
		if (txFechaCita == null) {
			txFechaCita = new JTextField();
			txFechaCita.setEditable(false);
			txFechaCita.setHorizontalAlignment(SwingConstants.CENTER);
			txFechaCita.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txFechaCita.setColumns(10);
		}
		return txFechaCita;
	}

	private JTextField getTxHoraInicioCita() {
		if (txHoraInicioCita == null) {
			txHoraInicioCita = new JTextField();
			txHoraInicioCita.setEditable(false);
			txHoraInicioCita.setHorizontalAlignment(SwingConstants.CENTER);
			txHoraInicioCita.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txHoraInicioCita.setColumns(10);
		}
		return txHoraInicioCita;
	}

	private JTextField getTxHoraFinCita() {
		if (txHoraFinCita == null) {
			txHoraFinCita = new JTextField();
			txHoraFinCita.setEditable(false);
			txHoraFinCita.setHorizontalAlignment(SwingConstants.CENTER);
			txHoraFinCita.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txHoraFinCita.setColumns(10);
		}
		return txHoraFinCita;
	}

	private JTextArea getTaCausasCita() {
		if (taCausasCita == null) {
			taCausasCita = new JTextArea();
			taCausasCita.setLineWrap(true);
			taCausasCita.setWrapStyleWord(true);
			taCausasCita.setBounds(12, 43, 1298, 85);
		}
		return taCausasCita;
	}

	private JScrollPane getScrollCausasCita() {
		if (scrollCausasCita == null) {
			scrollCausasCita = new JScrollPane();
			scrollCausasCita.setViewportBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
					"Causas por las que el paciente acudi\u00F3 a la cita:", TitledBorder.LEADING, TitledBorder.TOP,
					null, new Color(0, 0, 0)));
			scrollCausasCita.setBounds(10, 11, 1288, 85);
			scrollCausasCita.setViewportView(getTaCausasCita());
		}
		return scrollCausasCita;
	}

	private JButton getBtnFinalizarCita() {
		if (btnFinalizarCita == null) {
			btnFinalizarCita = new JButton("Finalizar Cita");
			btnFinalizarCita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String causas = getCausas();
					String prescripcion = getPrescripcion();
					Consulta.insertarConsulta(idCita, causas, prescripcion);

					for (Diagnostico d : diagnosticos)
						OperacionesDiagnosticos.insertarDiagnostico(d.getCodigo(), Consulta.getId(),
								getTxFechaCita().getText());

					int numeroProcedimientos = model.getSize();
					ArrayList<Procedimiento> procedimientos = new ArrayList<Procedimiento>();
					for (int i = 0; i < numeroProcedimientos; i++) {
						String temp = model.getElementAt(i);
						Procedimiento proc = new Procedimiento(temp.substring(0, 2), temp.substring(3, temp.length()));
						procedimientos.add(proc);
					}
					for (Procedimiento p : procedimientos)
						OperacionesProcedimientos.insertarProcedimiento(p.getCodigo(), Consulta.getId(),
								p.getDescripcion(), getTxFechaCita().getText());
					JOptionPane.showMessageDialog(null, "Se han a�adido los nuevos datos al historial");

					OperacionesCita.actualizarEstadoCita(idCita);
					cerrarVentana();
				}
			});
			btnFinalizarCita.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return btnFinalizarCita;
	}

	private void cerrarVentana() {
		this.dispose();
	}

	private String getCausas() {
		String resultado = getTxFechaCita().getText();
		String causas = getTaCausasCita().getText();

		if (causas.trim().equals("")) {
			causas = "No se estableci� ninguna causa.";
		}

		resultado += ": " + causas;
		return resultado;
	}

	private String getPrescripcion() {
		String fecha = getTxFechaCita().getText();
		String prescripcion = getTaPrescripcion().getText();

		if (prescripcion.trim().equals(""))
			prescripcion = "No se prescribi� ning�n medicamento.";

		return fecha + ": " + prescripcion;

	}

	private JPanel getPnBotones() {
		if (pnBotones == null) {
			pnBotones = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnBotones.getLayout();
			flowLayout.setAlignment(FlowLayout.RIGHT);
			pnBotones.add(getBtnFinalizarCita());
			pnBotones.add(getBtnCancelar());
		}
		return pnBotones;
	}

	private JPanel getPanelBoxLayout() {
		if (panelBoxLayout == null) {
			panelBoxLayout = new JPanel();
			panelBoxLayout.setLayout(new BoxLayout(panelBoxLayout, BoxLayout.Y_AXIS));
			panelBoxLayout.add(getPnDatosPaciente());
			panelBoxLayout.add(getPnCausasCita());
			panelBoxLayout.add(getPnProcedimientos());
			panelBoxLayout.add(getPnDiagnosticos());
			panelBoxLayout.add(getPnPrescripcion());
		}
		return panelBoxLayout;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					setVisible(false);
				}
			});
			btnCancelar.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return btnCancelar;
	}

	private JScrollPane getScrollPrescripcion() {
		if (scrollPrescripcion == null) {
			scrollPrescripcion = new JScrollPane();
			scrollPrescripcion.setViewportBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
					"Prescripci\u00F3n:", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			scrollPrescripcion.setBounds(0, 0, 1312, 85);
			scrollPrescripcion.add(getTaPrescripcion());
			scrollPrescripcion.setViewportView(getTaPrescripcion());
		}
		return scrollPrescripcion;
	}

	private JTextArea getTaPrescripcion() {
		if (taPrescripcion == null) {
			taPrescripcion = new JTextArea();
			taPrescripcion.setWrapStyleWord(true);
			taPrescripcion.setLineWrap(true);
			taPrescripcion.setBounds(0, 0, 1274, 60);
		}
		return taPrescripcion;
	}

	private JLabel getLbProcedimientos() {
		if (lbProcedimientos == null) {
			lbProcedimientos = new JLabel("Procedimientos");
			lbProcedimientos.setFont(new Font("Tahoma", Font.PLAIN, 23));
			lbProcedimientos.setBounds(31, 11, 153, 34);
		}
		return lbProcedimientos;
	}

	private JList<String> getListProcedimientos() {
		if (listProcedimientos == null) {
			listProcedimientos = new JList<String>(model);
			listProcedimientos.setBounds(337, 53, 394, 70);
		}
		return listProcedimientos;
	}

	private JComboBox<String> getCbSeccion() {
		if (cbSeccion == null) {
			cbSeccion = new JComboBox<String>();
			cbSeccion.setFont(new Font("Tahoma", Font.PLAIN, 10));
			cbSeccion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String cadena = (String) cbSeccion.getSelectedItem();
					cbSistemaOrganico.setEnabled(true);
					btnA�adir.setEnabled(false);
					rellenarComboBoxProcedimientos(cadena);
				}
			});
			cbSeccion.setModel(new DefaultComboBoxModel<String>(new String[] { "0 M\u00E9dico-Quir\u00FArgica",
					"1 Obstetricia", "2 Colocaci\u00F3n", "3 Administraci\u00F3n",
					"4 Medici\u00F3n y Monitorizaci\u00F3n", "5 Asistencia y Soporte Extracorp\u00F3reos",
					"6 Terapias Extracorp\u00F3reas", "7 Osteop\u00E1tico", "8 Otros Procedimientos",
					"9 Quiropr\u00E1ctica", "B Imagen", "C Medicina Nuclear", "D Radioterapia",
					"F Rehabilitaci\u00F3n F\u00EDsica y Audiolog\u00EDa Diagn\u00F3stica", "G Salud Mental",
					"H Tratamiento de Abuso de Sustancias", "X Nueva Tecnolog\u00EDa" }));
			cbSeccion.setBounds(205, 11, 241, 31);
		}
		return cbSeccion;
	}

	private void rellenarComboBoxProcedimientos(String cadena) {
		switch (cadena) {
		case "0 M\u00E9dico-Quir\u00FArgica":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(new String[] { "00 Sistema Nervioso Central",
					"01 Sistema Nervioso Perif\u00E9rico", "02 Coraz\u00F3n y Grandes Vasos", "03 Arterias Superiores",
					"04 Arterias Inferiores", "05 Venas Superiores", "06 Venas Inferiores",
					"07 Sistemas Linf\u00E1tico y Hem\u00E1tico", "08 Ojo", "09 O\u00EDdo, Nariz, Senos Paranasales",
					"0B Sistema Respiratorio", "0C Boca y Garganta", "0D Sistema Gastrointestinal",
					"0F Sistema Hepatobiliar y P\u00E1ncreas", "0G Sistema Endocrino", "0H Piel y Mama",
					"0J Tejido Subcut\u00E1neo y Fascia", "0K M\u00FAsculos", "0L Tendones", "0M Bursas y Ligamentos",
					"0N Huesos Cr\u00E1neo y Cara", "0P Huesos Superiores", "0Q Huesos Inferiores",
					"0R Articulaciones Superiores", "0S Articulaciones Inferiores", "0T Sistema Urinario",
					"0U Sistema Reproductor Femenino", "0V Sistema Reproductor Masculino",
					"0W Regiones Anat\u00F3micas Generales", "0X Regiones Anat\u00F3micas, Extremidades Superiores",
					"0Y Regiones Anat\u00F3micas, Extremidades Inferiores" }));
			break;
		case "1 Obstetricia":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(new String[] { "10 Embarazo" }));
			break;
		case "2 Colocaci\u00F3n":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(
					new String[] { "2W Regiones Anat\u00F3micas", "2Y Orificios Anat\u00F3micos" }));
			break;
		case "3 Administraci\u00F3n":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(new String[] { "30 Circulatorio",
					"3C Dispositivo Permanente", "3E Sistemas Fisiol\u00F3gicos y Regiones Anat\u00F3micas" }));
			break;
		case "4 Medici\u00F3n y Monitorizaci\u00F3n":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(
					new String[] { "4A Sistemas Fisiol\u00F3gicos", "4B Dispositivos Fisiol\u00F3gicos" }));
			break;
		case "5 Asistencia y Soporte Extracorp\u00F3reos":
			cbSistemaOrganico
					.setModel(new DefaultComboBoxModel<String>(new String[] { "5A Sistemas Fisiol\u00F3gicos" }));
			break;
		case "6 Terapias Extracorp\u00F3reas":
			cbSistemaOrganico
					.setModel(new DefaultComboBoxModel<String>(new String[] { "6A Sistemas Fisiol\u00F3gicos" }));
			break;
		case "7 Osteop\u00E1tico":
			cbSistemaOrganico
					.setModel(new DefaultComboBoxModel<String>(new String[] { "7W Regiones Anat\u00F3micas" }));
			break;
		case "8 Otros Procedimientos":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(new String[] { "8C Dispositivo Permanente",
					"8E Sistemas Fisiol\u00F3gicos y Regiones Anat\u00F3micas" }));
			break;
		case "9 Quiropr\u00E1ctica":
			cbSistemaOrganico
					.setModel(new DefaultComboBoxModel<String>(new String[] { "9W Regiones Anat\u00F3micas" }));
			break;
		case "B Imagen":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(new String[] { "B0 Sistema Nervioso Central",
					"B2 Coraz\u00F3n", "B3 Arterias Superiores", "B4 Arterias Inferiores", "B5 Venas",
					"B7 Sistema Linf\u00E1tico", "B8 Ojo", "B9 O\u00EDdo, Nariz, Boca y Garganta",
					"BB Sistema Respiratorio", "BD Sistema Gastrointestinal", "BF Sistema Hepatobiliar y P\u00E1ncreas",
					"BG Sistema Endocrino", "BH Piel, Tejido Subcut\u00E1neo y Mama", "BL Tejido Conectivo",
					"BN Huesos Cr\u00E1neo y Cara", "BP Huesos Superiores no Axiales",
					"BQ Huesos Inferiores no Axiales", "BR Esqueleto Axial, Excepto Huesos Craneales y Faciales",
					"BT Sistema Urinario", "BU Sistema Reproductor Femenino", "BV Sistema Reproductor Masculino",
					"BW Regiones Anat\u00F3micas", "BY Feto y Obstetricia" }));
			break;
		case "C Medicina Nuclear":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(new String[] { "C0 Sistema Nervioso Central",
					"C2 Coraz\u00F3n", "C5 Venas", "C7 Sistema Linf\u00E1tico y Hem\u00E1tico", "C8 Ojo",
					"C9 O\u00EDdo, Nariz, Boca y Garganta", "CB Sistema Respiratorio", "CD Sistema Gastrointestinal",
					"CF Sistema Hepatobiliar y P\u00E1ncreas", "CG Sistema Endocrino",
					"CH Piel, Tejido Subcut\u00E1neo y Mama", "CP Sistema M\u00FAsculo Esquel\u00E9tico",
					"CT Sistema Urinario", "CV Sistema Reproductor Masculino", "CW Regiones Anat\u00F3micas" }));
			break;
		case "D Radioterapia":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(new String[] {
					"D0 Sistema Nervioso Central y Perif\u00E9rico", "D7 Sistema Linf\u00E1tico y Hem\u00E1tico",
					"D8 Ojo", "D9 O\u00EDdo, Nariz, Boca y Garganta", "DB Sistema Respiratorio",
					"DD Sistema Gastrointestinal", "DF Sistema Hepatobiliar y P\u00E1ncreas", "DG Sistema Endocrino",
					"DH Piel", "DM Mama", "DP Sistema M\u00FAsculo Esquel\u00E9tico", "DT Sistema Urinario",
					"DU Sistema Reproductor Femenino", "DV Sistema Reproductor Masculino",
					"DW Regiones Anat\u00F3micas" }));
			break;
		case "F Rehabilitaci\u00F3n F\u00EDsica y Audiolog\u00EDa Diagn\u00F3stica":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(
					new String[] { "F0 Rehabilitaci\u00F3n", "F1 Audiolog\u00EDa Diagn\u00F3stica" }));
			break;
		case "G Salud Mental":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(new String[] { "GZ Ninguno(-a)" }));
			break;
		case "H Tratamiento de Abuso de Sustancias":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(new String[] { "HZ Ninguno(-a)" }));
			break;
		case "X Nueva Tecnolog\u00EDa":
			cbSistemaOrganico.setModel(new DefaultComboBoxModel<String>(
					new String[] { "X2 Sistema Cardiovascular", "XH Piel, Tejido Subcut\u00E1neo, Fascia y Mama",
							"XN Huesos", "XR Articulaciones", "XW Regiones Anat\u00F3micas" }));
			break;
		}
	}

	private JComboBox<String> getCbSistemaOrganico() {
		if (cbSistemaOrganico == null) {
			cbSistemaOrganico = new JComboBox<String>();
			cbSistemaOrganico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnA�adir.setEnabled(true);
				}
			});
			cbSistemaOrganico.setEnabled(false);
			cbSistemaOrganico.setBounds(490, 11, 241, 31);
		}
		return cbSistemaOrganico;
	}

	private JButton getBtnA�adir() {
		if (btnA�adir == null) {
			btnA�adir = new JButton("A\u00F1adir");
			btnA�adir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String codigo = (String) cbSistemaOrganico.getSelectedItem();
					model.addElement(codigo);
				}
			});
			btnA�adir.setEnabled(false);
			btnA�adir.setBounds(741, 46, 89, 31);
		}
		return btnA�adir;
	}

	private JButton getBtnEliminar() {
		if (btnEliminar == null) {
			btnEliminar = new JButton("Eliminar");
			btnEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (listProcedimientos.getSelectedIndex() != -1)
						model.remove(listProcedimientos.getSelectedIndex());
				}
			});
			btnEliminar.setBounds(741, 92, 89, 31);
		}
		return btnEliminar;
	}

	private JLabel getLbDiagnosticos() {
		if (lbDiagnosticos == null) {
			lbDiagnosticos = new JLabel("Diagn\u00F3sticos");
			lbDiagnosticos.setBounds(31, 11, 153, 28);
			lbDiagnosticos.setFont(new Font("Tahoma", Font.PLAIN, 23));
		}
		return lbDiagnosticos;
	}

	private JLabel getLbBusqueda() {
		if (lbBusqueda == null) {
			lbBusqueda = new JLabel("Buscar por: ");
			lbBusqueda.setBounds(55, 75, 74, 17);
		}
		return lbBusqueda;
	}

	private JComboBox<String> getCbBusqueda() {
		if (cbBusqueda == null) {
			cbBusqueda = new JComboBox<String>();
			cbBusqueda
					.setModel(new DefaultComboBoxModel<String>(new String[] { "Descripci�n", "C�digo", "Subsecci�n" }));
			cbBusqueda.setBounds(157, 50, 153, 20);
		}
		return cbBusqueda;
	}

	private JTextField getTxtBusqueda() {
		if (txtBusqueda == null) {
			txtBusqueda = new JTextField();
			txtBusqueda.setBounds(157, 73, 153, 20);
			txtBusqueda.setColumns(10);
		}
		return txtBusqueda;
	}

	private JPanel getPnTablaBusqueda() {
		if (pnTablaBusqueda == null) {
			pnTablaBusqueda = new JPanel();
			pnTablaBusqueda.setBounds(320, 11, 468, 114);
			pnTablaBusqueda.add(getScBusqueda());
		}
		return pnTablaBusqueda;
	}

	private JScrollPane getScBusqueda() {
		if (scBusqueda == null) {
			scBusqueda = new JScrollPane();
			scBusqueda.setViewportView(getTableBusqueda());
		}
		return scBusqueda;
	}

	private JTable getTableBusqueda() {
		if (tableBusqueda == null) {
			tableBusqueda = new JTable();
			scBusqueda.setViewportView(tableBusqueda);
			String[] columnas = { "C�digo", "Descripci�n", "Subsecci�n" };
			modeloTabla = new ModeloNoEditable(columnas, 0);
			tableBusqueda = new JTable(modeloTabla);
			tableBusqueda.getTableHeader().setReorderingAllowed(false);
			tableBusqueda.setRowHeight(20);
			tableBusqueda.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(20);

		}
		return tableBusqueda;
	}

	private JButton getBtBuscar() {
		if (btBuscar == null) {
			btBuscar = new JButton("Buscar");
			btBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					eliminarFilasTabla();
					a�adirFilas();
				}
			});

		}
		return btBuscar;
	}

	private void eliminarFilasTabla() {
		if (modeloTabla.getRowCount() > 0) {
			while (modeloTabla.getRowCount() > 0)
				modeloTabla.removeRow(0);
		}
	}

	private void a�adirFilas() {

		Object[] nuevaFila = new Object[3];
		List<Diagnostico> diagnosticosBuscados = null;
		String texto = getTxtBusqueda().getText();
		if (!texto.trim().equals("")) {
			if (getCbBusqueda().getSelectedItem().equals("C�digo")) {
				diagnosticosBuscados = OperacionesDiagnosticos.buscarPorCodigo(formatoSQL(texto));
			} else if (getCbBusqueda().getSelectedItem().equals("Descripci�n")) {
				diagnosticosBuscados = OperacionesDiagnosticos.buscarPorDescripcion(formatoSQL(texto));
			} else if (getCbBusqueda().getSelectedItem().equals("Subsecci�n")) {
				diagnosticosBuscados = OperacionesDiagnosticos.buscarPorSubseccion(formatoSQL(texto));
			}

			for (Diagnostico d : diagnosticosBuscados) {
				nuevaFila[0] = d.getCodigo();
				nuevaFila[1] = d.getDescripcion();
				nuevaFila[2] = d.getSubseccion();

				modeloTabla.addRow(nuevaFila);
			}
		}
	}

	private String formatoSQL(String texto) {

		char[] c = texto.toCharArray();
		c[0] = Character.toUpperCase(c[0]);
		for (int i = 1; i < c.length; i++)
			c[i] = Character.toLowerCase(c[i]);
		String resultado = "";
		for (char ch : c)
			resultado += ch;
		return resultado;
	}

	private JButton getBtnAdd() {
		if (btnAdd == null) {
			btnAdd = new JButton("A\u00F1adir diagnostico");
			btnAdd.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					int numeroDeFila = getTableBusqueda().getSelectedRow();
					if (numeroDeFila != -1) {
						TableModel md = getTableBusqueda().getModel();
						String codigo = (String) md.getValueAt(numeroDeFila, 0);
						String prescripcion = (String) md.getValueAt(numeroDeFila, 1);
						String subseccion = null;
						if (md.getValueAt(numeroDeFila, 2) != null)
							subseccion = (String) md.getValueAt(numeroDeFila, 2);
						boolean exito = diagnosticos.add(new Diagnostico(codigo, prescripcion, subseccion));
						if (!exito)
							JOptionPane.showMessageDialog(btnAdd,
									"No se ha podido introducir el di�gnostico (ya se ha introducido previamente)");
					}
				}
			});
		}
		return btnAdd;
	}

	private JButton getBtnDelete() {
		if (btnDelete == null) {
			btnDelete = new JButton("Eliminar diagnostico");
			btnDelete.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					int numeroDeFila = getTableBusqueda().getSelectedRow();
					if (numeroDeFila != -1) {
						TableModel md = getTableBusqueda().getModel();
						String codigo = (String) md.getValueAt(numeroDeFila, 0);
						boolean exito = false;
						for (Diagnostico d : diagnosticos)
							if (d.getCodigo().equals(codigo)) {
								diagnosticos.remove(d);
								exito = true;
								break;
							}
						if (!exito)
							JOptionPane.showMessageDialog(btnAdd,
									"No se ha podido borrar el di�gnostico (no existe o ya se ha borrado previamente de los diagnosticos)");
					}
				}
			});
		}
		return btnDelete;
	}

	private JPanel getPnBotonesDiagnosticos() {
		if (pnBotonesDiagnosticos == null) {
			pnBotonesDiagnosticos = new JPanel();
			pnBotonesDiagnosticos.setBounds(798, 11, 153, 114);
			pnBotonesDiagnosticos.setLayout(new GridLayout(0, 1, 0, 0));
			pnBotonesDiagnosticos.add(getBtBuscar());
			pnBotonesDiagnosticos.add(getBtnAdd());
			pnBotonesDiagnosticos.add(getBtnDelete());
		}
		return pnBotonesDiagnosticos;
	}

	private JRadioButton getRdCodigo() {
		if (rdCodigo == null) {
			rdCodigo = new JRadioButton("Por c\u00F3digo");
			rdCodigo.setSelected(true);
			rdCodigo.setBounds(41, 96, 89, 23);
		}
		return rdCodigo;
	}

	private JRadioButton getRdTexto() {
		if (rdTexto == null) {
			rdTexto = new JRadioButton("Por Texto");
			rdTexto.setBounds(132, 96, 81, 23);
		}
		return rdTexto;
	}

	private JLabel getLbBuscarProcedimientos() {
		if (lbBuscarProcedimientos == null) {
			lbBuscarProcedimientos = new JLabel("Buscar por: ");
			lbBuscarProcedimientos.setBounds(41, 56, 74, 17);
		}
		return lbBuscarProcedimientos;
	}

	private JTextField getTxProcedimientosBuscar() {
		if (txProcedimientosBuscar == null) {
			txProcedimientosBuscar = new JTextField();
			txProcedimientosBuscar.setColumns(10);
			txProcedimientosBuscar.setBounds(109, 56, 218, 20);
		}
		return txProcedimientosBuscar;
	}

	private JButton getBtBuscarProcedimientos() {
		if (btBuscarProcedimientos == null) {
			btBuscarProcedimientos = new JButton("Buscar");
			btBuscarProcedimientos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (rdCodigo.isSelected() && txProcedimientosBuscar.getText().length() >= 2) {
						String busqueda = txProcedimientosBuscar.getText();
						String numero1 = busqueda.substring(0, 1);
						String numero2 = busqueda.substring(1, 2);
						ArrayList<String> secciones = new ArrayList<String>();
						int numeroItemsSeccion = cbSeccion.getItemCount();
						for (int i = 0; i < numeroItemsSeccion; i++) {
							secciones.add(cbSeccion.getItemAt(i));
						}
						for (int i = 0; i < secciones.size(); i++) {
							String temp = secciones.get(i);
							if (numero1.equals(temp.substring(0, 1))) {
								cbSeccion.setSelectedIndex(i);
								rellenarComboBoxProcedimientos(temp);
								btnA�adir.setEnabled(true);
							}
						}

						int numeroItemsSistema = cbSistemaOrganico.getItemCount();
						ArrayList<String> sistemas = new ArrayList<String>();
						for (int i = 0; i < numeroItemsSistema; i++) {
							sistemas.add(cbSistemaOrganico.getItemAt(i));
						}
						for (int i = 0; i < sistemas.size(); i++) {
							String temp = sistemas.get(i);
							if (numero2.equals(temp.substring(1, 2))) {
								cbSistemaOrganico.setSelectedIndex(i);
							}
						}
					} else if (rdTexto.isSelected()) {
						String busqueda = txProcedimientosBuscar.getText();
						ArrayList<String> secciones = new ArrayList<String>();
						int numeroItemsSeccion = cbSeccion.getItemCount();
						for (int i = 0; i < numeroItemsSeccion; i++) {
							secciones.add(cbSeccion.getItemAt(i));
						}
						for (int i = 0; i < secciones.size(); i++) {
							cbSeccion.setSelectedIndex(i);
							rellenarComboBoxProcedimientos((String) cbSeccion.getSelectedItem());
							ArrayList<String> sistemas = new ArrayList<String>();
							for (int j = 0; j < cbSistemaOrganico.getItemCount(); j++) {
								sistemas.add(cbSistemaOrganico.getItemAt(i));
							}
							for (int x = 0; x < cbSistemaOrganico.getItemCount(); x++) {
								if (cbSistemaOrganico.getItemAt(x).contains(busqueda)) {
									cbSistemaOrganico.setSelectedIndex(x);
									btnA�adir.setEnabled(true);
									return;
								}

							}
						}
					}
				}
			});
			btBuscarProcedimientos.setBounds(253, 85, 74, 38);
		}
		return btBuscarProcedimientos;
	}
}