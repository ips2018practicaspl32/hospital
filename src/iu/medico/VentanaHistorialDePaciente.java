package iu.medico;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import iu.administrativo.VentanaOperacionesAdministrativo;
import logica.Historial;
import logica.OperacionesPaciente;
import logica.Paciente;

public class VentanaHistorialDePaciente extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panel;
	private JPanel panel_Datos;
	private JLabel lblNombre;
	private JLabel lblApellido;
	private JLabel lblDni;
	private JTextArea taCausasHistorial;
	private JPanel panel_Botones;
	private JButton button;
	private JPanel panel_Titulo;
	private JLabel label_Historial;

	private String dniPaciente;
	private JTabbedPane panelPestanias;
	private JPanel panel_Procedimientos;
	private JPanel panel_Diagnostico;
	private JPanel panel_Prescripciones;
	private JPanel panel_Causas;
	private JScrollPane scCausas;
	
	private JScrollPane spPrescripciones;
	private JTextArea taPrescripciones;
	
	private Paciente p;
	private Historial h;
	private JScrollPane spDiagnosticos;
	private JTextArea taDiagnosticos;
	private JScrollPane spProcedimientos;
	private JTextArea taProcedimientos;
	
	
	/**
	 * Create the frame.
	 */
	public VentanaHistorialDePaciente(String dni, String nombre, String apellidos) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaOperacionesAdministrativo.class.getResource("/iu/img/logo.jpeg")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 624, 461);
		contentPane = new JPanel();
		dniPaciente = dni;
		p = OperacionesPaciente.getPacienteDni(dniPaciente);
		h = new Historial();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPanel(), BorderLayout.CENTER);
		contentPane.add(getPanel_Titulo(), BorderLayout.NORTH);
		getLblDni().setText(getLblDni().getText()+ " "+ dni);
		getLblNombre().setText(getLblNombre().getText()+ " "+ nombre);
		getLblApellido().setText(getLblApellido().getText()+ " "+ apellidos);
		
		
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new BorderLayout(0, 0));
			panel.add(getPanel_Datos(), BorderLayout.NORTH);
			panel.add(getPanel_Botones(), BorderLayout.SOUTH);
			panel.add(getPanelPestanias(), BorderLayout.CENTER);
		}
		return panel;
	}
	private JPanel getPanel_Datos() {
		if (panel_Datos == null) {
			panel_Datos = new JPanel();
			panel_Datos.setLayout(new GridLayout(0, 1, 0, 0));
			panel_Datos.add(getLblNombre());
			panel_Datos.add(getLblApellido());
			panel_Datos.add(getLblDni());
		}
		return panel_Datos;
	}
	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre: ");
			lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 15));
		}
		return lblNombre;
	}
	private JLabel getLblApellido() {
		if (lblApellido == null) {
			lblApellido = new JLabel("Apellido:");
			lblApellido.setFont(new Font("Tahoma", Font.PLAIN, 15));
		}
		return lblApellido;
	}
	private JLabel getLblDni() {
		if (lblDni == null) {
			lblDni = new JLabel("Dni:");
			lblDni.setFont(new Font("Tahoma", Font.PLAIN, 15));
		}
		return lblDni;
	}
	private JTextArea getTaCausasHistorial() { 
		if (taCausasHistorial == null) {
			taCausasHistorial = new JTextArea();
			taCausasHistorial.setWrapStyleWord(true);
			taCausasHistorial.setLineWrap(true);
			taCausasHistorial.setEditable(false);
//			ListarPacientes lp = new ListarPacientes();
			Paciente p = OperacionesPaciente.getPacienteDni(dniPaciente);
//			taCausasHistorial.setText(p.getHistorial());
			Historial h = new Historial();
			taCausasHistorial.setText(h.getCausas(p.getIdPaciente()));
		}
		return taCausasHistorial;
	}
	private JPanel getPanel_Botones() {
		if (panel_Botones == null) {
			panel_Botones = new JPanel();
			panel_Botones.setLayout(new FlowLayout(FlowLayout.RIGHT));
			panel_Botones.add(getButton());
		}
		return panel_Botones;
	}
	private JButton getButton() {
		if (button == null) {
			button = new JButton("Cerrar");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					cerrarVentana();
				}
			});
			button.setMnemonic('c');
			button.setActionCommand("Cancel");
		}
		return button;
	}
	
	private void cerrarVentana()
	{
		this.setVisible(false);
	}
	
	private JPanel getPanel_Titulo() {
		if (panel_Titulo == null) {
			panel_Titulo = new JPanel();
			panel_Titulo.setBorder(new EmptyBorder(5, 5, 5, 5));
			panel_Titulo.setLayout(new BorderLayout(0, 0));
			panel_Titulo.add(getLabel_Historial(), BorderLayout.NORTH);
		}
		return panel_Titulo;
	}
	private JLabel getLabel_Historial() {
		if (label_Historial == null) {
			label_Historial = new JLabel("HISTORIAL");
			label_Historial.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return label_Historial;
	}
	private JTabbedPane getPanelPestanias() {
		if (panelPestanias == null) {
			panelPestanias = new JTabbedPane(JTabbedPane.TOP);
			panelPestanias.addTab("Causas", null, getPanel_Causas(), null);
			panelPestanias.addTab("Procedimientos", null, getPanel_Procedimientos(), null);
			panelPestanias.addTab("Diagnůsticos", null, getPanel_Diagnostico(), null);
			panelPestanias.addTab("Prescripciones", null, getPanel_Prescripciones(), null);
		}
		return panelPestanias;
	}
	private JPanel getPanel_Procedimientos() {
		if (panel_Procedimientos == null) {
			panel_Procedimientos = new JPanel();
			panel_Procedimientos.setLayout(new BorderLayout(0, 0));
			panel_Procedimientos.add(getSpProcedimientos());
		}
		return panel_Procedimientos;
	}
	private JPanel getPanel_Diagnostico() {
		if (panel_Diagnostico == null) {
			panel_Diagnostico = new JPanel();
			panel_Diagnostico.setLayout(new BorderLayout(0, 0));
			panel_Diagnostico.add(getScrollPane_1());
		}
		return panel_Diagnostico;
	}
	private JPanel getPanel_Prescripciones() {
		if (panel_Prescripciones == null) {
			panel_Prescripciones = new JPanel();
			panel_Prescripciones.setLayout(new BorderLayout(0, 0));
			panel_Prescripciones.add(getSpPrescripciones());
		}
		return panel_Prescripciones;
	}
	private JPanel getPanel_Causas() {
		if (panel_Causas == null) {
			panel_Causas = new JPanel();
			panel_Causas.setLayout(new BorderLayout(0, 0));
			panel_Causas.add(getScCausas(), BorderLayout.CENTER);
		}
		return panel_Causas;
	}
	private JScrollPane getScCausas() {
		if (scCausas == null) {
			scCausas = new JScrollPane();
			scCausas.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scCausas.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
			scCausas.setViewportView(getTaCausasHistorial());
		}
		return scCausas;
	}
	private JScrollPane getSpPrescripciones() {
		if (spPrescripciones == null) {
			spPrescripciones = new JScrollPane();
			spPrescripciones.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			spPrescripciones.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
			spPrescripciones.setViewportView(getTaPrescripciones());
		}
		return spPrescripciones;
	}
	private JTextArea getTaPrescripciones() {
		if (taPrescripciones == null) {
			taPrescripciones = new JTextArea();
			taPrescripciones.setWrapStyleWord(true);
			taPrescripciones.setText("");
			taPrescripciones.setLineWrap(true);
			taPrescripciones.setEditable(false);
			taPrescripciones.setText(h.getPrescripciones(p.getIdPaciente()));
		}
		return taPrescripciones;
	}
	private JScrollPane getScrollPane_1() {
		if (spDiagnosticos == null) {
			spDiagnosticos = new JScrollPane();
			spDiagnosticos.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			spDiagnosticos.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
			spDiagnosticos.add(getTaDiagnosticos());
			spDiagnosticos.setViewportView(getTaDiagnosticos());
		}
		return spDiagnosticos;
	}
	private JTextArea getTaDiagnosticos() {
		if (taDiagnosticos == null) {
			taDiagnosticos = new JTextArea();
			taDiagnosticos.setEditable(false);
			//hacer consulta a la tabla citas, luego a la tabla consultas y luego a la tabla diagnosticos
			taDiagnosticos.setText(h.getDiagnosticos(p.getIdPaciente()));
		}
		return taDiagnosticos;
	}
	private JScrollPane getSpProcedimientos() {
		if (spProcedimientos == null) {
			spProcedimientos = new JScrollPane();
			spProcedimientos.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			spProcedimientos.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
			spProcedimientos.add(getTaProcedimientos());
			spProcedimientos.setViewportView(getTaProcedimientos());
		}
		return spProcedimientos;
	}
	private JTextArea getTaProcedimientos() {
		if (taProcedimientos == null) {
			taProcedimientos = new JTextArea();
			taProcedimientos.setEditable(false);
			//hacer consulta a la tabla citas, luego a la tabla consultas y luego a la tabla diagnosticos
			taProcedimientos.setText(h.getProcedimientos(p.getIdPaciente()));
		}
		return taProcedimientos;
	}
}