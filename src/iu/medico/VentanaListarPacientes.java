	package iu.medico;

	import java.awt.BorderLayout;
	import javax.swing.JDialog;
	import javax.swing.JPanel;
	import javax.swing.border.EmptyBorder;
	import javax.swing.table.TableModel;

import iu.ModeloNoEditable;
import iu.administrativo.VentanaOperacionesAdministrativo;

import javax.swing.JLabel;
	import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.DefaultComboBoxModel;
	import javax.swing.JComboBox;

	import logica.Cita;
	import logica.ListarCitas;
	import logica.ListarPacientes;
	import logica.Medico;
	import logica.Paciente;
	//import logica.Trabajadores;


	import javax.swing.JTable;
	import javax.swing.JScrollPane;
	import javax.swing.JButton;
	import java.awt.event.ActionListener;
	import java.util.Calendar;
	import java.util.GregorianCalendar;
	import java.util.List;
	import java.awt.event.ActionEvent;

	public class VentanaListarPacientes extends JDialog {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		//private Trabajadores trabajadores;
		private JPanel panelPrincipal;
		private JPanel panelSeleccionarMedico;
		private JLabel lblCitas;
		private JPanel panelTable;
		private JPanel panelListado;
		private JTable table_Citas;
		private JScrollPane scrollPane;
		private ModeloNoEditable modeloTabla;
		private JPanel panelOpciones;
		private JButton btnVerHistorial;
		private JPanel panelCbMedico;
		private JPanel panelOpMedico;
		private JLabel lblDia;
		private JLabel lblMes;
		private JComboBox<Integer> cb_Dia;
		private JComboBox<Integer> cb_Mes;
		private JLabel lblAo;
		private JComboBox<Integer> cb_Anio;
		private JPanel panelFecha;
		private JPanel panelBtnListar;
		private JButton btnFinalizar;
		private Medico medicoSeleccionado;

		/**
		 * Create the frame.
		 */
		public VentanaListarPacientes(Medico medico) {
			setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaOperacionesAdministrativo.class.getResource("/iu/img/logo.jpeg")));
			//trabajadores = new Trabajadores();
			setBounds(100, 100, 1060, 533);
			panelPrincipal = new JPanel();
			panelPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
			panelPrincipal.setLayout(new BorderLayout(0, 0));
			setContentPane(panelPrincipal);
			panelPrincipal.add(getPanelSeleccionarMedico(), BorderLayout.NORTH);
			panelPrincipal.add(getPanelListado(), BorderLayout.CENTER);
			Calendar c = new GregorianCalendar();
			int dia = c.get(Calendar.DAY_OF_MONTH);
			int mes = c.get(Calendar.MONTH)+1;
			int anio = c.get(Calendar.YEAR);
			this.medicoSeleccionado=medico;
			cb_Dia.setSelectedItem(dia);
			cb_Mes.setSelectedItem(mes);
			cb_Anio.setSelectedItem(anio);
			if(modeloTabla.getRowCount()>0)
			{
				while(modeloTabla.getRowCount()>0)
					modeloTabla.removeRow(0);
			}
			a�adirFilas();	
		}
		
		

		private JPanel getPanelSeleccionarMedico() {
			if (panelSeleccionarMedico == null) {
				panelSeleccionarMedico = new JPanel();
				panelSeleccionarMedico.setLayout(new BorderLayout(0, 0));
				panelSeleccionarMedico.add(getPanelCbMedico(), BorderLayout.NORTH);
				panelSeleccionarMedico.add(getPanelOpMedico(), BorderLayout.SOUTH);
			}
			return panelSeleccionarMedico;
		}
		private JLabel getLblCitas() {
			if (lblCitas == null) {
				lblCitas = new JLabel("Citas");
				lblCitas.setFont(new Font("Tahoma", Font.PLAIN, 33));
			}
			return lblCitas;
		}
		private JPanel getPanelTable() {
			if (panelTable == null) {
				panelTable = new JPanel();
				panelTable.setLayout(new BorderLayout(0, 0));
				panelTable.add(getScrollPane(), BorderLayout.SOUTH);
			}
			return panelTable;
		}
		private JPanel getPanelListado() {
			if (panelListado == null) {
				panelListado = new JPanel();
				panelListado.setLayout(new BorderLayout(0, 0));
				panelListado.add(getPanelTable(), BorderLayout.CENTER);
				panelListado.add(getPanelOpciones(), BorderLayout.EAST);
			}
			return panelListado;
		}
		private JTable getTable_Citas() {
			if (table_Citas == null) {
				table_Citas = new JTable();
				
				scrollPane.setViewportView(table_Citas);
				String[] columnas = {"Dni","Nombre", "Apellido", "Sala", "Hora de Comienzo", "Hora de Fin","Tipo"};
				modeloTabla=new ModeloNoEditable(columnas, 0);
				table_Citas = new JTable(modeloTabla);
				table_Citas.getTableHeader().setReorderingAllowed(false);
				table_Citas.setRowHeight(20);
				table_Citas.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(20);
				
			}
			return table_Citas;
		}
		private JScrollPane getScrollPane() {
			if (scrollPane == null) {
				scrollPane = new JScrollPane();
				scrollPane.setViewportView(getTable_Citas());
			}
			return scrollPane;
		}
		private JPanel getPanelOpciones() {
			if (panelOpciones == null) {
				panelOpciones = new JPanel();
				panelOpciones.setLayout(new BorderLayout(0, 0));
				panelOpciones.add(getBtnVerHistorial(), BorderLayout.NORTH);
				panelOpciones.add(getBtnFinalizar(), BorderLayout.SOUTH);
			}
			return panelOpciones;
		}
		private JButton getBtnVerHistorial() {
			if (btnVerHistorial == null) {
				btnVerHistorial = new JButton("Ver Historial");
				btnVerHistorial.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						int numeroDeFila = getTable_Citas().getSelectedRow();
						if(numeroDeFila!=0 && numeroDeFila!=-1)
						{
							//Tengo que sacar los datos de esa fila.
							TableModel md = getTable_Citas().getModel();
							String dni = (String) md.getValueAt(numeroDeFila, 0);
							String nombre = (String) md.getValueAt(numeroDeFila, 1);
							String apellidos = (String) md.getValueAt(numeroDeFila, 2);
							mostrarVentanaHistorialDePaciente(dni, nombre, apellidos);						
							
						}
					}
					
				});
				btnVerHistorial.setMnemonic('v');
				btnVerHistorial.setFont(new Font("Tahoma", Font.PLAIN, 20));
			}
			return btnVerHistorial;
		}
		
		private void mostrarVentanaHistorialDePaciente(String dni, String nombre, String apellido) {
			VentanaHistorialDePaciente v = new VentanaHistorialDePaciente(dni,nombre,apellido);
			v.setLocationRelativeTo(this);
			v.setModal(true);
			v.setVisible(true);

		}
		
		private JPanel getPanelCbMedico() {
			if (panelCbMedico == null) {
				panelCbMedico = new JPanel();
				panelCbMedico.add(getLblCitas());
			}
			return panelCbMedico;
		}
		private JPanel getPanelOpMedico() {
			if (panelOpMedico == null) {
				panelOpMedico = new JPanel();
				panelOpMedico.setLayout(new BorderLayout(0, 0));
				panelOpMedico.add(getPanelFecha(), BorderLayout.WEST);
				panelOpMedico.add(getPanelBtnListar(), BorderLayout.CENTER);
			}
			return panelOpMedico;
		}
		private JLabel getLblDia() {
			if (lblDia == null) {
				lblDia = new JLabel("D\u00EDa");
				lblDia.setFont(new Font("Tahoma", Font.PLAIN, 25));
			}
			return lblDia;
		}
		
		private JLabel getLblMes() {
			if (lblMes == null) {
				lblMes = new JLabel("Mes");
				lblMes.setFont(new Font("Tahoma", Font.PLAIN, 25));
			}
			return lblMes;
		}
		private JComboBox<Integer> getCb_Dia() {
			if (cb_Dia == null) {
				cb_Dia = new JComboBox<Integer>();
				cb_Dia.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(modeloTabla.getRowCount()>0)
						{
							while(modeloTabla.getRowCount()>0)
								modeloTabla.removeRow(0);
						}
						a�adirFilas();
					}
				});
				cb_Dia.setFont(new Font("Tahoma", Font.PLAIN, 20));
				cb_Dia.setModel(new DefaultComboBoxModel<Integer>(new Integer[] {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}));
				
			}
			return cb_Dia;
		}
		
		
		
		private JComboBox<Integer> getCb_Mes() {
			if (cb_Mes == null) {
				cb_Mes = new JComboBox<Integer>();
				cb_Mes.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if(modeloTabla.getRowCount()>0)
						{
							while(modeloTabla.getRowCount()>0)
								modeloTabla.removeRow(0);
						}
						a�adirFilas();
					}
				});
				cb_Mes.setFont(new Font("Tahoma", Font.PLAIN, 20));
				cb_Mes.setModel(new DefaultComboBoxModel<Integer>(new Integer[] {1,2,3,4,5,6,7,8,9,10,11,12}));
				
			}
			return cb_Mes;
		}
		private JLabel getLblAo() {
			if (lblAo == null) {
				lblAo = new JLabel("A\u00F1o");
				lblAo.setFont(new Font("Tahoma", Font.PLAIN, 25));
			}
			return lblAo;
		}
		private JComboBox<Integer> getCb_Anio() {
			if (cb_Anio == null) {
				cb_Anio = new JComboBox<Integer>();
				cb_Anio.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(modeloTabla.getRowCount()>0)
						{
							while(modeloTabla.getRowCount()>0)
								modeloTabla.removeRow(0);
						}
						a�adirFilas();
					}
				});
				cb_Anio.setFont(new Font("Tahoma", Font.PLAIN, 20));
				cb_Anio.setModel(new DefaultComboBoxModel<Integer>(new Integer[] {2018,2019,2020,2021,2022,2023}));
			}
			return cb_Anio;
		}
		
		private void a�adirFilas()
		{
			Object[] nuevaFila = new Object[7];
			ListarCitas lc = new ListarCitas();
			String index = medicoSeleccionado.getIdMedico();
			List<Cita> relacionCitas = lc.cargarCitasPorFechaYMedico(index+"", cb_Dia.getSelectedItem()+"", cb_Mes.getSelectedItem()+"", cb_Anio.getSelectedItem()+"");
			Object[] leyendaTabla = new Object[7];
			
			leyendaTabla[0]="DNI";
			leyendaTabla[1]="Nombre";
			leyendaTabla[2]="Apellidos";
			leyendaTabla[3]="N� Sala";
			leyendaTabla[4]="Hora Comienzo";
			leyendaTabla[5]="Hora Final";
			leyendaTabla[6]="Tipo";
			
			modeloTabla.addRow(leyendaTabla);
			for(Cita c:relacionCitas)
			{
				//realizar consulta de dniPaciente
				
				ListarPacientes lp = new ListarPacientes();
				Paciente p = lp.getPacientePorID(c.getIdPaciente());
				nuevaFila[0] = p.getDniPaciente();
				nuevaFila[1]=p.getNombrePaciente();
				nuevaFila[2]=p.getApellidoPaciente();
				nuevaFila[3]=c.getIdSala();
				nuevaFila[4] = new java.util.Date(c.getHoraComienzo().getTime()).toString().substring(10, 16);
				nuevaFila[5] = new java.util.Date(c.getHoraFinal().getTime()).toString().substring(10, 16);
				nuevaFila[6]=c.getTipo();
				
				
				modeloTabla.addRow(nuevaFila);
			}
		}
		
		
		private JPanel getPanelFecha() {
			if (panelFecha == null) {
				panelFecha = new JPanel();
				panelFecha.add(getLblDia());
				panelFecha.add(getCb_Dia());
				panelFecha.add(getLblMes());
				panelFecha.add(getCb_Mes());
				panelFecha.add(getLblAo());
				panelFecha.add(getCb_Anio());
			}
			return panelFecha;
		}
		private JPanel getPanelBtnListar() {
			if (panelBtnListar == null) {
				panelBtnListar = new JPanel();
			}
			return panelBtnListar;
		}
		private JButton getBtnFinalizar() {
			if (btnFinalizar == null) {
				btnFinalizar = new JButton("Finalizar");
				btnFinalizar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						mostrarPrincipal();
					}
				});
				btnFinalizar.setMnemonic('f');
				btnFinalizar.setFont(new Font("Tahoma", Font.PLAIN, 20));
			}
			return btnFinalizar;
		}
		
		
		private void mostrarPrincipal() {
			this.setVisible(false);
		}
	}
