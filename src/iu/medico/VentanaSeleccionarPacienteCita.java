package iu.medico;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;

import iu.ModeloNoEditable;
import iu.administrativo.VentanaOperacionesAdministrativo;
import logica.Cita;
import logica.ListarCitas;
import logica.ListarPacientes;
import logica.Medico;
import logica.OperacionesPaciente;
import logica.Paciente;

public class VentanaSeleccionarPacienteCita extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel panelPrincipal;
	private JPanel panelSeleccionarMedico;
	private JPanel panelTable;
	private JPanel panelListado;
	private JTable table_Citas;
	private JScrollPane scrollPane;
	private ModeloNoEditable modeloTabla;
	private JPanel panelOpciones;
	private JButton btnVerHistorial;
	private JPanel panelCbMedico;
	private JPanel panelOpMedico;
	private JPanel panelBtnListar;
	private JButton btnFinalizar;
	private JPanel pnBotonesHistorialYCita;
	private JButton btAtenderCita;
	private Medico medicoActual;
	private JLabel lblCitasDeHoy;
	private JLabel lbl_nombre_Medico;

	private int dia;
	private int mes;
	private int anio;

	/**
	 * Create the frame.
	 */
	public VentanaSeleccionarPacienteCita(Medico medico) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaOperacionesAdministrativo.class.getResource("/iu/img/logo.jpeg")));
		this.medicoActual = medico;
		setTitle("Seleccionar paciente para proceder con la cita");
		setBounds(100, 100, 1060, 533);
		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelPrincipal.setLayout(new BorderLayout(0, 0));
		setContentPane(panelPrincipal);
		panelPrincipal.add(getPanelSeleccionarMedico(), BorderLayout.NORTH);
		panelPrincipal.add(getPanelListado(), BorderLayout.CENTER);
		if (modeloTabla.getRowCount() > 0) {
			while (modeloTabla.getRowCount() > 0)
				modeloTabla.removeRow(0);
		}

		inicializarCalendario();
		añadirFilas();
	}

	private void inicializarCalendario() {
		Calendar calendar = new GregorianCalendar();
		dia = calendar.get(Calendar.DAY_OF_MONTH);
		mes = calendar.get(Calendar.MONTH) + 1;
		anio = calendar.get(Calendar.YEAR);
	}

	private JPanel getPanelSeleccionarMedico() {
		if (panelSeleccionarMedico == null) {
			panelSeleccionarMedico = new JPanel();
			panelSeleccionarMedico.setLayout(new BorderLayout(0, 0));
			panelSeleccionarMedico.add(getPanelCbMedico(), BorderLayout.NORTH);
			panelSeleccionarMedico.add(getPanelOpMedico(), BorderLayout.SOUTH);
		}
		return panelSeleccionarMedico;
	}

	private JPanel getPanelTable() {
		if (panelTable == null) {
			panelTable = new JPanel();
			panelTable.setLayout(new BorderLayout(0, 0));
			panelTable.add(getScrollPane(), BorderLayout.SOUTH);
		}
		return panelTable;
	}

	private JPanel getPanelListado() {
		if (panelListado == null) {
			panelListado = new JPanel();
			panelListado.setLayout(new BorderLayout(0, 0));
			panelListado.add(getPanelTable(), BorderLayout.CENTER);
			panelListado.add(getPanelOpciones(), BorderLayout.EAST);
		}
		return panelListado;
	}

	private JTable getTable_Citas() {
		if (table_Citas == null) {
			table_Citas = new JTable();

			scrollPane.setViewportView(table_Citas);
			String[] columnas = { "Dni", "Nombre", "Apellido", "Sala", "Hora de Comienzo", "Hora de Fin", "Tipo", "ACUDIDO" };
			modeloTabla = new ModeloNoEditable(columnas, 0);
			table_Citas = new JTable(modeloTabla);
			table_Citas.getTableHeader().setReorderingAllowed(false);
			table_Citas.setRowHeight(20);
			table_Citas.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(20);

		}
		return table_Citas;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTable_Citas());
		}
		return scrollPane;
	}

	private JPanel getPanelOpciones() {
		if (panelOpciones == null) {
			panelOpciones = new JPanel();
			panelOpciones.setLayout(new BorderLayout(0, 0));
			panelOpciones.add(getPnBotonesHistorialYCita(), BorderLayout.NORTH);
			panelOpciones.add(getBtnFinalizar(), BorderLayout.SOUTH);
		}
		return panelOpciones;
	}

	private JButton getBtnVerHistorial() {
		if (btnVerHistorial == null) {
			btnVerHistorial = new JButton("Ver Historial");
			btnVerHistorial.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					int numeroDeFila = getTable_Citas().getSelectedRow();
					if (numeroDeFila != -1) {
						// Tengo que sacar los datos de esa fila.
						TableModel md = getTable_Citas().getModel();
						String dni = (String) md.getValueAt(numeroDeFila, 0);
						String nombre = (String) md.getValueAt(numeroDeFila, 1);
						String apellidos = (String) md.getValueAt(numeroDeFila, 2);
						mostrarVentanaHistorialDePaciente(dni, nombre, apellidos);

					}
				}

			});
			btnVerHistorial.setMnemonic('v');
			btnVerHistorial.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return btnVerHistorial;
	}

	private void mostrarVentanaHistorialDePaciente(String dni, String nombre, String apellido) {
		VentanaHistorialDePaciente v = new VentanaHistorialDePaciente(dni, nombre, apellido);
		v.setLocationRelativeTo(this);
		v.setModal(true);
		v.setVisible(true);

	}

	private JPanel getPanelCbMedico() {
		if (panelCbMedico == null) {
			panelCbMedico = new JPanel();
			panelCbMedico.add(getLblCitasDeHoy());
			panelCbMedico.add(getLbl_nombre_Medico());
		}
		return panelCbMedico;
	}

	private JPanel getPanelOpMedico() {
		if (panelOpMedico == null) {
			panelOpMedico = new JPanel();
			panelOpMedico.setLayout(new BorderLayout(0, 0));
			panelOpMedico.add(getPanelBtnListar(), BorderLayout.CENTER);
		}
		return panelOpMedico;
	}

	private void añadirFilas() {

		Object[] nuevaFila = new Object[8];
		ListarCitas lc = new ListarCitas();
		String index = medicoActual.getIdMedico();

		List<Cita> relacionCitas = lc.cargarCitasPorFechaYMedico(index + "", dia + "", mes + "", anio + "");

		for (Cita c : relacionCitas) {
			ListarPacientes lp = new ListarPacientes();
			Paciente p = lp.getPacientePorID(c.getIdPaciente());
			nuevaFila[0] = p.getDniPaciente();
			nuevaFila[1] = p.getNombrePaciente();
			nuevaFila[2] = p.getApellidoPaciente();
			nuevaFila[3] = c.getIdSala();
			nuevaFila[4] = new java.util.Date(c.getHoraComienzo().getTime()).toString().substring(10, 16);
			nuevaFila[5] = new java.util.Date(c.getHoraFinal().getTime()).toString().substring(10, 16);
			nuevaFila[6] = c.getTipo();
			nuevaFila[7] = c.getEstadoCita();
			modeloTabla.addRow(nuevaFila);
		}
	}

	private JPanel getPanelBtnListar() {
		if (panelBtnListar == null) {
			panelBtnListar = new JPanel();
		}
		return panelBtnListar;
	}

	private JButton getBtnFinalizar() {
		if (btnFinalizar == null) {
			btnFinalizar = new JButton("Finalizar");
			btnFinalizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarPrincipal();
				}
			});
			btnFinalizar.setMnemonic('f');
			btnFinalizar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return btnFinalizar;
	}

	private void mostrarPrincipal() {
		this.setVisible(false);
	}

	private JPanel getPnBotonesHistorialYCita() {
		if (pnBotonesHistorialYCita == null) {
			pnBotonesHistorialYCita = new JPanel();
			pnBotonesHistorialYCita.setLayout(new GridLayout(0, 1, 0, 0));
			pnBotonesHistorialYCita.add(getBtnVerHistorial());
			pnBotonesHistorialYCita.add(getBtAtenderCita());
		}
		return pnBotonesHistorialYCita;
	}

	private JButton getBtAtenderCita() {
		if (btAtenderCita == null) {
			btAtenderCita = new JButton("Atender Cita");
			btAtenderCita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					int row = getTable_Citas().getSelectedRow();
					if (row != -1) {
						String nombre = (String) modeloTabla.getValueAt(row, 1);
						String apellido = (String) modeloTabla.getValueAt(row, 2);
						String dni = (String) modeloTabla.getValueAt(row, 0);
						String fecha = dia + "-" + mes + "-" + anio;
						String horaComienzo = (String) modeloTabla.getValueAt(row, 4);
						String horaFin = (String) modeloTabla.getValueAt(row, 5);
						String idCita = getIdCita(OperacionesPaciente.getPacienteDni(dni).getIdPaciente(), fecha, horaComienzo);
						
						mostrarVentana(
								new VentanaAtenderCita(nombre, apellido, dni, fecha, horaComienzo, horaFin, idCita));
					}
				}
			});
			btAtenderCita.setMnemonic('A');
			btAtenderCita.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return btAtenderCita;
	}

	private void mostrarVentana(JDialog ventana) {
		ventana.setLocationRelativeTo(this);
		ventana.setModal(true);
		ventana.setVisible(true);
	}

	private String getIdCita(String idPaciente, String fecha, String horaComienzo) {
		return Cita.getIdCita(idPaciente, fecha, horaComienzo);
	}

	private JLabel getLblCitasDeHoy() {
		if (lblCitasDeHoy == null) {
			lblCitasDeHoy = new JLabel("Citas asignadas a ");
			lblCitasDeHoy.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return lblCitasDeHoy;
	}

	private JLabel getLbl_nombre_Medico() {
		if (lbl_nombre_Medico == null) {
			lbl_nombre_Medico = new JLabel(medicoActual.getNombre() + " " + medicoActual.getApellidos());
			lbl_nombre_Medico.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return lbl_nombre_Medico;
	}
}
