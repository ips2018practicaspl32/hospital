package iu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;

import iu.administrativo.VentanaCrearCitas;
import iu.medico.VentanaCrearCitasMedico;
import logica.BuscadorPacientes;
import logica.Paciente;

import java.awt.Toolkit;

public class VentanaBuscadorPacientes extends JDialog {

	private static final long serialVersionUID = 1L;
	private VentanaCrearCitas ventana;
	private VentanaCrearCitasMedico ventanaMedico;
	private ModeloNoEditable modeloTabla;
	private JPanel pnBuscadorMedicos;
	private JLabel lbBuscarPor;
	private JComboBox<String> cbBuscarPor;
	private JTextField txBuscar;
	private JButton btBuscar;
	private JScrollPane scrollBuscador;
	private JTable tableBuscador;
	private JPanel panelTabla;
	private JButton btSeleccionarMedico;
	private JButton btCancelar;

	/**
	 * Create the frame.
	 */
	public VentanaBuscadorPacientes(VentanaCrearCitas ventana) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaBuscadorPacientes.class.getResource("/iu/img/logo.jpeg")));
		this.ventana = ventana;
		setResizable(false);
		setTitle("Buscador de pacientes");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 632, 533);
		pnBuscadorMedicos = new JPanel();
		pnBuscadorMedicos.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnBuscadorMedicos);
		pnBuscadorMedicos.setLayout(null);
		pnBuscadorMedicos.add(getLbBuscarPor());
		pnBuscadorMedicos.add(getCbBuscarPor());
		pnBuscadorMedicos.add(getTxBuscar());
		pnBuscadorMedicos.add(getBtBuscar());
		pnBuscadorMedicos.add(getPanelTabla());
		pnBuscadorMedicos.add(getBtSeleccionarPaciente());
		pnBuscadorMedicos.add(getBtCancelar());
		
		eliminarFilasTabla();
		a�adirFilas();
	}
	public VentanaBuscadorPacientes(VentanaCrearCitasMedico ventana) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaBuscadorPacientes.class.getResource("/iu/img/logo.jpeg")));
		this.ventanaMedico= ventana;
		setResizable(false);
		setTitle("Buscador de pacientes");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 632, 533);
		pnBuscadorMedicos = new JPanel();
		pnBuscadorMedicos.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnBuscadorMedicos);
		pnBuscadorMedicos.setLayout(null);
		pnBuscadorMedicos.add(getLbBuscarPor());
		pnBuscadorMedicos.add(getCbBuscarPor());
		pnBuscadorMedicos.add(getTxBuscar());
		pnBuscadorMedicos.add(getBtBuscar());
		pnBuscadorMedicos.add(getPanelTabla());
		pnBuscadorMedicos.add(getBtSeleccionarPaciente());
		pnBuscadorMedicos.add(getBtCancelar());
		
		eliminarFilasTabla();
		a�adirFilas();
	}
	private JLabel getLbBuscarPor() {
		if (lbBuscarPor == null) {
			lbBuscarPor = new JLabel("Buscar por:");
			lbBuscarPor.setBounds(22, 24, 107, 26);
		}
		return lbBuscarPor;
	}
	private JComboBox<String> getCbBuscarPor() {
		if (cbBuscarPor == null) {
			cbBuscarPor = new JComboBox<String>();
			cbBuscarPor.setModel(new DefaultComboBoxModel<String>(new String[] {"Nombre", "Apellidos", "DNI"}));
			cbBuscarPor.setBounds(110, 26, 186, 22);
		}
		return cbBuscarPor;
	}
	private JTextField getTxBuscar() {
		if (txBuscar == null) {
			txBuscar = new JTextField();
			txBuscar.setBounds(308, 26, 214, 22);
			txBuscar.setColumns(10);
		}
		return txBuscar;
	}
	private JButton getBtBuscar() {
		if (btBuscar == null) {
			btBuscar = new JButton("Buscar");
			btBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					eliminarFilasTabla();
					a�adirFilas();
				}
			});
			btBuscar.setBounds(530, 25, 73, 25);
		}
		return btBuscar;
	}
	private JScrollPane getScrollBuscador() {
		if (scrollBuscador == null) {
			scrollBuscador = new JScrollPane();
			scrollBuscador.setViewportView(getTableBuscador());
		}
		return scrollBuscador;
	}
	private JTable getTableBuscador() {
		if (tableBuscador == null) {
			String[] columnas = {"Dni","Nombre", "Apellidos"};
			modeloTabla=new ModeloNoEditable(columnas, 0);
			tableBuscador = new JTable(modeloTabla);
			tableBuscador.setBounds(0, 0, 1, 1);
			tableBuscador.getTableHeader().setReorderingAllowed(false);
			tableBuscador.setRowHeight(20);
			tableBuscador.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(20);
		}
		return tableBuscador;
	}
	private void eliminarFilasTabla() {
		if(modeloTabla.getRowCount()>0)
		{
			while(modeloTabla.getRowCount()>0)
				modeloTabla.removeRow(0);
		}
	}
	private void a�adirFilas() {
		Object[] nuevaFila = new Object[3];
		List<Paciente> pacientesBuscados = null;
		String texto = getTxBuscar().getText();
		if(!texto.trim().equals("")) {
			if(getCbBuscarPor().getSelectedItem().equals("Nombre")) {
				pacientesBuscados = BuscadorPacientes.buscarPorNombre(formatoSQL(texto));
			}else if(getCbBuscarPor().getSelectedItem().equals("Apellidos")) {
				pacientesBuscados = BuscadorPacientes.buscarPorApellidos(formatoSQL(texto));
			}else if(getCbBuscarPor().getSelectedItem().equals("DNI")) {
				pacientesBuscados = BuscadorPacientes.buscarPorDni(texto);
			}
			
			for(Paciente paciente : pacientesBuscados)
			{
				nuevaFila[0] = paciente.getDniPaciente();
				nuevaFila[1]= paciente.getNombrePaciente();
				nuevaFila[2]= paciente.getApellidoPaciente();
				
				modeloTabla.addRow(nuevaFila);
			}
		}
	}
	
	private String formatoSQL(String texto) {
		
		char[] c = texto.toCharArray();
		c[0] = Character.toUpperCase(c[0]);
		for(int i = 1; i < c.length; i++)
			c[i] = Character.toLowerCase(c[i]);
		String resultado = "";
		for(char ch : c)
			resultado += ch;
		return resultado;
	}
	
	private JPanel getPanelTabla() {
		if (panelTabla == null) {
			panelTabla = new JPanel();
			panelTabla.setBounds(22, 63, 581, 388);
			panelTabla.add(getScrollBuscador());
		}
		return panelTabla;
	}
	private JButton getBtSeleccionarPaciente() {
		if (btSeleccionarMedico == null) {
			btSeleccionarMedico = new JButton("Seleccionar Paciente");
			btSeleccionarMedico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(ventanaMedico == null){
					int row = getTableBuscador().getSelectedRow();
					if(row == -1) {
						JOptionPane.showMessageDialog(null, "Seleccione un paciente de la tabla.");
					}else {
						TableModel md = getTableBuscador().getModel();
						String dni = (String) md.getValueAt(row, 0);
						String nombre = (String) md.getValueAt(row, 1);
						String apellidos = (String) md.getValueAt(row, 2);
						int result = -1;
						if(!ventana.getTxNombre().getText().trim().equals("")
								|| !ventana.getTxApellidos().getText().trim().equals("")
								|| !ventana.getTxDni().getText().trim().equals("")) {
							
							result = JOptionPane.showConfirmDialog(null, "Podr�a haber ya un paceinte seleccionado, �desea continuar?");
							
							if(result == JOptionPane.YES_OPTION) {
								ventana.getTxNombre().setText(nombre);
								ventana.getTxApellidos().setText(apellidos);
								ventana.getTxDni().setText(dni);
							}
						}else {
							ventana.getTxNombre().setText(nombre);
							ventana.getTxApellidos().setText(apellidos);
							ventana.getTxDni().setText(dni);
						}
						
						cerrar();
					}
				}
					else{
						int row = getTableBuscador().getSelectedRow();
						if(row == -1) {
							JOptionPane.showMessageDialog(null, "Seleccione un paciente de la tabla.");
						}else {
							TableModel md = getTableBuscador().getModel();
							String dni = (String) md.getValueAt(row, 0);
							String nombre = (String) md.getValueAt(row, 1);
							String apellidos = (String) md.getValueAt(row, 2);
							int result = -1;
							if(!ventanaMedico.getTxNombre().getText().trim().equals("")
									|| !ventanaMedico.getTxApellidos().getText().trim().equals("")
									|| !ventanaMedico.getTxDni().getText().trim().equals("")) {
								
								result = JOptionPane.showConfirmDialog(null, "Podr�a haber ya un paceinte seleccionado, �desea continuar?");
								
								if(result == JOptionPane.YES_OPTION) {
									ventanaMedico.getTxNombre().setText(nombre);
									ventanaMedico.getTxApellidos().setText(apellidos);
									ventanaMedico.getTxDni().setText(dni);
								}
							}else {
								ventanaMedico.getTxNombre().setText(nombre);
								ventanaMedico.getTxApellidos().setText(apellidos);
								ventanaMedico.getTxDni().setText(dni);
							}
							
							cerrar();
						}
					}
				}
			});
			btSeleccionarMedico.setBounds(439, 464, 164, 25);
		}
		return btSeleccionarMedico;
	}
	private JButton getBtCancelar() {
		if (btCancelar == null) {
			btCancelar = new JButton("Cancelar");
			btCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					cerrar();
				}
			});
			btCancelar.setBounds(22, 464, 164, 25);
		}
		return btCancelar;
	}
	private void cerrar() {
		this.dispose();
	}
}
