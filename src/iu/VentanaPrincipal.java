package iu;

import java.awt.BorderLayout; 
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import com.jtattoo.plaf.hifi.HiFiLookAndFeel;

import iu.administrativo.VentanaOperacionesAdministrativo;
import iu.medico.VentanaLoginMedico;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Properties;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;

@Deprecated
public class VentanaPrincipal extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel panelLogIn;
	private JLabel lbEntrarComo;
	private JButton btMedico;
	private JButton btAdministrativo;
	private JPanel pnBotonesLogIn;
	private JLabel lbImgHospital;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Properties props = new Properties();
					props.put("logoString","");
					HiFiLookAndFeel.setCurrentTheme(props);
					
					UIManager.setLookAndFeel("com.jtattoo.plaf.acryl.AcrylLookAndFeel");
					
					VentanaPrincipal frame = new VentanaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaPrincipal() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaOperacionesAdministrativo.class.getResource("/iu/img/logo.jpeg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1180, 592);
		panelLogIn = new JPanel();
		panelLogIn.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panelLogIn);
		panelLogIn.setLayout(new BorderLayout(0, 0));
		panelLogIn.add(getPnBotonesLogIn(), BorderLayout.WEST);
		panelLogIn.add(getLbImgHospital(), BorderLayout.CENTER);
		setLocationRelativeTo(null);
	}

	private JLabel getLbEntrarComo() {
		if (lbEntrarComo == null) {
			lbEntrarComo = new JLabel("Entrar como:");
			lbEntrarComo.setFont(new Font("Tahoma", Font.PLAIN, 30));
		}
		return lbEntrarComo;
	}
	private JButton getBtMedico() {
		if (btMedico == null) {
			btMedico = new JButton("M\u00E9dico");
			btMedico.setFont(new Font("Tahoma", Font.PLAIN, 28));
			btMedico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentanaMedico();
				}
			});
			btMedico.setFont(new Font("Tahoma", Font.PLAIN, 28));
		}
		return btMedico;
	}
	private JButton getBtAdministrativo() {
		if (btAdministrativo == null) {
			btAdministrativo = new JButton("Administrativo");
			btAdministrativo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentanaAdministrativo();
				}
			});
			btAdministrativo.setFont(new Font("Tahoma", Font.PLAIN, 28));
		}
		return btAdministrativo;
	}
	private void mostrarVentanaAdministrativo() {
		/*//VentanaOperacionesAdministrativo voa = new VentanaOperacionesAdministrativo();
		voa.setLocationRelativeTo(this);
		voa.setModal(true);
		voa.setVisible(true);*/
	}
	private void mostrarVentanaMedico() {
		VentanaLoginMedico voa = new VentanaLoginMedico();
		voa.setLocationRelativeTo(this);
		voa.setModal(true);
		voa.setVisible(true);
	}
	private JPanel getPnBotonesLogIn() {
		if (pnBotonesLogIn == null) {
			pnBotonesLogIn = new JPanel();
			pnBotonesLogIn.setLayout(new GridLayout(0, 1, 0, 50));
			pnBotonesLogIn.add(getLbEntrarComo());
			pnBotonesLogIn.add(getBtAdministrativo());
			pnBotonesLogIn.add(getBtMedico());
		}
		return pnBotonesLogIn;
	}
	private JLabel getLbImgHospital() {
		if (lbImgHospital == null) {
			lbImgHospital = new JLabel("");
			lbImgHospital.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iu/img/hospital1.jpg")));
			lbImgHospital.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbImgHospital;
	}
}
