package iu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;

import logica.BuscadorMedicos;
import logica.Medico;

public class VentanaBuscadorMedicos extends JDialog {

	private static final long serialVersionUID = 1L;
	private DefaultListModel<Medico> modeloLista;
	private JTextField txMedicoSeleccionado;
	private BuscadorMedicoInterface ventana;
	private ModeloNoEditable modeloTabla;
	private JPanel pnBuscadorMedicos;
	private JLabel lbBuscarPor;
	private JComboBox<String> cbBuscarPor;
	private JTextField txBuscar;
	private JButton btBuscar;
	private JScrollPane scrollBuscador;
	private JTable tableBuscador;
	private JPanel panelTabla;
	private JButton btSeleccionarMedico;
	private JButton btCancelar;

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("unchecked")
	public VentanaBuscadorMedicos(Object contenedor, BuscadorMedicoInterface ventana) {
		this.modeloLista = contenedor instanceof DefaultListModel ? (DefaultListModel<Medico>)contenedor : null;
		this.txMedicoSeleccionado = contenedor instanceof JTextField ? (JTextField)contenedor : null;
		this.ventana = ventana;
		setResizable(false);
		setTitle("Buscador de m\u00E9dicos");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 632, 533);
		pnBuscadorMedicos = new JPanel();
		pnBuscadorMedicos.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnBuscadorMedicos);
		pnBuscadorMedicos.setLayout(null);
		pnBuscadorMedicos.add(getLbBuscarPor());
		pnBuscadorMedicos.add(getCbBuscarPor());
		pnBuscadorMedicos.add(getTxBuscar());
		pnBuscadorMedicos.add(getBtBuscar());
		pnBuscadorMedicos.add(getPanelTabla());
		pnBuscadorMedicos.add(getBtSeleccionarMedico());
		pnBuscadorMedicos.add(getBtCancelar());
		
		eliminarFilasTabla();
		a�adirFilas();
	}
	private JLabel getLbBuscarPor() {
		if (lbBuscarPor == null) {
			lbBuscarPor = new JLabel("Buscar por:");
			lbBuscarPor.setBounds(22, 24, 107, 26);
		}
		return lbBuscarPor;
	}
	private JComboBox<String> getCbBuscarPor() {
		if (cbBuscarPor == null) {
			cbBuscarPor = new JComboBox<String>();
			cbBuscarPor.setModel(new DefaultComboBoxModel<String>(new String[] {"Nombre", "Apellidos", "Tarjeta sanitaria"}));
			cbBuscarPor.setBounds(110, 26, 186, 22);
		}
		return cbBuscarPor;
	}
	private JTextField getTxBuscar() {
		if (txBuscar == null) {
			txBuscar = new JTextField();
			txBuscar.setBounds(308, 26, 214, 22);
			txBuscar.setColumns(10);
		}
		return txBuscar;
	}
	private JButton getBtBuscar() {
		if (btBuscar == null) {
			btBuscar = new JButton("Buscar");
			btBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					eliminarFilasTabla();
					a�adirFilas();
				}
			});
			btBuscar.setBounds(530, 25, 73, 25);
		}
		return btBuscar;
	}
	private JScrollPane getScrollBuscador() {
		if (scrollBuscador == null) {
			scrollBuscador = new JScrollPane();
			scrollBuscador.setViewportView(getTableBuscador());
		}
		return scrollBuscador;
	}
	private JTable getTableBuscador() {
		if (tableBuscador == null) {
			String[] columnas = {"Dni","Nombre", "Apellidos"};
			modeloTabla=new ModeloNoEditable(columnas, 0);
			tableBuscador = new JTable(modeloTabla);
			tableBuscador.setBounds(0, 0, 1, 1);
			tableBuscador.getTableHeader().setReorderingAllowed(false);
			tableBuscador.setRowHeight(20);
			tableBuscador.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(20);
		}
		return tableBuscador;
	}
	private void eliminarFilasTabla() {
		if(modeloTabla.getRowCount()>0)
		{
			while(modeloTabla.getRowCount()>0)
				modeloTabla.removeRow(0);
		}
	}
	private void a�adirFilas() {
		Object[] nuevaFila = new Object[3];
		List<Medico> medicosBuscados = null;
		String texto = getTxBuscar().getText();
		if(!texto.trim().equals("")) {
			if(getCbBuscarPor().getSelectedItem().equals("Nombre")) {
				medicosBuscados = BuscadorMedicos.buscarPorNombre(formatoSQL(texto));
			}else if(getCbBuscarPor().getSelectedItem().equals("Apellidos")) {
				medicosBuscados = BuscadorMedicos.buscarPorApellidos(formatoSQL(texto));
			}else if(getCbBuscarPor().getSelectedItem().equals("Tarjeta sanitaria")) {
				medicosBuscados = BuscadorMedicos.buscarPorDni(texto);
			}
			
			for(Medico medico : medicosBuscados)
			{
				nuevaFila[0] = medico.getIdMedico();
				nuevaFila[1]= medico.getNombre();
				nuevaFila[2]= medico.getApellidos();
				
				modeloTabla.addRow(nuevaFila);
			}
		}
	}
	
	private String formatoSQL(String texto) {
		
		char[] c = texto.toCharArray();
		c[0] = Character.toUpperCase(c[0]);
		for(int i = 1; i < c.length; i++)
			c[i] = Character.toLowerCase(c[i]);
		String resultado = "";
		for(char ch : c)
			resultado += ch;
		return resultado;
	}
	
	private JPanel getPanelTabla() {
		if (panelTabla == null) {
			panelTabla = new JPanel();
			panelTabla.setBounds(22, 63, 581, 388);
			panelTabla.add(getScrollBuscador());
		}
		return panelTabla;
	}
	private JButton getBtSeleccionarMedico() { 
		if (btSeleccionarMedico == null) {
			btSeleccionarMedico = new JButton("Seleccionar Medico");
			btSeleccionarMedico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int row = getTableBuscador().getSelectedRow();
					if(row == -1) {
						JOptionPane.showMessageDialog(null, "Seleccione un m�dico de la tabla.");
					}else {
						TableModel md = getTableBuscador().getModel();
						String dni = (String) md.getValueAt(row, 0);
						String nombre = (String) md.getValueAt(row, 1);
						String apellidos = (String) md.getValueAt(row, 2);
						Medico medico = new Medico(dni,nombre,apellidos);
						if(modeloLista != null) {
							for(int i = 0; i < modeloLista.size();i++) {
								if(modeloLista.getElementAt(i).equals(medico)){
									JOptionPane.showMessageDialog(null, "El m�dico ya estaba seleccionado.");
									cerrar();
									return;
								}
							}
						}else if (txMedicoSeleccionado != null) {
							if(txMedicoSeleccionado.getText().equals(medico.toString())) {
								JOptionPane.showMessageDialog(null, "El m�dico ya estaba seleccionado.");
								cerrar();
								return;
							}
						}
						if(modeloLista != null) {
							modeloLista.addElement(medico);
						}else if (txMedicoSeleccionado != null) {
							txMedicoSeleccionado.setText(medico.toString());
							ventana.setMedico(medico);
						}
						cerrar();
					}
				}
			});
			btSeleccionarMedico.setBounds(439, 464, 164, 25);
		}
		return btSeleccionarMedico;
	}
	private JButton getBtCancelar() {
		if (btCancelar == null) {
			btCancelar = new JButton("Cancelar");
			btCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					cerrar();
				}
			});
			btCancelar.setBounds(22, 464, 164, 25);
		}
		return btCancelar;
	}
	private void cerrar() {
		this.dispose();
	}
}
