package iu;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.jtattoo.plaf.hifi.HiFiLookAndFeel;

import iu.administrativo.VentanaOperacionesAdministrativo;
import iu.medico.VentanaOperacionesMedico;
import logica.Administrativo;
import logica.Login;
import logica.Medico;
import logica.Trabajadores;
import util.PasswordEncrypt;

import java.awt.Toolkit;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;

public class VentanaLogin extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel panelLogin;
	private JPanel panelOpciones;
	private JPanel panelDatos;
	private JPanel panelTitulo;
	private JPanel panelAdmin;
	private JPanel panelMedico;
	private JLabel lblEntrarComo;
	private JRadioButton rdbtnAdministrador;
	private JRadioButton rdbtnMedico;
	private JTextField txtDni;
	private JLabel lblDni;
	private JPasswordField passwordField;
	private JLabel lblContra;
	private JLabel lbl_img;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JButton btnIniciar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Properties props = new Properties();
					props.put("logoString", "");
					HiFiLookAndFeel.setCurrentTheme(props);

					UIManager.setLookAndFeel("com.jtattoo.plaf.acryl.AcrylLookAndFeel");

					VentanaLogin frame = new VentanaLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaLogin() {
		setResizable(false);
		setTitle("Login");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaLogin.class.getResource("/iu/img/logo.jpeg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 724, 564);
		panelLogin = new JPanel();
		panelLogin.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelLogin.setLayout(new BorderLayout(0, 0));
		setContentPane(panelLogin);
		panelLogin.add(getPanelOpciones(), BorderLayout.NORTH);
		panelLogin.add(getPanelDatos(), BorderLayout.CENTER);
		getRdbtnAdministrador().setSelected(true);
		setLocationRelativeTo(null);
	}

	private JPanel getPanelOpciones() {
		if (panelOpciones == null) {
			panelOpciones = new JPanel();
			panelOpciones.setLayout(new BoxLayout(panelOpciones, BoxLayout.Y_AXIS));
			panelOpciones.add(getPanelTitulo());
			panelOpciones.add(getPanelAdmin());
			panelOpciones.add(getPanelMedico());
		}
		return panelOpciones;
	}

	private JPanel getPanelDatos() {
		if (panelDatos == null) {
			panelDatos = new JPanel();
			panelDatos.setLayout(null);
			panelDatos.add(getTxtDni());
			panelDatos.add(getLblDni());
			panelDatos.add(getPasswordField());
			panelDatos.add(getLblContra());
			panelDatos.add(getLbl_img());
			panelDatos.add(getBtnIniciar());
		}
		return panelDatos;
	}

	private JPanel getPanelTitulo() {
		if (panelTitulo == null) {
			panelTitulo = new JPanel();
			panelTitulo.add(getLblEntrarComo());
		}
		return panelTitulo;
	}

	private JPanel getPanelAdmin() {
		if (panelAdmin == null) {
			panelAdmin = new JPanel();
			panelAdmin.add(getRdbtnAdministrador());
		}
		return panelAdmin;
	}

	private JPanel getPanelMedico() {
		if (panelMedico == null) {
			panelMedico = new JPanel();
			panelMedico.add(getRdbtnMedico());
		}
		return panelMedico;
	}

	private JLabel getLblEntrarComo() {
		if (lblEntrarComo == null) {
			lblEntrarComo = new JLabel("Entrar como");
			lblEntrarComo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		}
		return lblEntrarComo;
	}

	private JRadioButton getRdbtnAdministrador() {
		if (rdbtnAdministrador == null) {
			rdbtnAdministrador = new JRadioButton("Administrativo");
			rdbtnAdministrador.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					getLbl_img().setIcon(new ImageIcon(VentanaLogin.class.getResource("/iu/img/adminIcon.png")));
				}
			});
			buttonGroup.add(rdbtnAdministrador);
			rdbtnAdministrador.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return rdbtnAdministrador;
	}

	private JRadioButton getRdbtnMedico() {
		if (rdbtnMedico == null) {
			rdbtnMedico = new JRadioButton("M\u00E9dico");
			buttonGroup.add(rdbtnMedico);
			rdbtnMedico.setFont(new Font("Tahoma", Font.PLAIN, 14));
			rdbtnMedico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					getLbl_img().setIcon(new ImageIcon(VentanaLogin.class.getResource("/iu/img/medicoIcon.png")));
				}
			});
		}
		return rdbtnMedico;
	}

	private JTextField getTxtDni() {
		if (txtDni == null) {
			txtDni = new JTextField();
			txtDni.setColumns(10);
			txtDni.setBounds(321, 278, 201, 20);
		}
		return txtDni;
	}

	private JLabel getLblDni() {
		if (lblDni == null) {
			lblDni = new JLabel("ID de Usuario");
			lblDni.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblDni.setBounds(202, 278, 109, 17);
		}
		return lblDni;
	}

	private JPasswordField getPasswordField() {
		if (passwordField == null) {
			passwordField = new JPasswordField();
			passwordField.setBounds(321, 330, 201, 20);
		}
		return passwordField;
	}

	private JLabel getLblContra() {
		if (lblContra == null) {
			lblContra = new JLabel("Contrase\u00F1a");
			lblContra.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblContra.setBounds(224, 330, 87, 17);
		}
		return lblContra;
	}

	private JLabel getLbl_img() {
		if (lbl_img == null) {
			lbl_img = new JLabel("");
			lbl_img.setIcon(new ImageIcon(VentanaLogin.class.getResource("/iu/img/adminIcon.png")));
			lbl_img.setBounds(275, 11, 201, 233);
		}
		return lbl_img;
	}

	private JButton getBtnIniciar() {
		if (btnIniciar == null) {
			btnIniciar = new JButton("Iniciar Sesi\u00F3n");
			btnIniciar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (comprobarCampos()) {

						if (rdbtnAdministrador.isSelected()) {
							Login l = new Login();
							if (comprobarUsuarioAdmin(l)) {
								if (comprobarContraseniaAdmin(l)) {

									//Aqu� seria el caso correcto de logueo.
									mostrarVentanaOperacionesAdministrador(new Administrativo(getTxtDni().getText()));
									
								} else
									JOptionPane.showMessageDialog(null, "Contrase�a incorrecta");
							} else {
								JOptionPane.showMessageDialog(null, "El usuario no existe");
							}
						}
						if (rdbtnMedico.isSelected()) {
							Login l = new Login();
							if (comprobarUsuarioMedico(l)) {
								if (comprobarContraseniaMedico(l)) {
									//Aqu� seria el caso correcto de logueo.
									Trabajadores t = new Trabajadores();
									
									Medico medico = t.getMedicoById(getTxtDni().getText());
									mostrarVentanaOperacionesMedico(medico);

									
								} else
									JOptionPane.showMessageDialog(null, "Contrase�a incorrecta");
							} else {
								JOptionPane.showMessageDialog(null, "El usuario no existe");
							}
						}
					} else {
						JOptionPane.showMessageDialog(null, "Deben rellenarse todos los campos para continuar");
					}
				}
			});
			btnIniciar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnIniciar.setBounds(224, 379, 298, 23);
		}
		return btnIniciar;
	}

	
	private void mostrarVentanaOperacionesAdministrador(Administrativo administrativo) {
		this.setVisible(false);
		VentanaOperacionesAdministrativo voa = new VentanaOperacionesAdministrativo(administrativo);
		voa.setLocationRelativeTo(this);
		voa.setModal(true);
		voa.setVisible(true);
	}
	
	
	private void mostrarVentanaOperacionesMedico(Medico medico) {
		this.setVisible(false);
		VentanaOperacionesMedico voa = new VentanaOperacionesMedico(medico);
		voa.setLocationRelativeTo(this);
		voa.setModal(true);
		voa.setVisible(true);
	}
	
	private boolean comprobarCampos() {
		if (getTxtDni().getText() == "" || getPasswordField().getPassword().length == 0) {
			return false;
		} else
			return true;
	}

	private boolean comprobarUsuarioAdmin(Login l) {

		int check = l.comprobarExisteAdmin(getTxtDni().getText());
		if (check != 0)
			return true;
		else
			return false;
	}

	private boolean comprobarContraseniaAdmin(Login l) {
		PasswordEncrypt pe = new PasswordEncrypt();
		String encriptada = pe.encriptarPassword(String.valueOf(getPasswordField().getPassword()));
		String contrasenaUsuario = l.comprobarContrasenaAdmin(getTxtDni().getText());
		if (encriptada.equals(contrasenaUsuario)) {
			return true;
		} else
			return false;

	}
	
	private boolean comprobarUsuarioMedico(Login l) {

		int check = l.comprobarExisteMedico(getTxtDni().getText());
		if (check != 0)
			return true;
		else
			return false;
	}

	private boolean comprobarContraseniaMedico(Login l) {
		PasswordEncrypt pe = new PasswordEncrypt();
		String encriptada = pe.encriptarPassword(String.valueOf(getPasswordField().getPassword()));
		String contrasenaUsuario = l.comprobarContrasenaMedico(getTxtDni().getText());
		if (encriptada.equals(contrasenaUsuario)) {
			return true;
		} else
			return false;

	}
	
	
}
