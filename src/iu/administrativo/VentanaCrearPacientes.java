package iu.administrativo;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import logica.AņadirPacientes;

public class VentanaCrearPacientes extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JLabel lbNombre;
	private JLabel lblIntroduzcaApellidosDel;
	private JLabel lblIntroduzcaDniDel;
	private JTextField txNombre;
	private JTextField txApellidos;
	private JTextField txDni;
	private JTextField txDireccion;
	private JTextField txTelefono;
	private JLabel lblIntroduzcaDireccionDel;
	private JLabel lblNmeroDeTelfondo;


	/**
	 * Create the dialog.
	 */
	public VentanaCrearPacientes() {
		setTitle("A\u00F1adir nuevo paciente");
		setBounds(100, 100, 676, 345);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 660, 273);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		contentPanel.add(getLbNombre());
		contentPanel.add(getLblIntroduzcaApellidosDel());
		contentPanel.add(getLblIntroduzcaDniDel());
		contentPanel.add(getTxNombre());
		contentPanel.add(getTxApellidos());
		contentPanel.add(getTxDni());
		contentPanel.add(getTxDireccion());
		contentPanel.add(getTxTelefono());
		contentPanel.add(getLblIntroduzcaDireccionDel());
		contentPanel.add(getLblNmeroDeTelfondo());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(0, 273, 660, 33);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane);
			{
				JButton btFinalizar = new JButton("Finalizar");
				btFinalizar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if(checkCamposValidos() && checkPacienteNoExiste()){
							AņadirPacientes.aņadir(txNombre.getText(),txApellidos.getText(),txDni.getText(),txDireccion.getText(),txTelefono.getText());
							JOptionPane.showMessageDialog(null, "Se ha aņadido el paciente correctamente");
							cerrar();
							}
					}
				});
				btFinalizar.setActionCommand("OK");
				buttonPane.add(btFinalizar);
				getRootPane().setDefaultButton(btFinalizar);
			}
			{
				JButton btCancelar = new JButton("Cancelar");
				btCancelar.setActionCommand("Cancel");
				buttonPane.add(btCancelar);
				btCancelar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg) {
						cerrar();
					}
				});
			}
		}
	}

	protected void cerrar() {
		this.dispose();
		
	}

	protected boolean checkPacienteNoExiste() {
		if (AņadirPacientes.comprobarPaciente(txNombre.getText(),
				txApellidos.getText(), txDni.getText())) {
			return true;
		}
		JOptionPane.showMessageDialog(null, "El paciente ya existe.");
		return false;
	}

	protected boolean checkCamposValidos() {
		if (txNombre.getText().isEmpty() || txApellidos.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"El nombre o los apellidos no deben de estar vacios");
			return false;
		}
		return true;
	}

	private JLabel getLbNombre() {
		if (lbNombre == null) {
			lbNombre = new JLabel("Nombre del paciente:");
			lbNombre.setBounds(21, 62, 171, 14);
		}
		return lbNombre;
	}

	private JLabel getLblIntroduzcaApellidosDel() {
		if (lblIntroduzcaApellidosDel == null) {
			lblIntroduzcaApellidosDel = new JLabel("Apellidos del paciente:");
			lblIntroduzcaApellidosDel.setBounds(21, 102, 171, 14);
		}
		return lblIntroduzcaApellidosDel;
	}

	private JLabel getLblIntroduzcaDniDel() {
		if (lblIntroduzcaDniDel == null) {
			lblIntroduzcaDniDel = new JLabel(
					"DNI del paciente (No obligatorio):");
			lblIntroduzcaDniDel.setBounds(21, 141, 227, 14);
		}
		return lblIntroduzcaDniDel;
	}

	private JTextField getTxNombre() {
		if (txNombre == null) {
			txNombre = new JTextField();
			txNombre.setBounds(306, 59, 299, 20);
			txNombre.setColumns(10);
		}
		return txNombre;
	}

	private JTextField getTxApellidos() {
		if (txApellidos == null) {
			txApellidos = new JTextField();
			txApellidos.setColumns(10);
			txApellidos.setBounds(306, 99, 299, 20);
		}
		return txApellidos;
	}

	private JTextField getTxDni() {
		if (txDni == null) {
			txDni = new JTextField();
			txDni.setColumns(10);
			txDni.setBounds(306, 138, 299, 20);
		}
		return txDni;
	}

	private JTextField getTxDireccion() {
		if (txDireccion == null) {
			txDireccion = new JTextField();
			txDireccion.setColumns(10);
			txDireccion.setBounds(306, 176, 299, 20);
		}
		return txDireccion;
	}

	private JTextField getTxTelefono() {
		if (txTelefono == null) {
			txTelefono = new JTextField();
			txTelefono.setColumns(10);
			txTelefono.setBounds(306, 215, 299, 20);
		}
		return txTelefono;
	}

	private JLabel getLblIntroduzcaDireccionDel() {
		if (lblIntroduzcaDireccionDel == null) {
			lblIntroduzcaDireccionDel = new JLabel(
					"Direcci\u00F3n del paciente (No obligatorio):");
			lblIntroduzcaDireccionDel.setBounds(21, 179, 227, 14);
		}
		return lblIntroduzcaDireccionDel;
	}

	private JLabel getLblNmeroDeTelfondo() {
		if (lblNmeroDeTelfondo == null) {
			lblNmeroDeTelfondo = new JLabel(
					"N\u00FAmero de tel\u00E9fono del paciente (No obligatorio):");
			lblNmeroDeTelfondo.setBounds(21, 218, 275, 14);
		}
		return lblNmeroDeTelfondo;
	}
}
