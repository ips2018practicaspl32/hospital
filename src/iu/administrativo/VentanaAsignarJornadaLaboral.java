package iu.administrativo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;

import iu.BuscadorMedicoInterface;
import iu.VentanaBuscadorMedicos;
import logica.Administrativo;
import logica.Medico;
import logica.Trabajadores;

public class VentanaAsignarJornadaLaboral extends JDialog implements BuscadorMedicoInterface{

	private static final long serialVersionUID = 1L;
	private JPanel panelAsignarJornadaLaboral;
	private JPanel panelSeleccionarMedico;
	private JLabel lbSeleccionarMedico;
	private Medico medico;

	private Trabajadores trabajadores;
	private JPanel pnJornada;
	private JPanel pnAgregarHorario;
	private JLabel lbA�adirDias;
	private JButton btA�adirDias;
	private JLabel lbHora;
	private JLabel lbDesde;
	private JLabel lbHasta;
	private JList<String> listHorario;
	private DefaultListModel<String> modeloListHorario;
	private JButton btAsignarHorario;
	private JButton btEliminarHorario;
	private JCheckBox chkLunes;
	private JCheckBox chkMartes;
	private JCheckBox chkMiercoles;
	private JCheckBox chkJueves;
	private JCheckBox chkViernes;
	private JCheckBox chkSabado;
	private JCheckBox chkDomingo;
	private JSpinner spHoraDesde;
	private JSpinner spHorarioHasta;
	private JPanel pnDias;
	private JPanel pnHoras;
	private JPanel pnBotones;
	private JScrollPane spListHorario;
	private JLabel lbDiaDesde;
	private JLabel lbDiaHasta;
	private JSpinner spDiaDesde;
	private JSpinner spDiaHasta;
	private JButton btSeleccionarMedico;
	private JTextField txMedicoSeleccionado;
	private Administrativo admin;
	
	/**
	 * Create the frame.
	 */
	public VentanaAsignarJornadaLaboral(Administrativo admin) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaAsignarJornadaLaboral.class.getResource("/iu/img/logo.jpeg")));
		trabajadores = new Trabajadores();
		this.medico = null;
		setTitle("Asignar Jornada Laboral a M\u00E9dico");
		setBounds(100, 100, 941, 533);
		panelAsignarJornadaLaboral = new JPanel();
		panelAsignarJornadaLaboral.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelAsignarJornadaLaboral.setLayout(new BorderLayout(0, 0));
		setContentPane(panelAsignarJornadaLaboral);
		panelAsignarJornadaLaboral.add(getPanelSeleccionarMedico(), BorderLayout.NORTH);
		panelAsignarJornadaLaboral.add(getPnJornada(), BorderLayout.CENTER);
		this.admin=admin;
	}

	private JPanel getPanelSeleccionarMedico() {
		if (panelSeleccionarMedico == null) {
			panelSeleccionarMedico = new JPanel();
			FlowLayout flowLayout = (FlowLayout) panelSeleccionarMedico.getLayout();
			flowLayout.setHgap(40);
			panelSeleccionarMedico.add(getLbSeleccionarMedico());
			panelSeleccionarMedico.add(getTxMedicoSeleccionado());
			panelSeleccionarMedico.add(getBtSeleccionarMedico());
		}
		return panelSeleccionarMedico;
	}
	private JLabel getLbSeleccionarMedico() {
		if (lbSeleccionarMedico == null) {
			lbSeleccionarMedico = new JLabel("Seleccione el m\u00E9dico:");
			lbSeleccionarMedico.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return lbSeleccionarMedico;
	}
	private JPanel getPnJornada() {
		if (pnJornada == null) {
			pnJornada = new JPanel();
			pnJornada.setLayout(new BorderLayout(0, 0));
			pnJornada.add(getPnAgregarHorario(), BorderLayout.NORTH);
			pnJornada.add(getSpListHorario(), BorderLayout.WEST);
			pnJornada.add(getSpListHorario(), BorderLayout.CENTER);
			pnJornada.add(getBtAsignarHorario(), BorderLayout.SOUTH);
		}
		return pnJornada;
	}
	private JPanel getPnAgregarHorario() {
		if (pnAgregarHorario == null) {
			pnAgregarHorario = new JPanel();
			pnAgregarHorario.setLayout(new GridLayout(3, 1, 0, 0));
			pnAgregarHorario.add(getPnDias());
			pnAgregarHorario.add(getPnHoras());
			pnAgregarHorario.add(getPnBotones());
		}
		return pnAgregarHorario;
	}
	private JLabel getLbA�adirDias() {
		if (lbA�adirDias == null) {
			lbA�adirDias = new JLabel("D\u00EDas:");
			lbA�adirDias.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return lbA�adirDias;
	}
	private JButton getBtA�adirDias() {
		if (btA�adirDias == null) {
			btA�adirDias = new JButton("A\u00F1adir horario");
			btA�adirDias.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Component[] components = getPnDias().getComponents();
					if(!checkHora()) {
						JOptionPane.showMessageDialog(null, "La hora de terminaci�n de la jornada no puede ser m�s temprana que la hora de comienzo.");
					}else if(!checkAlgunoSeleccionado(components)) {
						JOptionPane.showMessageDialog(null, "Debe seleccionar el d�a al que le desea asignar el horario antes de continuar.");
					}else {
						for(int i = 1; i < components.length; i++) {
							JCheckBox cb = (JCheckBox) components[i];
							if(cb.isSelected()) {
								String dia = cb.getText();
								String[] horaDesde = convertirStringHora(getSpHoraDesde());
								String[] horaHasta = convertirStringHora(getSpHorarioHasta());
								dia += " - " + horaDesde[0]+":"+horaDesde[1]+" - "+horaHasta[0]+":"+horaHasta[1];
								String[] diaDesde = convertirStringDia(getSpDiaDesde());
								String[] diaHasta = convertirStringDia(getSpDiaHasta());
								dia += " - " + diaDesde[2]+"-"+diaDesde[1]+"-"+diaDesde[5];
								dia += " - " + diaHasta[2]+"-"+diaHasta[1]+"-"+diaHasta[5];
								modeloListHorario.addElement(dia);
							}
						}
					}
				}
			});
			btA�adirDias.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btA�adirDias;
	}
	private boolean checkAlgunoSeleccionado(Component[] components) {
		for(int i = 1; i < components.length; i++) {
			if (((JCheckBox) components[i]).isSelected())
				return true;
		}
		return false;
	}
	private JLabel getLbHora() {
		if (lbHora == null) {
			lbHora = new JLabel("  Horario:");
			lbHora.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return lbHora;
	}
	private JLabel getLbDesde() {
		if (lbDesde == null) {
			lbDesde = new JLabel("  Desde:");
			lbDesde.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return lbDesde;
	}
	private JLabel getLbHasta() {
		if (lbHasta == null) {
			lbHasta = new JLabel("  Hasta:");
			lbHasta.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return lbHasta;
	}
	private JList<String> getListHorario() {
		if (listHorario == null) {
			listHorario = new JList<String>();
			listHorario.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
			modeloListHorario = new DefaultListModel<String>();
			listHorario.setFont(new Font("Tahoma", Font.PLAIN, 25));
			listHorario.setModel(modeloListHorario);
		}
		return listHorario;
	}
	private JButton getBtAsignarHorario() {
		if (btAsignarHorario == null) {
			btAsignarHorario = new JButton("Asignar Horario");
			btAsignarHorario.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(!modeloListHorario.isEmpty()) {
						String id = medico.getIdMedico();
						java.sql.Date diaDesde = new java.sql.Date(((Date) getSpDiaDesde().getValue()).getTime());
						java.sql.Date diaHasta = new java.sql.Date(((Date) getSpDiaHasta().getValue()).getTime());
						
						if(trabajadores.comprobarTrabajadorLibreDeHorario(id,diaDesde,diaHasta)) {
							int result= JOptionPane.showConfirmDialog(null, "Este m�dico ya tiene asignada una jornada laboral, �desea continuar?");
							if(result == JOptionPane.YES_OPTION) {
								asignarJornada(id);
							}
						}else {
							asignarJornada(id);
						}
					}else {
						JOptionPane.showMessageDialog(null, "Debe a�adir un horario para poder asign�rselo a alg�n m�dico.");
					}
				}
			});
			btAsignarHorario.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btAsignarHorario;
	}
	private void cerrar() {
		this.dispose();
	}
	private void asignarJornada(String id) {
		boolean listo = true;
		for(int i = 0; i<modeloListHorario.size(); i++) {
			String linea = modeloListHorario.get(i);
			String[] horario = linea.split(" - ");
			String dia = horario[0];
			String horaInicio = horario[1] + ":00";
			String horaFin = horario[2] + ":00";
			java.sql.Date diaDesde = new java.sql.Date(((Date) getSpDiaDesde().getValue()).getTime());
			java.sql.Date diaHasta = new java.sql.Date(((Date) getSpDiaHasta().getValue()).getTime());
			try {
				trabajadores.asignarJornadaLaboral(id, dia, horaInicio, horaFin,diaDesde,diaHasta,admin.getId());
			}catch(SQLException e) {
				JOptionPane.showMessageDialog(this, "El "+dia+" de "+horario[1]+" a "+horario[2]+", este m�dico ya trabaja, los dem�s d�as se han actualizado correctamente");
			}
		}
		if(listo) {
			JOptionPane.showMessageDialog(null, "Se ha asignado la jornada al m�dico indicado");
			cerrar();
		}
	}
	private JButton getBtEliminarHorario() {
		if (btEliminarHorario == null) {
			btEliminarHorario = new JButton("Eliminar horario");
			btEliminarHorario.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(!getListHorario().isSelectionEmpty()) {
						List<String> aEliminar = getListHorario().getSelectedValuesList();
						for (String linea : aEliminar) {
							modeloListHorario.removeElement(linea);
						}
					}else {
						JOptionPane.showMessageDialog(null, "No hay ninguna l�nea seleccionada en la lista.");
					}
				}
			});
			btEliminarHorario.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btEliminarHorario;
	}
	private JCheckBox getChkLunes() {
		if (chkLunes == null) {
			chkLunes = new JCheckBox("Lunes");
			chkLunes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return chkLunes;
	}
	private JCheckBox getChkMartes() {
		if (chkMartes == null) {
			chkMartes = new JCheckBox("Martes");
			chkMartes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return chkMartes;
	}
	private JCheckBox getChkMiercoles() {
		if (chkMiercoles == null) {
			chkMiercoles = new JCheckBox("Mi\u00E9rcoles");
			chkMiercoles.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return chkMiercoles;
	}
	private JCheckBox getChkJueves() {
		if (chkJueves == null) {
			chkJueves = new JCheckBox("Jueves");
			chkJueves.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return chkJueves;
	}
	private JCheckBox getChkViernes() {
		if (chkViernes == null) {
			chkViernes = new JCheckBox("Viernes");
			chkViernes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return chkViernes;
	}
	private JCheckBox getChkSabado() {
		if (chkSabado == null) {
			chkSabado = new JCheckBox("S\u00E1bado");
			chkSabado.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return chkSabado;
	}
	private JCheckBox getChkDomingo() {
		if (chkDomingo == null) {
			chkDomingo = new JCheckBox("Domingo");
			chkDomingo.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return chkDomingo;
	}
	private JSpinner getSpHoraDesde() {
		if (spHoraDesde == null) {
			try { 
				spHoraDesde = new JSpinner();
				spHoraDesde.setFont(new Font("Tahoma", Font.PLAIN, 25));
				SpinnerDateModel model = new SpinnerDateModel();
				model.setCalendarField(Calendar.MINUTE);
				String date = "09:00 AM";
				SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
				Date d = sdf.parse(date);
				spHoraDesde.setModel(new SpinnerDateModel(d, null, null, Calendar.HOUR_OF_DAY));
				spHoraDesde.setEditor(new JSpinner.DateEditor(spHoraDesde, "hh:mm a"));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return spHoraDesde;
	}
	private boolean checkHora() {
		String[] arrayHoraDesde = convertirStringHora(getSpHoraDesde());
		int horaDesde = Integer.parseInt(arrayHoraDesde[0]);
		String[] arrayHoraHasta = convertirStringHora(getSpHorarioHasta());
		int horaHasta = Integer.parseInt(arrayHoraHasta[0]);
		if(horaDesde < horaHasta || horaHasta == 0) return true;
		else return false;
	}
	private String[] convertirStringHora(JSpinner s) {
		String hora = s.getValue() + "";
		String[] arrayFecha = hora.split(" ");
		String[] arrayHora = arrayFecha[3].split(":");
		return arrayHora;
	}
	private JSpinner getSpHorarioHasta() {
		if (spHorarioHasta == null) {
			try {
				spHorarioHasta = new JSpinner();
				spHorarioHasta.setFont(new Font("Tahoma", Font.PLAIN, 25));
				SpinnerDateModel model = new SpinnerDateModel();
				model.setCalendarField(Calendar.MINUTE);
				String date = "03:00 PM";
				SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
				Date d = sdf.parse(date);
				spHorarioHasta.setModel(new SpinnerDateModel(d, null, null, Calendar.HOUR_OF_DAY));
				spHorarioHasta.setEditor(new JSpinner.DateEditor(spHorarioHasta, "hh:mm a"));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return spHorarioHasta;
	}
	private JPanel getPnDias() {
		if (pnDias == null) {
			pnDias = new JPanel();
			pnDias.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 5));
			pnDias.add(getLbA�adirDias());
			pnDias.add(getChkLunes());
			pnDias.add(getChkMartes());
			pnDias.add(getChkMiercoles());
			pnDias.add(getChkJueves());
			pnDias.add(getChkViernes());
			pnDias.add(getChkSabado());
			pnDias.add(getChkDomingo());
		}
		return pnDias;
	}
	private JPanel getPnHoras() {
		if (pnHoras == null) {
			pnHoras = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnHoras.getLayout();
			flowLayout.setHgap(15);
			pnHoras.add(getLbHora());
			pnHoras.add(getLbDesde());
			pnHoras.add(getSpHoraDesde());
			pnHoras.add(getLbHasta());
			pnHoras.add(getSpHorarioHasta());
		}
		return pnHoras;
	}
	private JPanel getPnBotones() {
		if (pnBotones == null) {
			pnBotones = new JPanel();
			pnBotones.add(getLbDiaDesde());
			pnBotones.add(getSpDiaDesde());
			pnBotones.add(getLbDiaHasta());
			pnBotones.add(getSpDiaHasta());
			pnBotones.add(getBtA�adirDias());
			pnBotones.add(getBtEliminarHorario());
		}
		return pnBotones;
	}
	private JScrollPane getSpListHorario() {
		if (spListHorario == null) {
			spListHorario = new JScrollPane();
			spListHorario.setViewportView(getListHorario());
		}
		return spListHorario;
	}
	private JLabel getLbDiaDesde() {
		if (lbDiaDesde == null) {
			lbDiaDesde = new JLabel("Desde:");
			lbDiaDesde.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return lbDiaDesde;
	}
	private JLabel getLbDiaHasta() {
		if (lbDiaHasta == null) {
			lbDiaHasta = new JLabel("Hasta:");
			lbDiaHasta.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return lbDiaHasta;
	}
	private JSpinner getSpDiaDesde() {
		if (spDiaDesde == null) {
			spDiaDesde = new JSpinner();
			spDiaDesde.setFont(new Font("Tahoma", Font.PLAIN, 25));
			try {
				SpinnerDateModel model = new SpinnerDateModel();
				model.setCalendarField(Calendar.DATE);
//				String date = "12-Oct-2018";
				Date date = Calendar.getInstance().getTime();
//				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
//				Date d = sdf.parse(date.toString());
				spDiaDesde.setModel(new SpinnerDateModel(date, null, null, Calendar.DAY_OF_MONTH));
				spDiaDesde.setEditor(new JSpinner.DateEditor(spDiaDesde, "dd-MMM-yyyy"));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return spDiaDesde;
	}
	private JSpinner getSpDiaHasta() {
		if (spDiaHasta == null) {
			spDiaHasta = new JSpinner();
			spDiaHasta.setFont(new Font("Tahoma", Font.PLAIN, 25));
			try {
				SpinnerDateModel model = new SpinnerDateModel();
				model.setCalendarField(Calendar.DATE);
				String date = "31-dic-2018";
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				Date d = sdf.parse(date);
				spDiaHasta.setModel(new SpinnerDateModel(d, null, null, Calendar.DAY_OF_MONTH));
				spDiaHasta.setEditor(new JSpinner.DateEditor(spDiaHasta, "dd-MMM-yyyy"));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return spDiaHasta;
	}
	private String[] convertirStringDia(JSpinner s) {
		String dia = s.getValue() + "";
		String[] arrayFecha = dia.split(" ");
		return arrayFecha;
	}
	private JButton getBtSeleccionarMedico() {
		if (btSeleccionarMedico == null) {
			btSeleccionarMedico = new JButton("Seleccionar Medico...");
			VentanaBuscadorMedicos ventana = new VentanaBuscadorMedicos(getTxMedicoSeleccionado(),this);
			btSeleccionarMedico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentana(ventana);
				}
			});
			btSeleccionarMedico.setFont(new Font("Tahoma", Font.PLAIN, 23));
		}
		return btSeleccionarMedico;
	}
	private void mostrarVentana(JDialog ventana) {
		ventana.setLocationRelativeTo(this);
		ventana.setModal(true);
		ventana.setVisible(true);
	}
	private JTextField getTxMedicoSeleccionado() {
		if (txMedicoSeleccionado == null) {
			txMedicoSeleccionado = new JTextField();
			txMedicoSeleccionado.setEditable(false);
			txMedicoSeleccionado.setFont(new Font("Tahoma", Font.PLAIN, 24));
			txMedicoSeleccionado.setColumns(10);
		}
		return txMedicoSeleccionado;
	}

	@Override
	public void setMedico(Medico medico) {
		this.medico = medico;
	}
}
