package iu.administrativo;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import iu.VentanaBuscadorMedicos;
import iu.VentanaBuscadorPacientes;
import iu.medico.VentanaHistorialDePaciente;
import logica.Administrativo;
import logica.Cita;
import logica.CrearCita;
import logica.ListarCitas;
import logica.ListarPacientes;
import logica.Medico;
import logica.Paciente;
import logica.SendEmail;

public class VentanaCrearCitas extends JDialog {

	private static final long serialVersionUID = 1L;

	private JPanel panelPrincipal;
	private JPanel pnAsignarMedicoPaciente;
	private JPanel pnAsignarHorarioUrgente;
	private JPanel pnInformacionPaciente;
	private JButton btnSiguienteVentana1;
	private JButton btnSiguienteVentana2;
	private JButton btnFinalizar;
	private JButton btnAtrasVentana2;
	private JButton btnAtrasVentana3;
	private JLabel lbHorarioInicio;
	private JLabel lbHorarioFinal;
	private JCheckBox chckbxCitaUrgente;
	private JSpinner spHorarioInicial;
	private JSpinner spHorarioFin;
	private JLabel lblNewLabel;
	private JPanel pnPaciente;
	private JPanel pnMedico;
	private JLabel lbNombre;
	private JLabel lbApellidos;
	private JTextField txNombre;
	private JTextField txApellidos;
	private JLabel lbDni;
	private JTextField txDni;
	private JButton btnHistorial;
	private JScrollPane scrollPaneInfoCita;
	private JTextArea txtInfoPaciente;
	private JLabel lblInformacionContacto;
	private JLabel lbElegirSala;
	private JComboBox<String> cbSalas;
	private JButton btnNewButton;
	private ListarCitas listar;
	private List<Cita> citas;
	private Paciente paciente;
	private JScrollPane scrollMedicos;
	private JList<Medico> listMedicos;
	private DefaultListModel<Medico> modeloListMedicos;
	private JButton btAñadirMédico;
	private JButton btnEliminarMedico;
	private JButton btBuscadorPacientes;
	private Administrativo administrador;
	/**
	 * Create the frame.
	 */
	public VentanaCrearCitas(Administrativo administrador) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaOperacionesAdministrativo.class.getResource("/iu/img/logo.jpeg")));
		listar = new ListarCitas();
		citas = new ArrayList<Cita>();
		setResizable(false);
		setTitle("Fijar citas");
		setBounds(100, 100, 616, 351);
		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panelPrincipal);
		panelPrincipal.setLayout(new CardLayout(0, 0));
		panelPrincipal.add(getPnAsignarMedicoPaciente(), "AsignarPacienteMedico");
		panelPrincipal.add(getPnAsignarHorarioUrgente(), "AsignarHorarioUrgente");
		panelPrincipal.add(getPnInformacionPaciente(), "AsignarSalaIInformacion");
		this.administrador=administrador;

	}

	private JPanel getPnAsignarMedicoPaciente() {
		if (pnAsignarMedicoPaciente == null) {
			pnAsignarMedicoPaciente = new JPanel();
			pnAsignarMedicoPaciente.setLayout(null);
			pnAsignarMedicoPaciente.add(getBtnSiguienteVentana1());
			pnAsignarMedicoPaciente.add(getPnPaciente());
			pnAsignarMedicoPaciente.add(getPnMedico());
		}
		return pnAsignarMedicoPaciente;
	}

	private JPanel getPnAsignarHorarioUrgente() {
		if (pnAsignarHorarioUrgente == null) {
			pnAsignarHorarioUrgente = new JPanel();
			pnAsignarHorarioUrgente.setLayout(null);
			pnAsignarHorarioUrgente.add(getBtnSiguienteVentana2());
			pnAsignarHorarioUrgente.add(getBtnAtrasVentana2());
			pnAsignarHorarioUrgente.add(getLbHorarioInicio());
			pnAsignarHorarioUrgente.add(getLbHorarioFinal());
			pnAsignarHorarioUrgente.add(getChckbxCitaUrgente());
			pnAsignarHorarioUrgente.add(getSpHorarioInicial());
			pnAsignarHorarioUrgente.add(getSpHorarioFin());
			pnAsignarHorarioUrgente.add(getLblNewLabel());
			pnAsignarHorarioUrgente.add(getLbElegirSala());
			pnAsignarHorarioUrgente.add(getCbSalas());
			pnAsignarHorarioUrgente.add(getBtnNewButton());
		}
		return pnAsignarHorarioUrgente;
	}

	private JPanel getPnInformacionPaciente() {
		if (pnInformacionPaciente == null) {
			pnInformacionPaciente = new JPanel();
			pnInformacionPaciente.setLayout(null);
			pnInformacionPaciente.add(getBtnFinalizar());
			pnInformacionPaciente.add(getBtnAtrasVentana3());
			pnInformacionPaciente.add(getScrollPaneInfoCita());
		}
		return pnInformacionPaciente;
	}

	private JButton getBtnSiguienteVentana1() {
		if (btnSiguienteVentana1 == null) {
			btnSiguienteVentana1 = new JButton("Siguiente");
			btnSiguienteVentana1.setFont(new Font("Tahoma", Font.PLAIN, 13));
			btnSiguienteVentana1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String nombre = getTxNombre().getText();
					String apellidos = getTxApellidos().getText();
					String dni = getTxDni().getText();

					if (nombre.trim().equals("")) {
						JOptionPane.showMessageDialog(null,
								"Debe introducir el nombre del paciente antes de continuar.");
						getTxNombre().requestFocus();
					} else if (apellidos.trim().equals("")) {
						JOptionPane.showMessageDialog(null,
								"Debe introducir los apellidos del paciente antes de continuar.");
						getTxApellidos().requestFocus();
					}else if(modeloListMedicos.isEmpty()) {
						JOptionPane.showMessageDialog(null, "Debe indicar el o los médicos a los que se les asignará la cita con este paciente.");
					} else {
						int cont = 0;
						for(int i =0; i < modeloListMedicos.size(); i++) {
							Cita cita = new Cita();
							if(cont>0) {
								int cita1 = Integer.parseInt(citas.get(0).getIdCita()) + 1;
								cita.setIdCita(cita1+"");
							}
							cita.setIdAdmin("1");
							if(!dni.trim().equals("")) {
								cita.setDniPaciente(dni);
							}
							cita.setIdMedico(modeloListMedicos.getElementAt(i).getIdMedico());
							citas.add(cita);
							cont++;
						}

						ListarPacientes lp = new ListarPacientes();
						paciente = lp.getPacientePorID(getTxDni().getText());
						if (paciente != null) //En teoria ya esta validado en la logica
							getTxtInfoPaciente().setText(paciente.toString());

						((CardLayout) panelPrincipal.getLayout()).next(panelPrincipal);
					}
				}
			});
			btnSiguienteVentana1.setBounds(442, 256, 143, 45);
		}
		return btnSiguienteVentana1;
	}

	private JButton getBtnSiguienteVentana2() {
		if (btnSiguienteVentana2 == null) {
			btnSiguienteVentana2 = new JButton("Siguiente");
			btnSiguienteVentana2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
//					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM hh:mm");
					
//					String horaInicial = dateFormat.format(((Date) spHorarioInicial.getValue()).getTime());
//					String horaFinal = dateFormat.format(((Date) spHorarioFin.getValue()).getTime());
					
					java.sql.Date horaInicial = new java.sql.Date(((Date) spHorarioInicial.getValue()).getTime());
					java.sql.Date horaFinal = new java.sql.Date(((Date) spHorarioFin.getValue()).getTime());
					
//					horaInicial.concat(":00.0");
//					horaFinal.concat(":00.0");
					// dia mes
					// 2008-08-08 20:08:08 formato de fecha para la base de
					// datos
					for(Cita cita : citas) {
						if (((Date) spHorarioInicial.getValue()).getTime() > ((Date) spHorarioFin.getValue()).getTime()) {
							JOptionPane.showMessageDialog(null,
									"La hora de fin de la cita no puede ser antes que la hora de inicio.");
							return;
						}else if (((Date) spHorarioInicial.getValue()).getTime() < ( Calendar.getInstance().getTime().getTime())) {
							JOptionPane.showMessageDialog(null,
									"La cita no se puede asignar antes del dia de hoy.");
							return;
						} else if (listar.getHorarioCoinciden(horaInicial, cita.getIdMedico())
								&& !chckbxCitaUrgente.isSelected()) {
							JOptionPane.showMessageDialog(null,
									"La hora seleccionada de inicio de la cita ya esta ocupada");
							return;
						} else if (!listar.coincideJornadaHorario((Date) spHorarioFin.getValue(),
								(Date) spHorarioFin.getValue(), cita.getIdMedico())) {
							JOptionPane.showMessageDialog(null,
									"El medico seleccionado no trabaja a en la franja de tiempo de la cita.");
							return;
						} else if (!listar.coincideJornadaHorario((Date) spHorarioInicial.getValue(),
								(Date) spHorarioFin.getValue(), cita.getIdMedico()) && chckbxCitaUrgente.isSelected()) {
	
							JOptionPane.showMessageDialog(null,
									"El medico seleccionado no trabaja a en la franja de tiempo de la cita pero al ser la cita urgente se le asignara de todas maneras.");
							return;
						} else if (cita.getIdSala() == "") {
								JOptionPane.showMessageDialog(null, "Debe elegir una sala para continuar");
								return;
						} else if (chckbxCitaUrgente.isSelected()) {
								cita.setTipo("URGENTE");
						}else {
								cita.setTipo("ORDINARIA");
						}
						cita.setIdSala((String) cbSalas.getSelectedItem());
//						cita.setHoraComienzo(horaInicial.concat(":00"));
//						cita.setHoraFinal(horaFinal.concat(":00"));
						cita.setHoraComienzo(horaInicial);
						cita.setHoraFinal(horaFinal);
						cita.setIdAdmin(administrador.getId());
						}
						((CardLayout) panelPrincipal.getLayout()).next(panelPrincipal);
					}
				}
			);
			btnSiguienteVentana2.setBounds(441, 254, 144, 47);
		}
		return btnSiguienteVentana2;
	}

	private JButton getBtnFinalizar() {
		if (btnFinalizar == null) {
			btnFinalizar = new JButton("Finalizar");
			btnFinalizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					for(Cita cita: citas) {
						cita.setInfoContacto(getTxtInfoPaciente().getText());
						cita.setEstadoCita("NO ACUDIDO");
						CrearCita cc = new CrearCita(cita,getTxNombre().getText(),getTxApellidos().getText());
						
						try{cc.crearCita();} catch (SQLException ex) { ex.printStackTrace();}
						
						if (cita.getTipo() == "URGENTE" && paciente != null)
							new SendEmail(paciente);
					}
					JOptionPane.showMessageDialog(null, "Se ha creado la cita correctamente");
					dispose();
				}
			});
			btnFinalizar.setBounds(441, 254, 144, 47);
		}
		return btnFinalizar;
	}

	private JButton getBtnAtrasVentana2() {
		if (btnAtrasVentana2 == null) {
			btnAtrasVentana2 = new JButton("Atr\u00E1s");
			btnAtrasVentana2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					((CardLayout) panelPrincipal.getLayout()).previous(panelPrincipal);
				}
			});
			btnAtrasVentana2.setBounds(12, 254, 144, 47);
		}
		return btnAtrasVentana2;
	}

	private JButton getBtnAtrasVentana3() {
		if (btnAtrasVentana3 == null) {
			btnAtrasVentana3 = new JButton("Atr\u00E1s");
			btnAtrasVentana3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					((CardLayout) panelPrincipal.getLayout()).previous(panelPrincipal);
				}
			});
			btnAtrasVentana3.setBounds(12, 254, 144, 47);
		}
		return btnAtrasVentana3;
	}

	private JLabel getLbHorarioInicio() {
		if (lbHorarioInicio == null) {
			lbHorarioInicio = new JLabel("Asigne el horario de inicio de la cita");
			lbHorarioInicio.setBounds(32, 39, 198, 16);
		}
		return lbHorarioInicio;
	}

	private JLabel getLbHorarioFinal() {
		if (lbHorarioFinal == null) {
			lbHorarioFinal = new JLabel("Asigne el horario de fin de la cita");
			lbHorarioFinal.setBounds(32, 82, 178, 16);
		}
		return lbHorarioFinal;
	}

	private JCheckBox getChckbxCitaUrgente() {
		if (chckbxCitaUrgente == null) {
			chckbxCitaUrgente = new JCheckBox("");
			chckbxCitaUrgente.setBounds(247, 113, 31, 24);
			chckbxCitaUrgente.setHorizontalTextPosition(SwingConstants.LEFT);
		}
		return chckbxCitaUrgente;
	}

	private JSpinner getSpHorarioInicial() {
		if (spHorarioInicial == null) {
			spHorarioInicial = new JSpinner();
			spHorarioInicial.setModel(new SpinnerDateModel(Calendar.getInstance().getTime(),
					null, null, Calendar.DAY_OF_MONTH));
			spHorarioInicial.setBounds(250, 36, 105, 22);
		}
		return spHorarioInicial;
	}

	private JSpinner getSpHorarioFin() {
		if (spHorarioFin == null) {
			spHorarioFin = new JSpinner();
			spHorarioFin.setModel(new SpinnerDateModel(Calendar.getInstance().getTime(),
					null, null, Calendar.DAY_OF_MONTH));
			spHorarioFin.setBounds(250, 79, 105, 22);
		}
		return spHorarioFin;
	}

	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("Seleccione si la cita es urgente");
			lblNewLabel.setBounds(32, 121, 171, 16);
		}
		return lblNewLabel;
	}

	private JPanel getPnPaciente() {
		if (pnPaciente == null) {
			pnPaciente = new JPanel();
			pnPaciente
					.setBorder(new TitledBorder(null, "Paciente", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnPaciente.setBounds(12, 13, 573, 122);
			pnPaciente.setLayout(null);
			pnPaciente.add(getLbNombre());
			pnPaciente.add(getLbApellidos());
			pnPaciente.add(getTxNombre());
			pnPaciente.add(getTxApellidos());
			pnPaciente.add(getLbDni());
			pnPaciente.add(getTxDni());
			pnPaciente.add(getBtnHistorial());
			pnPaciente.add(getBtBuscadorPacientes());
		}
		return pnPaciente;
	}

	private JPanel getPnMedico() {
		if (pnMedico == null) {
			pnMedico = new JPanel();
			pnMedico.setLayout(null);
			pnMedico.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "M\u00E9dico",
					TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			pnMedico.setBounds(12, 135, 573, 122);
			pnMedico.add(getScrollMedicos());
			pnMedico.add(getBtAñadirMédico());
			pnMedico.add(getBtnEliminarMedico());
		}
		return pnMedico;
	}

	private JLabel getLbNombre() {
		if (lbNombre == null) {
			lbNombre = new JLabel("Nombre:");
			lbNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
			lbNombre.setBounds(12, 17, 76, 30);
		}
		return lbNombre;
	}

	private JLabel getLbApellidos() {
		if (lbApellidos == null) {
			lbApellidos = new JLabel("Apellidos:");
			lbApellidos.setFont(new Font("Tahoma", Font.PLAIN, 13));
			lbApellidos.setBounds(183, 20, 83, 24);
		}
		return lbApellidos;
	}

	public JTextField getTxNombre() {
		if (txNombre == null) {
			txNombre = new JTextField();
			txNombre.setBounds(12, 39, 124, 26);
			txNombre.setColumns(10);
		}
		return txNombre;
	}

	public JTextField getTxApellidos() {
		if (txApellidos == null) {
			txApellidos = new JTextField();
			txApellidos.setColumns(10);
			txApellidos.setBounds(183, 40, 337, 24);
		}
		return txApellidos;
	}

	private JLabel getLbDni() {
		if (lbDni == null) {
			lbDni = new JLabel("DNI:");
			lbDni.setFont(new Font("Tahoma", Font.PLAIN, 13));
			lbDni.setBounds(12, 78, 56, 16);
		}
		return lbDni;
	}

	public JTextField getTxDni() {
		if (txDni == null) {
			txDni = new JTextField();
			txDni.setColumns(10);
			txDni.setBounds(12, 93, 168, 24);
		}
		return txDni;
	}

	private JButton getBtnHistorial() {
		if (btnHistorial == null) {
			btnHistorial = new JButton("Ver Historial");
			btnHistorial.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (getTxDni().getText().isEmpty() || getTxNombre().getText().isEmpty()
							|| getTxApellidos().getText().isEmpty()) {
						JOptionPane.showMessageDialog(getBtnHistorial(),
								"Rellena todos los campos sobre el paciente para ver el historial");
					} else {
						ListarPacientes lp = new ListarPacientes();
						try {
							paciente = lp.getPacientePorID(getTxDni().getText());

							if (paciente.getNombrePaciente().equals(getTxNombre().getText())
									&& paciente.getApellidoPaciente().equals(getTxApellidos().getText())
									&& paciente.getDniPaciente().equals(getTxDni().getText()))
								mostrarVentanaHistorialDePaciente(getTxDni().getText(), getTxNombre().getText(),
										getTxApellidos().getText());
							else {
								JOptionPane.showMessageDialog(getBtnHistorial(),
										"Los datos introducidos no son correctos, el dni no coincide con el nombre y apellidos introducidos");
							}
						} catch (Exception ex) {
							JOptionPane.showMessageDialog(getBtnHistorial(), "El dni del paciente no es válido o no hay ningún paciente con ese dni");
						}
					}
				}
			});
			btnHistorial.setBounds(431, 94, 89, 23);
		}
		return btnHistorial;
	}

	private void mostrarVentanaHistorialDePaciente(String dni, String nombre, String apellido) {
		VentanaHistorialDePaciente v = new VentanaHistorialDePaciente(dni, nombre, apellido);
		v.setLocationRelativeTo(this);
		v.setModal(true);
		v.setVisible(true);
	}

	private JScrollPane getScrollPaneInfoCita() {
		if (scrollPaneInfoCita == null) {
			scrollPaneInfoCita = new JScrollPane();
			scrollPaneInfoCita.setBounds(10, 11, 575, 232);
			scrollPaneInfoCita.setViewportView(getTxtInfoPaciente());
			scrollPaneInfoCita.setColumnHeaderView(getLblInformacionContacto());
		}
		return scrollPaneInfoCita;
	}

	private JTextArea getTxtInfoPaciente() {
		if (txtInfoPaciente == null) {
			txtInfoPaciente = new JTextArea();
		}
		return txtInfoPaciente;
	}

	private JLabel getLblInformacionContacto() {
		if (lblInformacionContacto == null) {
			lblInformacionContacto = new JLabel("Informaci\u00F3n de contacto:");
		}
		return lblInformacionContacto;
	}

	private JLabel getLbElegirSala() {
		if (lbElegirSala == null) {
			lbElegirSala = new JLabel("Elegir sala para la cita");
			lbElegirSala.setBounds(32, 165, 171, 14);
		}
		return lbElegirSala;
	}

	private JComboBox<String> getCbSalas() {
		if (cbSalas == null) {
			cbSalas = new JComboBox<String>();
			cbSalas.setEnabled(false);
			cbSalas.setBounds(250, 162, 102, 20);
		}
		return cbSalas;
	}

	private JButton getBtnNewButton() {
		if (btnNewButton == null) {
			btnNewButton = new JButton("Cargar Salas Disponibles");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM hh:mm");
					String horaInicial = dateFormat.format(((Date) spHorarioInicial.getValue()).getTime());

					if (listar.getSalasDisponibles(horaInicial).length == 0) {
						JOptionPane.showMessageDialog(null, "No hay salas disponibles en el horario seleccionado");
					} else {
						cbSalas.setModel(new DefaultComboBoxModel<String>(listar.getSalasDisponibles(horaInicial)));
						cbSalas.setEnabled(true);
					}
				}
			});
			btnNewButton.setBounds(362, 161, 149, 23);
		}
		return btnNewButton;
	}
	private JScrollPane getScrollMedicos() {
		if (scrollMedicos == null) {
			scrollMedicos = new JScrollPane();
			scrollMedicos.setBounds(12, 24, 396, 85);
			scrollMedicos.setViewportView(getListMedicos());
		}
		return scrollMedicos;
	}
	private JList<Medico> getListMedicos() {
		if (listMedicos == null) {
			listMedicos = new JList<Medico>();
			modeloListMedicos = new DefaultListModel<Medico>();
			listMedicos.setModel(modeloListMedicos);
		}
		return listMedicos;
	}
	private JButton getBtAñadirMédico() {
		if (btAñadirMédico == null) {
			btAñadirMédico = new JButton("A\u00F1adir m\u00E9dico...");
			btAñadirMédico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentana(new VentanaBuscadorMedicos(modeloListMedicos,null));
				}
			});
			btAñadirMédico.setBounds(431, 22, 130, 37);
		}
		return btAñadirMédico;
	}
	private void mostrarVentana(JDialog ventana) {
		ventana.setLocationRelativeTo(this);
		ventana.setModal(true);
		ventana.setVisible(true);
	}
	private JButton getBtnEliminarMedico() {
		if (btnEliminarMedico == null) {
			btnEliminarMedico = new JButton("Eliminar m\u00E9dico");
			btnEliminarMedico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(!getListMedicos().isSelectionEmpty()) {
						List<Medico> aEliminar = getListMedicos().getSelectedValuesList();
						for (Medico med : aEliminar) {
							modeloListMedicos.removeElement(med);
						}
					}else {  
						JOptionPane.showMessageDialog(null, "No hay ningún médico seleccionado.");
					}
				}
			});
			btnEliminarMedico.setBounds(431, 72, 130, 37);
		}
		return btnEliminarMedico;
	}
	private JButton getBtBuscadorPacientes() {
		if (btBuscadorPacientes == null) {
			btBuscadorPacientes = new JButton("Buscar Paciente...\r\n");
			VentanaCrearCitas ventana= this;
			btBuscadorPacientes.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					VentanaBuscadorPacientes buscador = new VentanaBuscadorPacientes(ventana);
					mostrarVentana(buscador);
				}
			});
			btBuscadorPacientes.setBounds(265, 93, 154, 25);
		}
		return btBuscadorPacientes;
	}
}
