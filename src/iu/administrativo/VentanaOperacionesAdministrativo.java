package iu.administrativo;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import iu.VentanaLogin;
import logica.Administrativo;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

public class VentanaOperacionesAdministrativo extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel panelOpAdministrativo;
	private JLabel lbOperacionesAdministrativo;
	private JPanel pnBotonesOpAdministrativo;
	private JButton btAsignarJornadaLaboral;
	private JButton btAñadirMedico;
	private JButton btnCrearCitas;
	private JButton btAñadirPaciente;

	private Administrativo admin;
	private JButton btDesconectarse;
	private JButton btnModificarMedico;
	private JButton btnModificarPaciente;
	private JButton btAceptarCitas;
	private JButton btEstadisticos;

	public VentanaOperacionesAdministrativo(Administrativo admin) {
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(VentanaOperacionesAdministrativo.class.getResource("/iu/img/logo.jpeg")));
		setTitle("Operaciones");
		setBounds(100, 100, 752, 384);
		panelOpAdministrativo = new JPanel();
		panelOpAdministrativo.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelOpAdministrativo.setLayout(new BorderLayout(0, 0));
		setContentPane(panelOpAdministrativo);
		panelOpAdministrativo.add(getLbOperacionesAdministrativo(), BorderLayout.NORTH);
		panelOpAdministrativo.add(getPnBotonesOpAdministrativo(), BorderLayout.CENTER);
		this.admin = admin;
	}

	private JLabel getLbOperacionesAdministrativo() {
		if (lbOperacionesAdministrativo == null) {
			lbOperacionesAdministrativo = new JLabel("Elija la operaci\u00F3n que desea realizar:");
			lbOperacionesAdministrativo.setFont(new Font("Tahoma", Font.PLAIN, 30));
		}
		return lbOperacionesAdministrativo;
	}

	private JPanel getPnBotonesOpAdministrativo() {
		if (pnBotonesOpAdministrativo == null) {
			pnBotonesOpAdministrativo = new JPanel();
			pnBotonesOpAdministrativo.setLayout(new GridLayout(0, 1, 10, 2));
			pnBotonesOpAdministrativo.add(getBtAsignarJornadaLaboral());
			pnBotonesOpAdministrativo.add(getBtnCrearCitas());
			pnBotonesOpAdministrativo.add(getBtAceptarCitas());
			pnBotonesOpAdministrativo.add(getBtAñadirPaciente());
			pnBotonesOpAdministrativo.add(getBtAñadirMedico());
			pnBotonesOpAdministrativo.add(getBtnModificarMedico());
			pnBotonesOpAdministrativo.add(getBtnModificarPaciente());
			pnBotonesOpAdministrativo.add(getBtEstadisticos());
			pnBotonesOpAdministrativo.add(getBtDesconectarse());
		}
		return pnBotonesOpAdministrativo;
	}

	private JButton getBtAsignarJornadaLaboral() {
		if (btAsignarJornadaLaboral == null) {
			btAsignarJornadaLaboral = new JButton("Asignar Jornada Laboral a un M\u00E9dico");
			btAsignarJornadaLaboral.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					mostrarVentana(new VentanaAsignarJornadaLaboral(admin));
				}
			});
			btAsignarJornadaLaboral.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btAsignarJornadaLaboral;
	}

	private JButton getBtAñadirMedico() {
		if (btAñadirMedico == null) {
			btAñadirMedico = new JButton("A\u00F1adir Medico");
			btAñadirMedico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentana(new VentanaCrearMedicos());
				}
			});
			btAñadirMedico.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btAñadirMedico;
	}

	private void mostrarVentana(JDialog ventana) {
		ventana.setLocationRelativeTo(this);
		ventana.setModal(true);
		ventana.setVisible(true);
	}

	private JButton getBtnCrearCitas() {
		if (btnCrearCitas == null) {
			btnCrearCitas = new JButton("Fijar citas");
			btnCrearCitas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentana(new VentanaCrearCitas(admin));
				}
			});
			btnCrearCitas.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btnCrearCitas;
	}

	private JButton getBtAñadirPaciente() {
		if (btAñadirPaciente == null) {
			btAñadirPaciente = new JButton("A\u00F1adir Paciente");
			btAñadirPaciente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentana(new VentanaCrearPacientes());
				}
			});
			btAñadirPaciente.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btAñadirPaciente;
	}

	private JButton getBtDesconectarse() {
		if (btDesconectarse == null) {
			btDesconectarse = new JButton("Desconectarse");
			btDesconectarse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					mostrarLogin();
				}
			});
			btDesconectarse.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btDesconectarse;
	}

	private void mostrarLogin() {
		this.setVisible(false);
		VentanaLogin ventana = new VentanaLogin();
		ventana.setLocationRelativeTo(this);
		ventana.setVisible(true);
	}

	private JButton getBtnModificarMedico() {
		if (btnModificarMedico == null) {
			btnModificarMedico = new JButton("Modificar Medico");
			btnModificarMedico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentana(new VentanaModificarMedico());
				}
			});
			btnModificarMedico.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btnModificarMedico;
	}

	private JButton getBtnModificarPaciente() {
		if (btnModificarPaciente == null) {
			btnModificarPaciente = new JButton("Modificar Paciente");
			btnModificarPaciente.setFont(new Font("Tahoma", Font.PLAIN, 25));
			btnModificarPaciente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					mostrarVentana(new VentanaModificarPaciente());
				}
			});
		}
		return btnModificarPaciente;
	}
	private JButton getBtAceptarCitas() {
		if (btAceptarCitas == null) {
			btAceptarCitas = new JButton("Aceptar Citas");
			btAceptarCitas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentana(new VentanaAceptarCitas(admin));
				}
			});
			btAceptarCitas.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btAceptarCitas;
	}
	private JButton getBtEstadisticos() {
		if (btEstadisticos == null) {
			btEstadisticos = new JButton("Ver Datos Estad\u00EDsticos de las Enfermedades");
			btEstadisticos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarVentana(new VentanaEstadisticosEnfermedades());
				}
			});
			btEstadisticos.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return btEstadisticos;
	}
}
