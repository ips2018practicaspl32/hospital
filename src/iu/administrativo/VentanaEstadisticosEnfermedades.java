package iu.administrativo;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import iu.ModeloNoEditable;
import logica.Enfermedad;
import logica.EstadisticosEnfermedades;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class VentanaEstadisticosEnfermedades extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel pnIntroducirFechas;
	private JLabel lbFechas;
	private JSpinner spDiaDesde;
	private JSpinner spDiaHasta;
	private JPanel pnRankingEnfermedades;
	private JScrollPane scrollTable;
	private JTable tableRanking;
	private ModeloNoEditable modeloTabla;
	private JButton btAnalizar;
	
	private Hashtable<String, Double> ranking;
	private JPanel pnMasInfo;
	private JButton btMasInfo;
	private JPanel pnMasInfoSBt;
	private JLabel lbCodigo;
	private JTextField txCodigo;
	private JLabel lbDescripcion;
	private JLabel lbNumeroPersonas;
	private JTextField txNumeroPersonas;
	private JPanel pnCodigo;
	private JPanel pnDescripcion;
	private JPanel pnNumeroPersonas;
	private JTextArea taDescripcion;

	/**
	 * 
	 */
	public VentanaEstadisticosEnfermedades() {
		setTitle("Estad\u00EDsticas de las Enfermedades");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaEstadisticosEnfermedades.class.getResource("/iu/img/logo.jpeg")));
		setBounds(100, 100, 993, 431);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPnIntroducirFechas(), BorderLayout.NORTH);
		contentPane.add(getPnRankingEnfermedades(), BorderLayout.WEST);
		contentPane.add(getPnMasInfo(), BorderLayout.CENTER);
		
		getBtAnalizar().doClick();
	}

	private JPanel getPnIntroducirFechas() {
		if (pnIntroducirFechas == null) {
			pnIntroducirFechas = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnIntroducirFechas.getLayout();
			flowLayout.setHgap(13);
			pnIntroducirFechas.add(getLbFechas());
			pnIntroducirFechas.add(getSpDiaDesde());
			pnIntroducirFechas.add(getSpDiaHasta());
			pnIntroducirFechas.add(getBtAnalizar());
		}
		return pnIntroducirFechas;
	}
	private JLabel getLbFechas() {
		if (lbFechas == null) {
			lbFechas = new JLabel("Introduzca un per\u00EDodo de tiempo:");
			lbFechas.setFont(new Font("Tahoma", Font.PLAIN, 17));
		}
		return lbFechas;
	}
	private JSpinner getSpDiaDesde() {
		if (spDiaDesde == null) {
			spDiaDesde = new JSpinner();
			spDiaDesde.setFont(new Font("Tahoma", Font.PLAIN, 17));
			try {
				SpinnerDateModel model = new SpinnerDateModel();
				model.setCalendarField(Calendar.DATE);
				Calendar c = Calendar.getInstance();
				c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
				Date date = c.getTime();
				spDiaDesde.setModel(new SpinnerDateModel(date, null, null, Calendar.DAY_OF_MONTH));
				spDiaDesde.setEditor(new JSpinner.DateEditor(spDiaDesde, "dd-MMM-yyyy"));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return spDiaDesde;
	}
	private JSpinner getSpDiaHasta() {
		if (spDiaHasta == null) {
			spDiaHasta = new JSpinner();
			spDiaHasta.setFont(new Font("Tahoma", Font.PLAIN, 17));
			try {
				SpinnerDateModel model = new SpinnerDateModel();
				model.setCalendarField(Calendar.DATE);
				Calendar c = Calendar.getInstance();
				c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
				Date date = c.getTime();
				spDiaHasta.setModel(new SpinnerDateModel(date, null, null, Calendar.DAY_OF_MONTH));
				spDiaHasta.setEditor(new JSpinner.DateEditor(spDiaHasta, "dd-MMM-yyyy"));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return spDiaHasta;
	}
	private JPanel getPnRankingEnfermedades() {
		if (pnRankingEnfermedades == null) {
			pnRankingEnfermedades = new JPanel();
			pnRankingEnfermedades.setLayout(new BorderLayout(0, 0));
			pnRankingEnfermedades.add(getScrollBuscador(), BorderLayout.CENTER);
		}
		return pnRankingEnfermedades;
	}
	private JScrollPane getScrollBuscador() {
		if (scrollTable == null) {
			scrollTable = new JScrollPane();
			scrollTable.setViewportView(getTableRanking());
		}
		return scrollTable;
	}
	private JTable getTableRanking() {
		if (tableRanking == null) {
			String[] columnas = {"Cod","Enfermedad", "Frecuencia"};
			modeloTabla=new ModeloNoEditable(columnas, 0);
			tableRanking = new JTable(modeloTabla);
			tableRanking.setBounds(0, 0, 1, 1);
			tableRanking.getTableHeader().setReorderingAllowed(false);
			tableRanking.setRowHeight(20);
			tableRanking.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(10);
		}
		return tableRanking;
	}
	private void eliminarFilasTabla() {
		if(modeloTabla.getRowCount()>0)
		{
			while(modeloTabla.getRowCount()>0)
				modeloTabla.removeRow(0);
		}
	}
	private void añadirFilas(java.sql.Date ini,java.sql.Date fin) {
		Object[] nuevaFila = new Object[3];
		List<Enfermedad> enfermedades = EstadisticosEnfermedades.buscarEnfermedadesPeriodo(ini, fin);
		
		Hashtable<String, Double> frecuencias2 = frecuencias(enfermedades);
		ranking = new Hashtable<String, Double>(frecuencias2);
		int size = frecuencias2.size();
			
		for(int i = 0; i < size; i++)
		{
			String max = getMax(frecuencias2);
			nuevaFila[0] = max;
			nuevaFila[1]= getDescripcion(enfermedades, max);
			nuevaFila[2]= (frecuencias2.get(max)/enfermedades.size()) * 100 + " %";
			
			frecuencias2.remove(max);
			
			modeloTabla.addRow(nuevaFila);
		}
	}
	private Hashtable<String, Double> frecuencias(List<Enfermedad> enfermedades){
		Hashtable<String, Double> diccionario = new Hashtable<String, Double>();
		for(Enfermedad enfermedad : enfermedades)
		{
			if(diccionario.containsKey(enfermedad.getCodigo())) {
				String key = enfermedad.getCodigo();
				double value = diccionario.get(key);
				diccionario.remove(key);
				diccionario.put(key, value+1);
			}else {
				String key = enfermedad.getCodigo();
				diccionario.put(key, 1.0);
			}
		}
		return diccionario;
	}
	private String getMax(Hashtable<String, Double> frecuencias) {
		double max = 0;
		String codigo = "";
		for(String key : frecuencias.keySet()) {
			if(max < frecuencias.get(key)) {
				max=frecuencias.get(key);
				codigo =key;
			}
		}
		return codigo;
	}
	private String getDescripcion(List<Enfermedad> enfermedades,String codigo) {
		for(Enfermedad enfermedad : enfermedades) {
			if(enfermedad.getCodigo().equals(codigo))
				return enfermedad.getDescripcion();
		}
		return null;
	}
	private JButton getBtAnalizar() {
		if (btAnalizar == null) {
			btAnalizar = new JButton("Analizar");
			btAnalizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Date iniUtil = (Date) getSpDiaDesde().getValue();
					java.sql.Date ini = new java.sql.Date(iniUtil.getTime());
					Date finUtil = (Date) getSpDiaHasta().getValue();
					java.sql.Date fin = new java.sql.Date(finUtil.getTime());
					if(iniUtil.after(finUtil)) {
						JOptionPane.showMessageDialog(null, "La fecha de inicio no puede ser posterior a la fecha de fin");
					}else {
						eliminarFilasTabla();
						añadirFilas(ini,fin);
					}
				}
			});
			btAnalizar.setFont(new Font("Tahoma", Font.PLAIN, 17));
		}
		return btAnalizar;
	}
	private JPanel getPnMasInfo() {
		if (pnMasInfo == null) {
			pnMasInfo = new JPanel();
			pnMasInfo.setLayout(new BorderLayout(0, 0));
			pnMasInfo.add(getBtMasInfo(), BorderLayout.NORTH);
			pnMasInfo.add(getPnMasInfoSBt(), BorderLayout.CENTER);
		}
		return pnMasInfo;
	}
	private JButton getBtMasInfo() {
		if (btMasInfo == null) {
			btMasInfo = new JButton("M\u00E1s informaci\u00F3n");
			btMasInfo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					int row = getTableRanking().getSelectedRow();
					if(row == -1)
						JOptionPane.showMessageDialog(null, "Se debe elegir una enfermedad antes.");
					else {
						String code = (String) modeloTabla.getValueAt(row, 0);
						getTxCodigo().setText(code);
						getTaDescripcion().setText("  "+(String) modeloTabla.getValueAt(row, 1));
						getTxNumeroPersonas().setText(ranking.get(code).intValue()+ "");
					}
				}
			});
			btMasInfo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		}
		return btMasInfo;
	}
	private JPanel getPnMasInfoSBt() {
		if (pnMasInfoSBt == null) {
			pnMasInfoSBt = new JPanel();
			pnMasInfoSBt.setLayout(null);
			pnMasInfoSBt.add(getPnCodigo());
			pnMasInfoSBt.add(getPnDescripcion());
		}
		return pnMasInfoSBt;
	}
	private JLabel getLbCodigo() {
		if (lbCodigo == null) {
			lbCodigo = new JLabel("C\u00F3digo:");
			lbCodigo.setBounds(128, 15, 57, 23);
			lbCodigo.setHorizontalAlignment(SwingConstants.CENTER);
			lbCodigo.setFont(new Font("Dialog", Font.PLAIN, 17));
		}
		return lbCodigo;
	}
	private JTextField getTxCodigo() {
		if (txCodigo == null) {
			txCodigo = new JTextField();
			txCodigo.setBounds(197, 13, 146, 27);
			txCodigo.setHorizontalAlignment(SwingConstants.CENTER);
			txCodigo.setEditable(false);
			txCodigo.setFont(new Font("Tahoma", Font.PLAIN, 17));
			txCodigo.setColumns(10);
		}
		return txCodigo;
	}
	private JLabel getLbDescripcion() {
		if (lbDescripcion == null) {
			lbDescripcion = new JLabel("Descripci\u00F3n:");
			lbDescripcion.setBounds(46, 8, 92, 21);
			lbDescripcion.setHorizontalAlignment(SwingConstants.CENTER);
			lbDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 17));
		}
		return lbDescripcion;
	}
	private JLabel getLbNumeroPersonas() {
		if (lbNumeroPersonas == null) {
			lbNumeroPersonas = new JLabel("N\u00FAmero de personas que la han padecido:");
			lbNumeroPersonas.setFont(new Font("Tahoma", Font.PLAIN, 17));
			lbNumeroPersonas.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lbNumeroPersonas;
	}
	private JTextField getTxNumeroPersonas() {
		if (txNumeroPersonas == null) {
			txNumeroPersonas = new JTextField();
			txNumeroPersonas.setHorizontalAlignment(SwingConstants.CENTER);
			txNumeroPersonas.setEditable(false);
			txNumeroPersonas.setFont(new Font("Tahoma", Font.PLAIN, 17));
			txNumeroPersonas.setColumns(10);
		}
		return txNumeroPersonas;
	}
	private JPanel getPnCodigo() {
		if (pnCodigo == null) {
			pnCodigo = new JPanel();
			pnCodigo.setBounds(0, 0, 513, 49);
			pnCodigo.setLayout(null);
			pnCodigo.add(getLbCodigo());
			pnCodigo.add(getTxCodigo());
		}
		return pnCodigo;
	}
	private JPanel getPnDescripcion() {
		if (pnDescripcion == null) {
			pnDescripcion = new JPanel();
			pnDescripcion.setBounds(0, 48, 513, 156);
			pnDescripcion.setLayout(null);
			pnDescripcion.add(getLbDescripcion());
			pnDescripcion.add(getTaDescripcion());
			pnDescripcion.add(getPnNumeroPersonas());
		}
		return pnDescripcion;
	}
	private JPanel getPnNumeroPersonas() {
		if (pnNumeroPersonas == null) {
			pnNumeroPersonas = new JPanel();
			pnNumeroPersonas.setBounds(0, 102, 513, 54);
			FlowLayout flowLayout = (FlowLayout) pnNumeroPersonas.getLayout();
			flowLayout.setHgap(15);
			pnNumeroPersonas.add(getLbNumeroPersonas());
			pnNumeroPersonas.add(getTxNumeroPersonas());
		}
		return pnNumeroPersonas;
	}
	private JTextArea getTaDescripcion() {
		if (taDescripcion == null) {
			taDescripcion = new JTextArea();
			taDescripcion.setBorder(new LineBorder(new Color(0, 0, 0)));
			taDescripcion.setWrapStyleWord(true);
			taDescripcion.setLineWrap(true);
			taDescripcion.setEditable(false);
			taDescripcion.setBounds(150, 5, 351, 84);
			taDescripcion.setFont(new Font("Monospaced", Font.PLAIN, 17));
		}
		return taDescripcion;
	}
}
