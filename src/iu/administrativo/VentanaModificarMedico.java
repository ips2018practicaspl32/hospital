package iu.administrativo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import iu.ModeloNoEditable;
import logica.Medico;
import logica.OperacionesMedico;
import util.PasswordEncrypt;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaModificarMedico extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel pnPrincipal = new JPanel();
	private JPanel pnModificaciones;
	private JPanel pnOpciones;
	private JPanel pnBotones;
	private JButton btnActualizar;
	private JLabel lblNombre;
	private JTextField txtNombre;
	private JLabel lblApellidos;
	private JTextField txtApellidos;
	private JLabel lblCorreo;
	private JTextField txtCorreo;
	private JScrollPane scrTablaMedicos;
	private JTable tableMedicos;
	private JLabel lblContrase�a;
	private JTextField txtContrase�a;

	private ModeloNoEditable modeloTabla;

	/**
	 * Create the dialog.
	 */
	public VentanaModificarMedico() {
		setBounds(100, 100, 709, 484);
		getContentPane().setLayout(new BorderLayout());
		pnPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(pnPrincipal, BorderLayout.CENTER);
		pnPrincipal.setLayout(new BorderLayout(0, 0));
		pnPrincipal.add(getPnModificaciones(), BorderLayout.EAST);
		pnPrincipal.add(getScrTablaMedicos(), BorderLayout.CENTER);
	}

	private JPanel getPnModificaciones() {
		if (pnModificaciones == null) {
			pnModificaciones = new JPanel();
			pnModificaciones.setLayout(new BorderLayout(0, 0));
			pnModificaciones.add(getPnOpciones(), BorderLayout.CENTER);
			pnModificaciones.add(getPnBotones(), BorderLayout.SOUTH);
		}
		return pnModificaciones;
	}

	private JPanel getPnOpciones() {
		if (pnOpciones == null) {
			pnOpciones = new JPanel();
			pnOpciones.setLayout(new GridLayout(0, 1, 0, 10));
			pnOpciones.add(getLblNombre());
			pnOpciones.add(getTxtNombre());
			pnOpciones.add(getLblApellidos());
			pnOpciones.add(getTxtApellidos());
			pnOpciones.add(getLblCorreo());
			pnOpciones.add(getTxtCorreo());
			pnOpciones.add(getLblContrase�a());
			pnOpciones.add(getTxtContrase�a());
		}
		return pnOpciones;
	}

	private JPanel getPnBotones() {
		if (pnBotones == null) {
			pnBotones = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnBotones.getLayout();
			flowLayout.setVgap(20);
			pnBotones.add(getBtnActualizar());
		}
		return pnBotones;
	}

	private JButton getBtnActualizar() {
		if (btnActualizar == null) {
			btnActualizar = new JButton("Actualizar");
			btnActualizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					actualizaMedico();
					eliminarFilasTabla();
					a�adirFilas();
				}
			});
			btnActualizar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return btnActualizar;
	}

	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre:");
			lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return lblNombre;
	}

	private JTextField getTxtNombre() {
		if (txtNombre == null) {
			txtNombre = new JTextField();
			txtNombre.setColumns(10);
		}
		return txtNombre;
	}

	private JLabel getLblApellidos() {
		if (lblApellidos == null) {
			lblApellidos = new JLabel("Apellidos:");
			lblApellidos.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return lblApellidos;
	}

	private JTextField getTxtApellidos() {
		if (txtApellidos == null) {
			txtApellidos = new JTextField();
			txtApellidos.setColumns(10);
		}
		return txtApellidos;
	}

	private JLabel getLblCorreo() {
		if (lblCorreo == null) {
			lblCorreo = new JLabel("Correo electr\u00F3nico:         ");
			lblCorreo.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return lblCorreo;
	}

	private JTextField getTxtCorreo() {
		if (txtCorreo == null) {
			txtCorreo = new JTextField();
			txtCorreo.setColumns(10);
		}
		return txtCorreo;
	}

	private JScrollPane getScrTablaMedicos() {
		if (scrTablaMedicos == null) {
			scrTablaMedicos = new JScrollPane();
			scrTablaMedicos.setViewportView(getTableMedicos());
		}
		return scrTablaMedicos;
	}

	private JTable getTableMedicos() {
		if (tableMedicos == null) {
			tableMedicos = new JTable();
			scrTablaMedicos.setViewportView(tableMedicos);
			String[] columnas = { "DNI", "Nombre", "Apellidos", "E-Mail", "Contrase�a" };

			modeloTabla = new ModeloNoEditable(columnas, 0);
			tableMedicos = new JTable(modeloTabla);
			tableMedicos.getTableHeader().setReorderingAllowed(false);
			tableMedicos.setRowHeight(20);
			a�adirFilas();
		}
		return tableMedicos;
	}

	private JLabel getLblContrase�a() {
		if (lblContrase�a == null) {
			lblContrase�a = new JLabel("Contrase\u00F1a:");
			lblContrase�a.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return lblContrase�a;
	}

	private JTextField getTxtContrase�a() {
		if (txtContrase�a == null) {
			txtContrase�a = new JTextField();
			txtContrase�a.setColumns(10);
		}
		return txtContrase�a;
	}

	private void a�adirFilas() {

		Object[] nuevaFila = new Object[5];
		List<Medico> medicos = OperacionesMedico.listarMedicos();

		for (Medico m : medicos) {

			nuevaFila[0] = m.getIdMedico();
			nuevaFila[1] = m.getNombre();
			nuevaFila[2] = m.getApellidos();
			nuevaFila[3] = m.getEmail();
			nuevaFila[4] = m.getContrase�a();

			modeloTabla.addRow(nuevaFila);
		}
	}

	private void actualizaMedico() {

		String txtNombre = getTxtNombre().getText();
		String txtApellidos = getTxtApellidos().getText();
		String txtCorreo = getTxtCorreo().getText();
		String txtContrase�a = getTxtContrase�a().getText();

		int numeroDeFila = getTableMedicos().getSelectedRow();
		if (numeroDeFila != -1) {
			if (txtNombre.trim().equals("") && txtApellidos.trim().equals("") && txtCorreo.trim().equals("")
					&& txtContrase�a.trim().equals(""))
				JOptionPane.showMessageDialog(this, "Debe introducir al menos un campo para actualizar");
			else {

				String idMedicoActualizar = (String) tableMedicos.getModel().getValueAt(numeroDeFila, 0);

				if (!(txtNombre.trim().equals("")))
					OperacionesMedico.actualizarNombre(idMedicoActualizar, txtNombre);
				if (!(txtApellidos.trim().equals("")))
					OperacionesMedico.actualizarApellidos(idMedicoActualizar, txtApellidos);
				if (!(txtCorreo.trim().equals("")))
					OperacionesMedico.actualizarCorreo(idMedicoActualizar, txtCorreo);
				if (!(txtContrase�a.trim().equals(""))) {
					PasswordEncrypt encriptador = new PasswordEncrypt();
					OperacionesMedico.actualizarContrase�a(idMedicoActualizar,
							encriptador.encriptarPassword(txtContrase�a));
				}
			}
		}
	}

	private void eliminarFilasTabla() {
		if (modeloTabla.getRowCount() > 0) {
			while (modeloTabla.getRowCount() > 0)
				modeloTabla.removeRow(0);
		}
	}
}
