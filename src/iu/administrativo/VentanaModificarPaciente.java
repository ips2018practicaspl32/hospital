package iu.administrativo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import iu.ModeloNoEditable;
import logica.OperacionesPaciente;
import logica.Paciente;

public class VentanaModificarPaciente extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel pnPrincipal = new JPanel();
	private JPanel pnModificaciones;
	private JPanel pnOpciones;
	private JPanel pnBotones;
	private JButton btnActualizar;
	private JLabel lblNombre;
	private JTextField txtNombre;
	private JLabel lblApellidos;
	private JTextField txtApellidos;
	private JLabel lblDireccion;
	private JTextField txtDireccion;
	private JScrollPane scrTablaPacientes;
	private JTable tablePacientes;
	private JLabel lblTelefono;
	private JTextField txtTelefono;

	private ModeloNoEditable modeloTabla;

	/**
	 * Create the dialog.
	 */
	public VentanaModificarPaciente() {
		setBounds(100, 100, 709, 484);
		getContentPane().setLayout(new BorderLayout());
		pnPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(pnPrincipal, BorderLayout.CENTER);
		pnPrincipal.setLayout(new BorderLayout(0, 0));
		pnPrincipal.add(getPnModificaciones(), BorderLayout.EAST);
		pnPrincipal.add(getScrTablaPacientes(), BorderLayout.CENTER);
	}

	private JPanel getPnModificaciones() {
		if (pnModificaciones == null) {
			pnModificaciones = new JPanel();
			pnModificaciones.setLayout(new BorderLayout(0, 0));
			pnModificaciones.add(getPnOpciones(), BorderLayout.CENTER);
			pnModificaciones.add(getPnBotones(), BorderLayout.SOUTH);
		}
		return pnModificaciones;
	}

	private JPanel getPnOpciones() {
		if (pnOpciones == null) {
			pnOpciones = new JPanel();
			pnOpciones.setLayout(new GridLayout(0, 1, 0, 10));
			pnOpciones.add(getLblNombre());
			pnOpciones.add(getTxtNombre());
			pnOpciones.add(getLblApellidos());
			pnOpciones.add(getTxtApellidos());
			pnOpciones.add(getLblDireccion());
			pnOpciones.add(getTxtDireccion());
			pnOpciones.add(getLblTelefono());
			pnOpciones.add(getTxtTelefono());
		}
		return pnOpciones;
	}

	private JPanel getPnBotones() {
		if (pnBotones == null) {
			pnBotones = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnBotones.getLayout();
			flowLayout.setVgap(20);
			pnBotones.add(getBtnActualizar());
		}
		return pnBotones;
	}

	private JButton getBtnActualizar() {
		if (btnActualizar == null) {
			btnActualizar = new JButton("Actualizar");
			btnActualizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					actualizaPaciente();
					eliminarFilasTabla();
					a�adirFilas();
				}
			});
			btnActualizar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return btnActualizar;
	}

	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre:");
			lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return lblNombre;
	}

	private JTextField getTxtNombre() {
		if (txtNombre == null) {
			txtNombre = new JTextField();
			txtNombre.setColumns(10);
		}
		return txtNombre;
	}

	private JLabel getLblApellidos() {
		if (lblApellidos == null) {
			lblApellidos = new JLabel("Apellidos:");
			lblApellidos.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return lblApellidos;
	}

	private JTextField getTxtApellidos() {
		if (txtApellidos == null) {
			txtApellidos = new JTextField();
			txtApellidos.setColumns(10);
		}
		return txtApellidos;
	}

	private JLabel getLblDireccion() {
		if (lblDireccion == null) {
			lblDireccion = new JLabel("Direcci\u00F3n:                        ");
			lblDireccion.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return lblDireccion;
	}

	private JTextField getTxtDireccion() {
		if (txtDireccion == null) {
			txtDireccion = new JTextField();
			txtDireccion.setColumns(10);
		}
		return txtDireccion;
	}

	private JScrollPane getScrTablaPacientes() {
		if (scrTablaPacientes == null) {
			scrTablaPacientes = new JScrollPane();
			scrTablaPacientes.setViewportView(getTablePacientes());
		}
		return scrTablaPacientes;
	}

	private JTable getTablePacientes() {
		if (tablePacientes == null) {
			tablePacientes = new JTable();
			scrTablaPacientes.setViewportView(tablePacientes);
			String[] columnas = { "DNI", "Nombre", "Apellidos", "Direccion", "N� Tel�fono", "ID" };

			modeloTabla = new ModeloNoEditable(columnas, 0);
			tablePacientes = new JTable(modeloTabla);
			tablePacientes.getTableHeader().setReorderingAllowed(false);
			tablePacientes.setRowHeight(20);
			a�adirFilas();
		}
		return tablePacientes;
	}

	private JLabel getLblTelefono() {
		if (lblTelefono == null) {
			lblTelefono = new JLabel("N\u00FAmero de tel\u00E9fono:");
			lblTelefono.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return lblTelefono;
	}

	private JTextField getTxtTelefono() {
		if (txtTelefono == null) {
			txtTelefono = new JTextField();
			txtTelefono.setColumns(10);
		}
		return txtTelefono;
	}

	private void a�adirFilas() {

		Object[] nuevaFila = new Object[6];

		List<Paciente> pacientes = OperacionesPaciente.listarPacientes();

		for (Paciente p : pacientes) {

			nuevaFila[0] = p.getDniPaciente();
			nuevaFila[1] = p.getNombrePaciente();
			nuevaFila[2] = p.getApellidoPaciente();
			nuevaFila[3] = p.getDireccion();
			nuevaFila[4] = p.getTelefono();
			nuevaFila[5] = p.getIdPaciente();

			modeloTabla.addRow(nuevaFila);
		}

		// Eliminar del modelo la tabla del id, pero no de la tabla en si
		if (tablePacientes.getColumnModel().getColumnCount() > 5)
			tablePacientes.removeColumn(tablePacientes.getColumnModel().getColumn(5));
	}

	private void actualizaPaciente() {

		String txtNombre = getTxtNombre().getText();
		String txtApellidos = getTxtApellidos().getText();
		String txtDireccion = getTxtDireccion().getText();
		String txtTelefono = getTxtTelefono().getText();

		int numeroDeFila = getTablePacientes().getSelectedRow();
		if (numeroDeFila != -1) {
			if (txtNombre.trim().equals("") && txtApellidos.trim().equals("") && txtDireccion.trim().equals("")
					&& txtTelefono.trim().equals(""))
				JOptionPane.showMessageDialog(this, "Debe introducir al menos un campo para actualizar");

			else {
				String idPacienteActualizar = (String) tablePacientes.getModel().getValueAt(numeroDeFila, 5);
				if (!(txtNombre.trim().equals("")))
					OperacionesPaciente.actualizarNombre(idPacienteActualizar, txtNombre);
				if (!(txtApellidos.trim().equals("")))
					OperacionesPaciente.actualizarApellido(idPacienteActualizar, txtApellidos);
				if (!(txtDireccion.trim().equals("")))
					OperacionesPaciente.actualizarDireccion(idPacienteActualizar, txtDireccion);
				if (!(txtTelefono.trim().equals(""))) {
					OperacionesPaciente.actualizarTelefono(idPacienteActualizar, txtTelefono);
				}
			}
		}
	}

	private void eliminarFilasTabla() {
		if (modeloTabla.getRowCount() > 0) {
			while (modeloTabla.getRowCount() > 0)
				modeloTabla.removeRow(0);
		}
	}
}
