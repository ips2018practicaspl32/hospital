package iu.administrativo;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import iu.BuscadorMedicoInterface;
import iu.ModeloNoEditable;
import iu.VentanaBuscadorMedicos;
import logica.Administrativo;
import logica.Cita;
import logica.ListarCitas;
import logica.ListarPacientes;
import logica.Medico;
import logica.OperacionesPaciente;
import logica.Paciente;

public class VentanaAceptarCitas extends JDialog implements BuscadorMedicoInterface {

	private static final long serialVersionUID = 1L;
	private JPanel panelPrincipal;
	private JPanel panelSeleccionarMedico;
	private JPanel panelTable;
	private JPanel panelListado;
	private JTable table_Citas;
	private JScrollPane scrollPane;
	private ModeloNoEditable modeloTabla;
	private JPanel panelOpciones;
	private JPanel panelCbMedico;
	private JPanel panelOpMedico;
	private JPanel panelBtnListar;
	private JButton btnFinalizar;
	private JPanel pnBotonesHistorialYCita;
	private JButton btAceptarCita;
	private Medico medicoActual;
	private JLabel lblCitasDeHoy;

	private int dia;
	private int mes;
	private int anio;
	
	private Administrativo admin;
	private JTextField txMedico;
	private JButton btSeleccionarMedico;

	public VentanaAceptarCitas(Administrativo admin) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaOperacionesAdministrativo.class.getResource("/iu/img/logo.jpeg")));
		this.admin = admin;
		setTitle("Seleccionar citas que se quieren aceptar");
		setBounds(100, 100, 1060, 533);
		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelPrincipal.setLayout(new BorderLayout(0, 0));
		setContentPane(panelPrincipal);
		panelPrincipal.add(getPanelSeleccionarMedico(), BorderLayout.NORTH);
		panelPrincipal.add(getPanelListado(), BorderLayout.CENTER);
		if (modeloTabla.getRowCount() > 0) {
			while (modeloTabla.getRowCount() > 0)
				modeloTabla.removeRow(0);
		}

		inicializarCalendario();
		//añadirFilas();
	}

	private void inicializarCalendario() {
		Calendar calendar = new GregorianCalendar();
		dia = calendar.get(Calendar.DAY_OF_MONTH);
		mes = calendar.get(Calendar.MONTH) + 1;
		anio = calendar.get(Calendar.YEAR);
	}

	private JPanel getPanelSeleccionarMedico() {
		if (panelSeleccionarMedico == null) {
			panelSeleccionarMedico = new JPanel();
			panelSeleccionarMedico.setLayout(new BorderLayout(0, 0));
			panelSeleccionarMedico.add(getPanelCbMedico(), BorderLayout.NORTH);
			panelSeleccionarMedico.add(getPanelOpMedico(), BorderLayout.SOUTH);
		}
		return panelSeleccionarMedico;
	}

	private JPanel getPanelTable() {
		if (panelTable == null) {
			panelTable = new JPanel();
			panelTable.setLayout(new BorderLayout(0, 0));
			panelTable.add(getScrollPane(), BorderLayout.SOUTH);
		}
		return panelTable;
	}

	private JPanel getPanelListado() {
		if (panelListado == null) {
			panelListado = new JPanel();
			panelListado.setLayout(new BorderLayout(0, 0));
			panelListado.add(getPanelTable(), BorderLayout.CENTER);
			panelListado.add(getPanelOpciones(), BorderLayout.EAST);
		}
		return panelListado;
	}

	private JTable getTable_Citas() {
		if (table_Citas == null) {
			table_Citas = new JTable();

			scrollPane.setViewportView(table_Citas);
			String[] columnas = { "Dni", "Nombre", "Apellido", "Sala", "Hora de Comienzo", "Hora de Fin", "Tipo" };
			modeloTabla = new ModeloNoEditable(columnas, 0);
			table_Citas = new JTable(modeloTabla);
			table_Citas.getTableHeader().setReorderingAllowed(false);
			table_Citas.setRowHeight(20);
			table_Citas.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(20);

		}
		return table_Citas;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTable_Citas());
		}
		return scrollPane;
	}

	private JPanel getPanelOpciones() {
		if (panelOpciones == null) {
			panelOpciones = new JPanel();
			panelOpciones.setLayout(new BorderLayout(0, 0));
			panelOpciones.add(getPnBotonesHistorialYCita(), BorderLayout.NORTH);
			panelOpciones.add(getBtnFinalizar(), BorderLayout.SOUTH);
		}
		return panelOpciones;
	}


	private JPanel getPanelCbMedico() {
		if (panelCbMedico == null) {
			panelCbMedico = new JPanel();
			panelCbMedico.add(getLblCitasDeHoy());
			panelCbMedico.add(getTxMedico());
			panelCbMedico.add(getBtSeleccionarMedico());
		}
		return panelCbMedico;
	}

	private JPanel getPanelOpMedico() {
		if (panelOpMedico == null) {
			panelOpMedico = new JPanel();
			panelOpMedico.setLayout(new BorderLayout(0, 0));
			panelOpMedico.add(getPanelBtnListar(), BorderLayout.CENTER);
		}
		return panelOpMedico;
	}

	private void añadirFilas() {

		Object[] nuevaFila = new Object[7];
		ListarCitas lc = new ListarCitas();
		String index = medicoActual.getIdMedico();

		List<Cita> relacionCitas = lc.cargarCitasPorFechaYMedicoNoAceptadas(index + "", dia + "", mes + "", anio + "");

		for (Cita c : relacionCitas) {
			ListarPacientes lp = new ListarPacientes();
			Paciente p = lp.getPacientePorID(c.getIdPaciente());
			nuevaFila[0] = p.getDniPaciente();
			nuevaFila[1] = p.getNombrePaciente();
			nuevaFila[2] = p.getApellidoPaciente();
			nuevaFila[3] = c.getIdSala();
			nuevaFila[4] = new java.util.Date(c.getHoraComienzo().getTime()).toString().substring(10, 16);
			nuevaFila[5] = new java.util.Date(c.getHoraFinal().getTime()).toString().substring(10, 16);
			nuevaFila[6] = c.getTipo();
			modeloTabla.addRow(nuevaFila);
		}
	}

	private JPanel getPanelBtnListar() {
		if (panelBtnListar == null) {
			panelBtnListar = new JPanel();
		}
		return panelBtnListar;
	}

	private JButton getBtnFinalizar() {
		if (btnFinalizar == null) {
			btnFinalizar = new JButton("Finalizar");
			btnFinalizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mostrarPrincipal();
				}
			});
			btnFinalizar.setMnemonic('f');
			btnFinalizar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return btnFinalizar;
	}

	private void mostrarPrincipal() {
		this.setVisible(false);
	}

	private JPanel getPnBotonesHistorialYCita() {
		if (pnBotonesHistorialYCita == null) {
			pnBotonesHistorialYCita = new JPanel();
			pnBotonesHistorialYCita.setLayout(new GridLayout(0, 1, 0, 0));
			pnBotonesHistorialYCita.add(getBtAceptarCita());
		}
		return pnBotonesHistorialYCita;
	}

	private JButton getBtAceptarCita() {
		if (btAceptarCita == null) {
			btAceptarCita = new JButton("Aceptar Cita");
			btAceptarCita.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					int row = getTable_Citas().getSelectedRow();
					if (row != -1) {
						String dni = (String) modeloTabla.getValueAt(row, 0);
						String fecha = dia + "-" + mes + "-" + anio;
						String horaComienzo = (String) modeloTabla.getValueAt(row, 4);
						String idCita = getIdCita(OperacionesPaciente.getPacienteDni(dni).getIdPaciente(), fecha, horaComienzo);
						Cita.updateCita(idCita, admin.getId());
						JOptionPane.showMessageDialog(null, "Se ha aceptado la cita correctamente");
						borrarFilas();
						añadirFilas();
					}
				}
			});
			btAceptarCita.setMnemonic('A');
			btAceptarCita.setFont(new Font("Tahoma", Font.PLAIN, 20));
		}
		return btAceptarCita;
	}

	private String getIdCita(String idPaciente, String fecha, String horaComienzo) {
		return Cita.getIdCita(idPaciente, fecha, horaComienzo);
	}

	private JLabel getLblCitasDeHoy() {
		if (lblCitasDeHoy == null) {
			lblCitasDeHoy = new JLabel("Citas asignadas a:");
			lblCitasDeHoy.setFont(new Font("Tahoma", Font.PLAIN, 25));
		}
		return lblCitasDeHoy;
	}
	private JTextField getTxMedico() {
		if (txMedico == null) {
			txMedico = new JTextField();
			txMedico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					añadirFilas();
				}
			});
			txMedico.setFont(new Font("Tahoma", Font.PLAIN, 24));
			txMedico.setEditable(false);
			txMedico.setColumns(10);
		}
		return txMedico;
	}
	private JButton getBtSeleccionarMedico() {
		if (btSeleccionarMedico == null) {
			btSeleccionarMedico = new JButton("Seleccionar Medico...");
			VentanaBuscadorMedicos ventana = new VentanaBuscadorMedicos(getTxMedico(),this);
			btSeleccionarMedico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					mostrarVentana(ventana);
					if (medicoActual!=null){
					borrarFilas();
					añadirFilas();
					}
				}
			});
			btSeleccionarMedico.setFont(new Font("Tahoma", Font.PLAIN, 23));
		}
		return btSeleccionarMedico;
	}

	protected void borrarFilas() {
		int filas = modeloTabla.getRowCount();
		for (int i=0;i<filas;i++){
			modeloTabla.removeRow(filas-1-i);
		}
		
	}

	protected void mostrarVentana(JDialog ventana) {
		ventana.setLocationRelativeTo(this);
		ventana.setModal(true);
		ventana.setVisible(true);
		
	}

	@Override
	public void setMedico(Medico medico) {
		this.medicoActual = medico;
		
	}
}
