package iu.administrativo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import logica.AņadirMedicos;
import logica.SendEmail;

public class VentanaCrearMedicos extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel pnIntroducir = new JPanel();
	private JTextField txNombre;
	private JTextField txApellidos;
	private JTextField txEmail;
	private JLabel lbNombre;
	private JLabel lbApellidos;
	private JLabel lbEmail;
	private AņadirMedicos aņadir = new AņadirMedicos();
	private JTextField txDni;


	/**
	 * Create the dialog.
	 */
	public VentanaCrearMedicos() {
		setTitle("A\u00F1adir nuevo m\u00E9dico");
		setBounds(100, 100, 535, 309);
		getContentPane().setLayout(new BorderLayout());
		pnIntroducir.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(pnIntroducir, BorderLayout.CENTER);
		pnIntroducir.setLayout(null);
		{
			txNombre = new JTextField();
			txNombre.setBounds(209, 40, 230, 20);
			pnIntroducir.add(txNombre);
			txNombre.setColumns(10);
		}
		{
			txApellidos = new JTextField();
			txApellidos.setBounds(209, 85, 230, 20);
			pnIntroducir.add(txApellidos);
			txApellidos.setColumns(10);
		}
		{
			txEmail = new JTextField();
			txEmail.setBounds(209, 171, 230, 20);
			pnIntroducir.add(txEmail);
			txEmail.setColumns(10);
		}
		pnIntroducir.add(getLbNombre());
		pnIntroducir.add(getLbApellidos());
		pnIntroducir.add(getLbEmail());
		{

			JLabel lblIntroduzcaDelMedico = new JLabel("Introduzca DNI del medico:");
			lblIntroduzcaDelMedico.setBounds(26, 133, 202, 14);
			pnIntroducir.add(lblIntroduzcaDelMedico);
		}
		pnIntroducir.add(getTxDni());
		{
			JPanel pnBotones = new JPanel();
			pnBotones.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(pnBotones, BorderLayout.SOUTH);
			{
				JButton botonAceptar = new JButton("Finalizar");
				botonAceptar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if (checkCamposValidos() && checkMedicoNoRepetido()) {
							String contraseņa = aņadir.aņadirMedico(txNombre.getText(), txApellidos.getText(),
									txEmail.getText(), txDni.getText());

							@SuppressWarnings("unused")
							SendEmail correo = new SendEmail(txDni.getText(), txNombre.getText(), txApellidos.getText(),
									txEmail.getText(), contraseņa);
							cerrar();
						}
					}
				});
				botonAceptar.setActionCommand("OK");
				pnBotones.add(botonAceptar);
				getRootPane().setDefaultButton(botonAceptar);
			}
			{
				JButton botonCancelar = new JButton("Cancelar");
				botonCancelar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						cerrar();
					}
				});
				botonCancelar.setActionCommand("Cancel");
				pnBotones.add(botonCancelar);
			}
		}
	}

	protected void cerrar() {
		this.dispose();

	}

	protected boolean checkMedicoNoRepetido() {
		boolean flag = aņadir.checkMedicoNoRepetido(txNombre.getText(), txApellidos.getText(), txDni.getText());
		if (!flag) {
			JOptionPane.showMessageDialog(null, "Ya existe un medico con esos datos");
		}
		return flag;
	}

	protected boolean checkCamposValidos() {
		if (txApellidos.getText().isEmpty() || txApellidos.getText().isEmpty() || txEmail.getText().isEmpty()
				|| txDni.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Todos los campos deben de estar rellenados");
			return false;
		}
		return true;
	}

	private JLabel getLbNombre() {
		if (lbNombre == null) {
			lbNombre = new JLabel("Nombre del m\u00E9dico");
			lbNombre.setBounds(26, 37, 202, 26);
		}
		return lbNombre;
	}

	private JLabel getLbApellidos() {
		if (lbApellidos == null) {
			lbApellidos = new JLabel("Apellidos del m\u00E9dico");
			lbApellidos.setBounds(26, 88, 202, 14);
		}
		return lbApellidos;
	}

	private JLabel getLbEmail() {
		if (lbEmail == null) {
			lbEmail = new JLabel("Email del m\u00E9dico");
			lbEmail.setBounds(26, 174, 147, 14);
		}
		return lbEmail;
	}

	private JTextField getTxDni() {
		if (txDni == null) {
			txDni = new JTextField();
			txDni.setColumns(10);
			txDni.setBounds(209, 130, 230, 20);
		}
		return txDni;
	}
}
