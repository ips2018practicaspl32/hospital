package util;

import org.apache.commons.codec.digest.DigestUtils;

public class PasswordEncrypt {
	/**
	 * Encripta el texto introducido como par�metro
	 * 
	 * @param passSinEncriptar contrase�a sin encriptar, escrita por el usuario o la que se va a asignar a la hora de crear el usuario.
	 * @return cadena encriptada que equivale a la contrase�a introducida como par�metro, se utilizar� a la hora de introducirla en la bbdd y a la hora de querer realizar comprobaciones.
	 */
	public String encriptarPassword(String passSinEncriptar) {
		return DigestUtils.md5Hex(passSinEncriptar);
	}
	/*
	 * En el login debe extraerse de la bbdd la contrase�a del usuario introducido, esta contrase�a estar� encriptada,
	 * hay que encriptar el texto introducido como contrase�a por el usuario y utilizar este m�todo para comprobar si son iguales.
	 */
	public boolean checkPasswordsEncriptadas(String passEncriptada1, String passEncriptada2) {
		return passEncriptada1.equals(passEncriptada2);
	}
}


