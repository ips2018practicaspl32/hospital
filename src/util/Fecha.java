package util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Fecha {

	private int dia;
	private int mes;
	private int a�o;
	
	public Fecha() {
		Date date = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
		String fechaFormat = formateador.format(date);
		String[] fecha = fechaFormat.split("-"); 
		setDia(Integer.parseInt(fecha[0]));
		setMes(Integer.parseInt(fecha[1]));
		setA�o(Integer.parseInt(fecha[2]));
	}
	
	public Fecha(int dia, int mes, int a�o) {
		setDia(dia);
		setMes(mes);
		setA�o(a�o);
	}
	
	private void setDia(int dia) {
		this.dia = dia;
	}
	
	private void setMes(int mes) {
		this.mes = mes;
	}
	
	private void setA�o(int a�o) {
		this.a�o = a�o;
	}
	
	public int getDia() {
		return dia;
	}
	
	public int getMes() {
		return mes;
	}
	
	public int getA�o() {
		return a�o;
	}
}
